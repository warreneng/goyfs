﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs.Test.Common
{
    using Goyfs;
    using System.Collections.Generic;
    using System;

    public class MediatorImpl
    {
        // Record Keeping
        public static List<Instance> Instances = new List<Instance>();
        public class Instance
        {
            public object Mediator { get; internal set; }
            public object View { get; internal set; }
            public Context Context { get; internal set; }
            public int LastMediate { get; internal set; }
            public int LastDispose { get; internal set; }
        }
        protected static Instance GetInstance(object mediator, object view, Context context)
        {
            Instance instance = MediatorImpl.Instances.Find((Instance x) => { return x.Mediator.Equals(mediator) && x.View.Equals(view) && x.Context == context; });
            if (instance == null)
            {
                MediatorImpl.Instances.Add(instance = new Instance() { Mediator = mediator, View = view, Context = context });
            }
            return instance;
        }

        // IMediator implementations
        public class A<TView> : Mediator<TView> where TView : class
        {
            public override void OnMediate(object view)
            {
                MediatorImpl.GetInstance(this, view, this.Context).LastMediate = Sequence.Next;
                base.OnMediate(view);
            }

            protected override void OnDispose(bool invokedByUser)
            {
                MediatorImpl.GetInstance(this, this.view, this.Context).LastDispose = Sequence.Next;
            }
        }

        public class B<TView> : Mediator<TView> where TView : class
        {
            public override void OnMediate(object view)
            {
                MediatorImpl.GetInstance(this, view, this.Context).LastMediate = Sequence.Next;
                base.OnMediate(view);
            }

            protected override void OnDispose(bool invokedByUser)
            {
                MediatorImpl.GetInstance(this, this.view, this.Context).LastDispose = Sequence.Next;
            }
        }

        public class FetchSingleton<TView, TInstance> : Mediator<TView> where TView : class
        {
            public object FetchedObject { get; private set; }

            public override void OnMediate(object view)
            {
                MediatorImpl.GetInstance(this, view, this.Context).LastMediate = Sequence.Next;
                base.OnMediate(view);
                this.FetchedObject = this.Context.InstanceBinder.Get<TInstance>();  // expect IInstanceBinder to throw exception if not available
            }

            protected override void OnDispose(bool invokedByUser)
            {
            }
        }
    }
}
