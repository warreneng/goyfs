﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs
{
    using System;

    /// <summary>
    /// IInstanceBinding represents IInstanceBinder's mapping between a type and its bound instances.
    /// </summary>
    public interface IInstanceBinding<TInstance, TKey> : IDisposable, IContextSensitive
    {
        /// <summary>
        /// Returns true if no instances exist.
        /// </summary>
        bool Empty { get; }

        /// <summary>
        /// Create an anonymous instance with an empty value.
        /// </summary>
        void AddInstance();

        /// <summary>
        /// Create an anonymous instance with the given value.
        /// </summary>
        /// <param name="instance">instance of type</param>
        void AddInstance(TInstance instance);

        /// <summary>
        /// Create an ID'd instance with an empty value.
        /// </summary>
        /// <param name="key">identifier of the instance</param>
        void AddInstance(TKey key);

        /// <summary>
        /// Create an ID'd instance with the given value.
        /// </summary>
        /// <param name="instance">instance of type</param>
        /// <param name="key">identifier of the instance</param>
        void AddInstance(TInstance instance, TKey key);

        /// <summary>
        /// Remove an anonymous instance.
        /// </summary>
        void RemoveInstance();

        /// <summary>
        /// Remove an ID'd instance.
        /// </summary>
        /// <param name="key">identifier of the instance</param>
        void RemoveInstance(TKey key);

        /// <summary>
        /// Get the value of an anonymous instance.
        /// </summary>
        /// <returns>value of the instance</returns>
        TInstance GetInstance();

        /// <summary>
        /// Get the value of a named instance.
        /// </summary>
        /// <param name="key">identifier of the instance</param>
        /// <returns>instance of type</returns>
        TInstance GetInstance(TKey key);
    }
}
