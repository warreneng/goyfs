﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Singleton represents a solitary instance bound to a type in an IInstanceBinder.
    /// </summary>
    public class Singleton<TInstance> : InstanceBinding<TInstance>
    {
        /// <summary>
        /// The instance managed by this Singleton.
        /// </summary>
        private TInstance instance;

        /// <summary>
        /// \copybrief InstanceBinding<T>::AddInstance(object value)
        /// 
        /// If the value is an IContextSensitive, its Context will be set to the Singleton's Context.
        /// </summary>
        /// <param name="instance">instance of TInstance</param>
        /// <exception cref="ArgumentNullException">value is null</exception>
        /// <exception cref="InvalidOperationException">instance already exists</exception>
        /// <exception cref="ObjectDisposedException">calling after Dispose() has occurred</exception>
        public void AddInstance(TInstance instance)
        {
            this.AssertUndisposed();

            if (EqualityComparer<TInstance>.Default.Equals(instance, default(TInstance)))
            {
                throw new ArgumentException("Singleton instance cannot be defualt value.");
            }
            else if (!EqualityComparer<TInstance>.Default.Equals(this.instance, default(TInstance)))
            {
                throw new InvalidOperationException("Singleton instance already bound.");
            }
            
            this.instance = instance;
            this.ScopeToContext(instance);
        }

        /// <summary>
        /// \copybrief InstanceBinding<T>::GetInstance()
        /// 
        /// If necessary, the instance's value will be lazily constructed via its default constructor.
        /// If the value is an IContextSensitive, its Context will be set to the Singleton's Context.
        /// </summary>
        /// <returns>value of the instance</returns>
        /// <exception cref="InvalidOperationException">instance does not exist</exception>
        /// <exception cref="ObjectDisposedException">calling after Dispose() has occurred</exception>
        public TInstance GetInstance()
        {
            this.AssertUndisposed();

            return this.instance = this.GetOrConstructValue(this.instance);
        }

        /// \copydoc InstanceBinding<T>::OnDispose(bool invokedByUser)
        protected override void OnDispose(bool invokedByUser)
        {
            if (!this.Disposed)
            {
                if (invokedByUser)
                {
                    (this.instance as IDisposable)?.Dispose();
                    this.instance = default(TInstance);
                }
            }
        }
    }
}
