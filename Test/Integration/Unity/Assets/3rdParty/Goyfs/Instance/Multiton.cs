﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Multiton represents any number of instances bound to a type in an IInstanceBinder.
    /// </summary>
    public class Multiton<TInstance, TKey> : InstanceBinding<TInstance>
    {
        /// <summary>
        /// The instances managed by this Multiton.
        /// </summary>
        private Dictionary<TKey, TInstance> instances = new Dictionary<TKey, TInstance>();

        /// \copydoc InstanceBinding<T>::Empty
        public bool Empty { get { return this.instances.Count < 1; } }

        /// <summary>
        /// \copybrief InstanceBinding<T>::AddInstance(string name)
        /// 
        /// The instance's value will be lazily constructed via its default constructor when retrieved via GetInstance(string name).
        /// </summary>
        /// <param name="key">identifier of the instance</param>
        /// <exception cref="ArgumentNullException">name is null or empty</exception>
        /// <exception cref="ArgumentException">name is not unique</exception>
        /// <exception cref="ObjectDisposedException">calling after Dispose() has occurred</exception>
        public void AddInstance(TKey key)
        {
            this.AssertUndisposed();

            if ((typeof(TKey) == typeof(string) && string.IsNullOrEmpty(key as string)) || key == null || key.Equals(default(TKey)))
            {
                throw new ArgumentNullException($"Multiton key cannot be null, empty, or default value of type {typeof(TKey).FullName}.");
            }
            else if (this.instances.ContainsKey(key))
            {
                throw new ArgumentException($"Multiton key \"{key}\" must be unique.");
            }

            this.instances.Add(key, default(TInstance));
        }

        /// <summary>
        /// \copybrief InstanceBinding<T>::AddInstance(object value, string name)
        /// 
        /// If the value is an IContextSensitive, its Context will be set to the Multiton's Context.
        /// </summary>
        /// <param name="instance">value of the instance</param>
        /// <param name="key">name of the instance</param>
        /// <exception cref="ArgumentNullException">value or name is null or empty</exception>
        /// <exception cref="ArgumentException">name is not unique</exception>
        /// <exception cref="ObjectDisposedException">calling after Dispose() has occurred</exception>
        public void AddInstance(TInstance instance, TKey key)
        {
            this.AssertUndisposed();

            if (EqualityComparer<TInstance>.Default.Equals(instance, default(TInstance)))
            {
                throw new ArgumentNullException("Multiton instance cannot be default value.");
            }
            else if ((typeof(TKey) == typeof(string) && string.IsNullOrEmpty(key as string) || key == null || key.Equals(default(TKey))))
            {
                throw new ArgumentNullException($"Multiton key cannot be null, empty, or default value of type {typeof(TKey).FullName}.");
            }
            else if (this.instances.ContainsKey(key))
            {
                throw new ArgumentException($"Multiton key \"{key}\" must be unique.");
            }

            this.instances.Add(key, instance);
            this.ScopeToContext(instance);
        }

        /// <summary>
        /// \copybrief InstanceBinding<T>::RemoveInstance(string name)
        /// 
        /// If the instance's value exists and is an IDisposable, its Dispose() method will be called.
        /// </summary>
        /// <param name="key">name of the instance</param>
        /// <exception cref="KeyNotFoundException">instance with name not found</exception>
        /// <exception cref="ObjectDisposedException">calling after Dispose() has occurred</exception>
        public void RemoveInstance(TKey key)
        {
            this.AssertUndisposed();

            if (!this.instances.TryGetValue(key, out TInstance instance))
            {
                throw new KeyNotFoundException($"Multiton with key \"{key}\" not found.");
            }
            else
            {
                this.DisposeInstance(instance);
                this.instances.Remove(key);
            }
        }

        /// <summary>
        /// \copybrief InstanceBinding<T>::GetInstance(string name)
        /// 
        /// If necessary, the instance's value will be lazily constructed via its default constructor.
        /// If the value is an IContextSensitive, its Context will be set to the Multiton's Context.
        /// </summary>
        /// <param name="key">name of the instance</param>
        /// <returns>value of the instance</returns>
        /// <exception cref="KeyNotFoundException">instance with name not found</exception>
        /// <exception cref="ObjectDisposedException">calling after Dispose() has occurred</exception>
        public TInstance GetInstance(TKey key)
        {
            this.AssertUndisposed();

            if (!this.instances.TryGetValue(key, out TInstance instance))
            {
                throw new KeyNotFoundException($"Multiton with key \"{key}\" not found.");
            }
            else
            {
                return this.instances[key] = this.GetOrConstructValue(instance);
            }
        }

        
        /// \copydoc Disposable::OnDispose()
        protected override void OnDispose(bool invokedByUser)
        {
            if (!this.Disposed)
            {
                if (invokedByUser)
                {
                    foreach (TInstance instance in this.instances.Values)
                    {
                        this.DisposeInstance(instance);
                    }
                }

                this.instances.Clear();
            }
        }

        /// <summary>
        /// Disposes of an Instance's value if the value is an IDisposable.
        /// </summary>
        /// <param name="instance">Instance whose value to dispose</param>
        private void DisposeInstance(TInstance instance)
        {
            (instance as IDisposable)?.Dispose();
        }
    }
}
