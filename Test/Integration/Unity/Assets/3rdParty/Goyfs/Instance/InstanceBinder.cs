﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// InstanceBinder binds types to optionally-named instances of that type.
    /// Useful for virtual singletons and multitons without implementing the pattern in each class.
    /// </summary>
    public class InstanceBinder : Disposable, IInstanceBinder
    {
        /// \copydoc IContextSensitive::Context
        public Context Context { protected get; set; }

        /// <summary>
        /// Dictionary of the type+instance bindings registered to this InstanceBinder.
        /// Any number of named instances may be bound to a single type, otherwise just
        /// a single anonymous instance may be bound to the type.
        /// </summary>
        private Dictionary<Type, object> bindings;

        /// <summary>
        /// Constructs a new instance of InstanceBinder.
        /// </summary>
        public InstanceBinder() : base()
        {
            this.bindings = new Dictionary<Type, object>();
        }

        /// <summary>
        /// \copybrief IInstanceBinder::Bind<T>()
        /// 
        /// This is a singleton binding which does not support future bindings to T.
        /// The instance's value will be lazily constructed via its default constructor when retrieved via Get<T>().
        /// </summary>
        /// <typeparam name="TInstance">type of the binding</typeparam>
        /// <exception cref="ObjectDisposedException">calling after Dispose() has occurred</exception>
        /// <exception cref="InvalidOperationException">binding for this type already exists</exception>
        public virtual void Bind<TInstance>()
        {
            this.AssertUndisposed();

            if (!this.bindings.TryGetValue(typeof(TInstance), out object binding))
            {
                binding = new Singleton<TInstance>() { Context = this.Context };
                this.bindings.Add(typeof(TInstance), binding);
            }
            else
            {
                throw new InvalidOperationException($"Binding for type {typeof(TInstance).FullName} already exists");
            }
        }

        /// <summary>
        /// \copybrief IInstanceBinder::Bind<T>(T value)
        /// 
        /// This is a singleton binding which does not support future bindings to T.
        /// If the instance's value is an IContextSentitive, its Context will be set to the InstanceBinder's Context.
        /// </summary>
        /// <typeparam name="TInstance">type of the binding</typeparam>
        /// <param name="instance">instance of type T</param>
        /// <exception cref="ObjectDisposedException">calling after Dispose() has occurred</exception>
        /// <exception cref="InvalidOperationException">binding for this type already exists</exception>
        public virtual void Bind<TInstance>(TInstance instance)
        {
            this.AssertUndisposed();

            if (!this.bindings.TryGetValue(typeof(TInstance), out object binding))
            {
                binding = new Singleton<TInstance>() { Context = this.Context };
                this.bindings.Add(typeof(TInstance), binding);
                (binding as Singleton<TInstance>).AddInstance(instance);
            }
            else
            {
                throw new InvalidOperationException($"Binding for type {typeof(TInstance).FullName} already exists");
            }
        }

        /// <summary>
        /// \copybrief IInstanceBinder::Bind<T>(string name)
        /// 
        /// This is a multiton binding which supports future bindings to T by name.
        /// The instance's value will be lazily constructed via its default constructor when retrieved via Get<T>(string name).
        /// </summary>
        /// <typeparam name="TInstance">type of the binding</typeparam>
        /// <typeparamref name="TKey"/>type of the key</typeparam>
        /// <param name="key">identifier of the instance</param>
        /// <exception cref="ObjectDisposedException">calling after Dispose() has occurred</exception>
        /// <exception cref="ArgumentNullException">name is null or empty</exception>
        /// <exception cref="ArgumentException">name is not unique</exception>
        /// <exception cref="InvalidOperationException">binding already exists as non-multiton</exception>
        public virtual void Bind<TInstance, TKey>(TKey key)
        {
            this.AssertUndisposed();

            object binding = null;
            if (!this.bindings.TryGetValue(typeof(TInstance), out binding))
            {
                binding = new Multiton<TInstance, TKey>() { Context = this.Context };
                this.bindings.Add(typeof(TInstance), binding);
            }
            if (binding is Singleton<TInstance>)
            {
                throw new InvalidOperationException($"Binding for type {typeof(TInstance).FullName} already exists as Singleton");
            }

            (binding as Multiton<TInstance, TKey>).AddInstance(key);
        }

        /// <summary>
        /// \copybrief IInstanceBinder::Bind<T>(T value, string name)
        /// 
        /// This is a multiton binding which supports future bindings to T by name. 
        /// If the instance's value is an IContextSensitive, its Context will be set to the InstanceBinder's Context.
        /// </summary>
        /// <typeparam name="TInstance">type of the binding</typeparam>
        /// <typeparam name="TKey">type of the key</typeparam>
        /// <param name="instance">instance of type T</param>
        /// <param name="key">name of the instance</param>
        /// <exception cref="ObjectDisposedException">calling after Dispose() has occurred</exception>
        /// <exception cref="ArgumentNullException">value or name is null or empty</exception>
        /// <exception cref="ArgumentException">name is not unique</exception>
        /// <exception cref="InvalidOperationException">binding already exists as non-multiton</exception>
        public virtual void Bind<TInstance, TKey>(TInstance instance, TKey key)
        {
            this.AssertUndisposed();

            object binding = null;
            if (!this.bindings.TryGetValue(typeof(TInstance), out binding))
            {
                binding = new Multiton<TInstance, TKey>() { Context = this.Context };
                this.bindings.Add(typeof(TInstance), binding);
            }
            if (binding is Singleton<TInstance>)
            {
                throw new InvalidOperationException($"Binding for type {typeof(TInstance).FullName} already exists as Singleton");
            }

            (binding as Multiton<TInstance, TKey>).AddInstance(instance, key);
        }

        /// <summary>
        /// \copybrief IInstanceBinder::Unbind<T>()
        /// 
        /// This method can only unbind singleton instances.
        /// If the instance's value is an IDisposable, its Dispose() will be called.
        /// </summary>
        /// <typeparam name="TInstance">type of the binding</typeparam>
        /// <exception cref="ObjectDisposedException">calling after Dispose() has occurred</exception>
        /// <exception cref="KeyNotFoundException">binding not found for type</exception>
        /// <exception cref="InvalidOperationException">binding is not a singleton</exception>
        public virtual void Unbind<TInstance>()
        {
            this.AssertUndisposed();

            if (this.bindings.TryGetValue(typeof(TInstance), out object binding))
            {
                if (!(binding is Singleton<TInstance>))
                {
                    throw new InvalidOperationException($"Binding for type {typeof(TInstance).FullName} is not a Singleton");
                }
                else
                {
                    (binding as Singleton<TInstance>).Dispose();
                    this.bindings.Remove(typeof(TInstance));
                }
            }
            else
            {
                throw new KeyNotFoundException($"Type {typeof(TInstance).FullName} is not bound.");
            }
        }

        /// <summary>
        /// \copybrief IInstanceBinder::Unbind<T>(string name)
        /// 
        /// This method can only unbind multiton instances.
        /// If the instance's value is an IDisposable, its Dispose() will be called.
        /// </summary>
        /// <typeparam name="TInstance">type of the binding</typeparam>
        /// <typeparam name="TKey">type of the key</typeparam>
        /// <param name="key">name of the instance</param>
        /// <exception cref="ObjectDisposedException">calling after Dispose() has occurred</exception>
        /// <exception cref="KeyNotFoundException">binding not found for type</exception>
        /// <exception cref="ArgumentNullException">binding name is null or empty</exception>
        /// <exception cref="InvalidOperationException">binding is not a multiton</exception>
        public virtual void Unbind<TInstance, TKey>(TKey key)
        {
            this.AssertUndisposed();

            if (this.bindings.TryGetValue(typeof(TInstance), out object binding))
            {
                if (binding is Singleton<TInstance>)
                {
                    throw new InvalidOperationException($"Binding for type {typeof(TInstance).FullName} is not a Multiton");
                }

                Multiton<TInstance, TKey> multiton = binding as Multiton<TInstance, TKey>;
                if (multiton == null)
                {
                    throw new ArgumentException($"Key of type {typeof(TKey).FullName} incompatible with Multiton");
                }
                multiton.RemoveInstance(key);
                if (multiton.Empty)
                {
                    multiton.Dispose();
                    this.bindings.Remove(typeof(TInstance));
                }
            }
            else
            {
                throw new KeyNotFoundException($"Type {typeof(TInstance).FullName} is not bound");
            }
        }

        /// <summary>
        /// \copybrief IInstanceBinder::Get<T>()
        /// 
        /// This method can only retrieve singleton instances.
        /// If necessary, the instance's value will be constructed via its default constructor.
        /// If the value is an IContextSensitive, its Context will be set to the InstanceBinder's Context.
        /// </summary>
        /// <typeparam name="TInstance">type of the binding</typeparam>
        /// <returns>instance of type TInstance</returns>
        /// <exception cref="ObjectDisposedException">calling after Dispose() has occurred</exception>
        /// <exception cref="KeyNotFoundException">binding not found for type</exception>
        /// <exception cref="InvalidOperationException">binding is not a singleton</exception>
        public virtual TInstance Get<TInstance>()
        {
            this.AssertUndisposed();

            if (this.bindings.TryGetValue(typeof(TInstance), out object binding))
            {
                Singleton<TInstance> singleton = binding as Singleton<TInstance>;
                if (singleton == null)
                {
                    throw new InvalidOperationException($"Type {typeof(TInstance).FullName} bound as Multiton");
                }
                return singleton.GetInstance();
            }
            else if (this.Context.Parent != null)
            {
                // raise to parent context
                return this.Context.Parent.InstanceBinder.Get<TInstance>();
            }
            else
            {
                // binding not found in tree
                throw new KeyNotFoundException($"No binding found for type {typeof(TInstance).FullName}");
            }
        }

        /// <summary>
        /// \copybrief IInstanceBinder::Get<T>(string name)
        /// 
        /// This method can only retrieve multiton instances.
        /// If necessary, the instance's value will be constructed via its default constructor.
        /// If the value is an IContextSensitive, its Context will be set to the InstanceBinder's Context.
        /// </summary>
        /// <typeparam name="TInstance">type of the binding</typeparam>
        /// <typeparam name="TKey">type of the key</typeparam>
        /// <param name="key">identidier of the instance</param>
        /// <returns>instance of type TInstance</returns>
        /// <exception cref="ObjectDisposedException">calling after Dispose() has occurred</exception>
        /// <exception cref="KeyNotFoundException">binding not found for type and name</exception>
        /// <exception cref="ArgumentNullException">name is null or empty</exception>
        /// <exception cref="ArgumentException">named instance not found</exception>
        /// <exception cref="InvalidOperationException">type is not a multiton</exception>
        public virtual TInstance Get<TInstance, TKey>(TKey key)
        {
            this.AssertUndisposed();

            if (this.bindings.TryGetValue(typeof(TInstance), out object binding))
            {
                Singleton<TInstance> singleton = binding as Singleton<TInstance>;
                if (singleton != null)
                {
                    throw new InvalidOperationException($"Type {typeof(TInstance).FullName} bound as Singleton");
                }
                Multiton<TInstance, TKey> multiton = binding as Multiton<TInstance, TKey>;
                if (multiton == null)
                {
                    throw new ArgumentException($"Key of type {typeof(TKey).FullName} incompatible with Multiton");
                }
                return multiton.GetInstance(key);
            }
            else if (this.Context.Parent != null)
            {
                // raise to parent context
                return this.Context.Parent.InstanceBinder.Get<TInstance, TKey>(key);
            }
            else
            {
                // binding not found in tree
                throw new KeyNotFoundException($"No binding found for type {typeof(TInstance).FullName}");
            }
        }

        /// \copydoc Disposable::OnDispose
        protected override void OnDispose(bool invokedByUser)
        {
            if (!this.Disposed)
            {
                if (invokedByUser)
                {
                    foreach (IDisposable disposable in this.bindings.Values)
                    {
                        disposable.Dispose();
                    }
                }

                this.bindings.Clear();
            }
        }
    }
}
