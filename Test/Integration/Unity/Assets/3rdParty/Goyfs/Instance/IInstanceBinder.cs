﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs
{
    using System;

    /// <summary>
    /// IInstanceBinder defines the methods to bind types to (optionally named) instances of the type.
    /// Useful for enforcing virtual singletons and multitons without implementing the Singleton Pattern.
    /// </summary>
    public interface IInstanceBinder : IContextSensitive, IDisposable
    {
        /// <summary>
        /// Create an anonymous binding for type TInstance.
        /// </summary>
        /// <typeparam name="TInstance">type of the binding</typeparam>
        void Bind<TInstance>();

        /// <summary>
        /// Create an anonymous binding for type TInstance associated with the given instance.
        /// </summary>
        /// <typeparam name="TInstance">type of the binding</typeparam>
        /// <param name="instance">instance of type TInstance</param>
        void Bind<TInstance>(TInstance instance);

        /// <summary>
        /// Create an ID'd binding for type TInstance.
        /// </summary>
        /// <typeparam name="TInstance">type of the binding</typeparam>
        /// <typeparam name="TKey">type of the identifier</typeparam>
        /// <param name="key">identifier of the instance</param>
        void Bind<TInstance, TKey>(TKey key);

        /// <summary>
        /// Create an ID'd binding for type TInstance associated with the given instance.
        /// </summary>
        /// <typeparam name="TInstance">type of the binding</typeparam>
        /// <typeparam name="TKey">type of the identifier</typeparam>
        /// <param name="instance">instance of type TInstance</param>
        /// <param name="key">identifier of the instance</param>
        void Bind<TInstance, TKey>(TInstance instance, TKey key);

        /// <summary>
        /// Remove an anonymous binding for type TInstance.
        /// </summary>
        /// <typeparam name="TInstance">type of the binding</typeparam>
        void Unbind<TInstance>();

        /// <summary>
        /// Remove a named binding for type TInstance.
        /// </summary>
        /// <typeparam name="TInstance">type of the binding</typeparam>
        /// <typeparam name="TKey">type of the key</typeparam>
        /// <param name="key">identifier of the instance</param>
        void Unbind<TInstance, TKey>(TKey key);

        /// <summary>
        /// Get the anonymous instance value bound to type TInstance.
        /// </summary>
        /// <typeparam name="TInstance">type of the binding</typeparam>
        /// <returns>instance of TInstance</returns>
        TInstance Get<TInstance>();

        /// <summary>
        /// Get the named instance value bound to type TInstance.
        /// </summary>
        /// <typeparam name="TInstance">type of the binding</typeparam>
        /// <typeparam name="TKey">type of the identifier</typeparam>
        /// <param name="key">identidier of the instance</param>
        /// <returns>instance of TInstance</returns>
        TInstance Get<TInstance, TKey>(TKey key);
    }
}
