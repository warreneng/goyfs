﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    /// <summary>
    /// Abstract implementation of ICommand that takes 0 arguments
    /// and whose execution can be iterated asynchronously.
    /// </summary>
    public abstract class AsyncCommand : ICommand
    {
        /// \copydoc ICommand::Context
        public Context Context { protected get; set; }

        /// <summary>
        /// \copybrief ICommand::Execute()
        /// </summary>
        /// <exception cref="InvalidOperationException">invoking without IAsyncCommandRunner available</exception>
        public void Execute()
        {
            IAsyncExecutor executor = null;
            try
            {
                executor = this.Context.InstanceBinder.Get<IAsyncExecutor>();
            }
            catch (KeyNotFoundException e)
            {
                throw new InvalidOperationException("AsyncCommand requires an IAsyncExecutor to be bound to an IInstanceBinder within Context scope.", e);
            }
            executor.Run(this.Execution());
        }

        /// <summary>
        /// Enumerable implementation of AsyncCommand's unit of work.
        /// </summary>
        /// <returns>IEnumerator to be iterated until execution is complete</returns>
        protected abstract IEnumerator Execution();
    }

    /// <summary>
    /// Abstract implementation of ICommand that takes 1 argument
    /// and whose execution can be iterated asynchronously.
    /// </summary>
    /// <typeparam name="T1">first argument type</typeparam>
    public abstract class AsyncCommand<T1> : ICommand<T1>
    {
        /// \copydoc ICommand<T1>::Context
        public Context Context { protected get; set; }

        /// <summary>
        /// \copybrief ICommand<T1>::Execute(T1 arg1)
        /// </summary>
        /// <exception cref="InvalidOperationException">invoking without IAsyncCommandRunner available</exception>
        public void Execute(T1 arg1)
        {
            IAsyncExecutor executor = null;
            try
            {
                executor = this.Context.InstanceBinder.Get<IAsyncExecutor>();
            }
            catch (KeyNotFoundException e)
            {
                throw new InvalidOperationException("AsyncCommand requires an IAsyncExecutor to be bound to an IInstanceBinder within Context scope.", e);
            }
            executor.Run(this.Execution(arg1));
        }

        /// <summary>
        /// Enumerable implementation of AsyncCommand's unit of work.
        /// </summary>
        /// <param name="arg1">first argument</param>
        /// <returns>IEnumerator to be iterated until execution is complete</returns>
        protected abstract IEnumerator Execution(T1 arg1);
    }

    /// <summary>
    /// Abstract implementation of ICommand that takes 2 arguments
    /// and whose execution can be iterated asynchronously.
    /// </summary>
    /// <typeparam name="T1">first argument type</typeparam>
    /// <typeparam name="T2">second argument type</typeparam>
    public abstract class AsyncCommand<T1, T2> : ICommand<T1, T2>
    {
        /// \copydoc ICommand<T1, T2>::Context
        public Context Context { protected get; set; }

        /// <summary>
        /// \copybrief ICommand<T1, T2>::Execute(T1 arg1, T2 arg2)
        /// </summary>
        /// <exception cref="InvalidOperationException">invoking without IAsyncCommandRunner available</exception>
        public void Execute(T1 arg1, T2 arg2)
        {
            IAsyncExecutor executor = null;
            try
            {
                executor = this.Context.InstanceBinder.Get<IAsyncExecutor>();
            }
            catch (KeyNotFoundException e)
            {
                throw new InvalidOperationException("AsyncCommand execution requires an IAsyncCommandRunner to be bound to an IInstanceBinder within Context scope.", e);
            }
            executor.Run(this.Execution(arg1, arg2));
        }

        /// <summary>
        /// Enumerable implementation of AsyncCommand's unit of work.
        /// </summary>
        /// <param name="arg1">first argument</param>
        /// <param name="arg2">second argument</param>
        /// <returns>IEnumerator to be iterated until execution is complete</returns>
        protected abstract IEnumerator Execution(T1 arg1, T2 arg2);
    }

    /// <summary>
    /// Abstract implementation of ICommand that takes 3 arguments
    /// and whose execution can be iterated asynchronously.
    /// </summary>
    /// <typeparam name="T1">first argument type</typeparam>
    /// <typeparam name="T2">second argument type</typeparam>
    /// <typeparam name="T3">third argument type</typeparam>
    public abstract class AsyncCommand<T1, T2, T3> : ICommand<T1, T2, T3>
    {
        /// \copydoc ICommand<T1, T2, T3>::Context
        public Context Context { protected get; set; }

        /// <summary>
        /// \copybrief ICommand<T1, T2, T3>::Execute(T1 arg1, T2 arg2, T3 arg3)
        /// </summary>
        /// <exception cref="InvalidOperationException">invoking without IAsyncCommandRunner available</exception>
        public void Execute(T1 arg1, T2 arg2, T3 arg3)
        {
            IAsyncExecutor executor = null;
            try
            {
                executor = this.Context.InstanceBinder.Get<IAsyncExecutor>();
            }
            catch (KeyNotFoundException e)
            {
                throw new InvalidOperationException("AsyncCommand execution requires an IAsyncCommandRunner to be bound to an IInstanceBinder within Context scope.", e);
            }
            executor.Run(this.Execution(arg1, arg2, arg3));
        }

        /// <summary>
        /// Enumerable implementation of AsyncCommand's unit of work.
        /// </summary>
        /// <param name="arg1">first argument</param>
        /// <param name="arg2">second argument</param>
        /// <param name="arg3">third argument</param>
        /// <returns>IEnumerator to be iterated until execution is complete</returns>
        protected abstract IEnumerator Execution(T1 arg1, T2 arg2, T3 arg3);
    }

    /// <summary>
    /// Abstract implementation of ICommand that takes 4 arguments
    /// and whose execution can be iterated asynchronously.
    /// </summary>
    /// <typeparam name="T1">first argument type</typeparam>
    /// <typeparam name="T2">second argument type</typeparam>
    /// <typeparam name="T3">third argument type</typeparam>
    /// <typeparam name="T4">fourth argument type</typeparam>
    public abstract class AsyncCommand<T1, T2, T3, T4> : ICommand<T1, T2, T3, T4>
    {
        /// \copydoc ICommand<T1, T2, T3, T4>::Context
        public Context Context { protected get; set; }

        /// <summary>
        /// \copybrief ICommand<T1, T2, T3, T4>::Execute(T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        /// </summary>
        /// <exception cref="InvalidOperationException">invoking without IAsyncCommandRunner available</exception>
        public void Execute(T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        {
            IAsyncExecutor executor = null;
            try
            {
                executor = this.Context.InstanceBinder.Get<IAsyncExecutor>();
            }
            catch (KeyNotFoundException e)
            {
                throw new InvalidOperationException("AsyncCommand execution requires an IAsyncCommandRunner to be bound to an IInstanceBinder within Context scope.", e);
            }
            executor.Run(this.Execution(arg1, arg2, arg3, arg4));
        }

        /// <summary>
        /// Enumerable implementation of AsyncCommand's unit of work.
        /// </summary>
        /// <param name="arg1">first argument</param>
        /// <param name="arg2">second argument</param>
        /// <param name="arg3">third argument</param>
        /// <param name="arg4">fourth argument</param>
        /// <returns>IEnumerator to be iterated until execution is complete</returns>
        protected abstract IEnumerator Execution(T1 arg1, T2 arg2, T3 arg3, T4 arg4);
    }
}
