﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs
{
    /// <summary>
    /// Abstract implementation of ICommand that executes with 0 arguments.
    /// </summary>
    public abstract class Command : ICommand
    {
        /// \copydoc ICommand::Context
        public Context Context { protected get; set; }

        /// \copydoc ICommand::Execute()
        public abstract void Execute();
    }

    /// <summary>
    /// Abstract implementation of ICommand that executes with 1 argument.
    /// </summary>
    /// <typeparam name="T1">first argument type</typeparam>
    public abstract class Command<T1> : ICommand<T1>
    {
        /// \copydoc ICommand<T1>::Context
        public Context Context { protected get; set; }

        /// \copydoc ICommand<T1>::Execute(T1 arg1)
        public abstract void Execute(T1 arg1);
    }

    /// <summary>
    /// Abstract implementation of ICommand that executes with 2 arguments.
    /// </summary>
    /// <typeparam name="T1">first argument type</typeparam>
    /// <typeparam name="T2">second argument type</typeparam>
    public abstract class Command<T1, T2> : ICommand<T1, T2>
    {
        /// \copydoc ICommand<T1,T2>::Context
        public Context Context { protected get; set; }

        /// \copydoc ICommand<T1,T2>::Execute(T1 arg1, T2 arg2)
        public abstract void Execute(T1 arg1, T2 arg2);
    }

    /// <summary>
    /// Abstract implementation of ICommand that executes with 3 arguments.
    /// </summary>
    /// <typeparam name="T1">first argument type</typeparam>
    /// <typeparam name="T2">second argument type</typeparam>
    /// <typeparam name="T3">third argument type</typeparam>
    public abstract class Command<T1, T2, T3> : ICommand<T1, T2, T3>
    {
        /// \copydoc ICommand<T1,T2,T3>::Context
        public Context Context { protected get; set; }

        /// \copydoc ICommand<T1,T2,T3>::Execute(T1 arg1, T2 arg2, T3 arg3)
        public abstract void Execute(T1 arg1, T2 arg2, T3 arg3);
    }

    /// <summary>
    /// Abstract implementation of ICommand that executes with 4 arguments.
    /// </summary>
    /// <typeparam name="T1">first argument type</typeparam>
    /// <typeparam name="T2">second argument type</typeparam>
    /// <typeparam name="T3">third argument type</typeparam>
    /// <typeparam name="T4">fourth argument type</typeparam>
    public abstract class Command<T1, T2, T3, T4> : ICommand<T1, T2, T3, T4>
    {
        /// \copydoc ICommand<T1,T2,T3,T4>::Context
        public Context Context { protected get; set; }

        /// \copydoc ICommand<T1,T2,T3,T4>::Execute(T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        public abstract void Execute(T1 arg1, T2 arg2, T3 arg3, T4 arg4);
    }
}
