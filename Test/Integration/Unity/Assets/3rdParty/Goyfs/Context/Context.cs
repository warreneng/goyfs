﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs
{
    using System;

    /// <summary>
    /// Context establishes a scope for class instances bound to it.
    /// Contexts can be linked together to form a tree, allowing dependencies
    /// unfulfilled in child Contexts to be satisfied by parent Contexts.
    /// </summary>
    public class Context : Disposable
    {
        /// <summary>
        /// Parent of this Context.
        /// It defines the next broader scope.
        /// </summary>
        public Context Parent { get; private set; }

        /// <summary>
        /// IInstanceBinder that manages the class instances bound to this Context.
        /// </summary>
        public IInstanceBinder InstanceBinder { get; private set; }

        /// <summary>
        /// IMediationBinder that handles view/mediation pairing in this Context.
        /// </summary>
        public IMediationBinder MediationBinder { get; private set; }

        /// <summary>
        /// Event invoked when this Context is being disposed.
        /// Allows listeners to clean up before the binders are disposed of.
        /// </summary>
        public event Action Disposing;

        /// <summary>
        /// Constructs a new instance of Context.
        /// </summary>
        /// <param name="instanceBinder">IInstanceBinder to handle instance bindings in this Context</param>
        /// <param name="mediationBinder">IMediationBinder to handle mediation bindings in this Context</param>
        /// <param name="parent">Context that parents this Context</param>
        public Context(IInstanceBinder instanceBinder, IMediationBinder mediationBinder, Context parent = null) : base()
        {
            this.InstanceBinder = instanceBinder;
            this.InstanceBinder.Context = this;

            this.MediationBinder = mediationBinder;
            this.MediationBinder.Context = this;

            this.Parent = parent;
        }

        /// \copydoc Disposable::OnDispose
        protected override void OnDispose(bool invokedByUser)
        {
            if (!this.Disposed)
            {
                if (invokedByUser)
                {
                    try
                    {
                        this.Disposing?.Invoke();
                    }
                    finally
                    {
                        this.MediationBinder.Dispose();
                        this.InstanceBinder.Dispose();
                    }
                }
            }
            
            this.Parent = null;
            this.Disposing = null;
        }
    }
}
