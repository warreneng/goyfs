﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs.Unity
{
    using Goyfs;
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// UnityMediationBinder features a deferred mediations queue for views passed to this.Mediate()
    /// prior to a call to Start(). The Start() feature allows all views to bind values to
    /// a ContextMaterial's IInstanceBinder during their Awake() method before mediators are created
    /// and attempt accessing those bindings.
    /// </summary>
    public class UnityMediationBinder : MediationBinder
    {
        /// <summary>
        /// List of views passed to Mediate() prior to this UnityMediationBinder being started.
        /// </summary>
        private List<object> earlyViews;
        
        /// <summary>
        /// Tracks whether or not this.Start() has been called yet.
        /// </summary>
        private bool started;

        /// <summary>
        /// Begins mediation of early views queued for deferred mediation, and immediate mediation of future calls to Mediate()
        /// </summary>
        public void Start()
        {
            if (!this.started)
            {
                GoyfsDebug.Log($"[Goyfs] Frame {Time.frameCount}: UnityMediationBinder in Context {this.Context.GetType().Name} starting");
                this.started = true;

                // mediate all views queued from calls to Mediate() prior to Start()
                while (this.earlyViews != null && this.earlyViews.Count > 0)
                {
                    object obj = this.earlyViews[0];
                    if (!this.Mediate(obj))
                    {
                        GoyfsDebug.Warn($"[Goyfs] Mediation of {obj.GetType().Name} failed via UnityMediationBinder in Context {this.Context.GetType().Name}");
                    }
                    this.earlyViews.RemoveAt(0);
                }
            }
            else
            {
                GoyfsDebug.Log($"[Goyfs] Frame {Time.frameCount}: UnityMediationBinder in Context {this.Context.GetType().Name} already started");
            }
        }

        /// \copydoc MediationBinder::Mediate(object view)
        public override bool Mediate(object view)
        {
            if (!this.started)
            {
                // queue view for deferred mediation upon Start()
                GoyfsDebug.Log($"[Goyfs] {Time.frameCount}: UnityMediationBinder in Context {this.Context.GetType().Name} not started, adding {view.GetType().Name} to mediation queue");
                this.AssertUndisposed();
                this.earlyViews = this.earlyViews ?? new List<object>();
                this.earlyViews.Add(view);
                return true;
            }
            else
            {
                // mediation can be attempted immediately once started
                GoyfsDebug.Log($"[Goyfs] {Time.frameCount}: UnityMediationBinder in Context {this.Context.GetType().Name} running, attempting mediation of {view.GetType().Name}");
                return base.Mediate(view);
            }
        }

        /// \copydoc MediationBinder::Unmediate(object view)
        public override bool Unmediate(object view)
        {
            if (!this.started)
            {
                // remove view from deferred mediation queue if present
                GoyfsDebug.Log($"[Goyfs] {Time.frameCount}: UnityMediationBinder in Context {this.Context.GetType().Name} not started, removing {view.GetType().Name} from mediation queue");
                this.AssertUndisposed();
                return this.earlyViews == null || this.earlyViews.Remove(view);
            }
            else
            {
                // view would already have a mediator if possible
                GoyfsDebug.Log($"[Goyfs] {Time.frameCount}: UnityMediationBinder in Context {this.Context.GetType().Name} running, unmediating {view.GetType().Name}");
                return base.Unmediate(view);
            }
        }
    }
}
