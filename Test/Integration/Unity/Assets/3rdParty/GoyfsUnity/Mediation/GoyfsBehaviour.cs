﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs.Unity
{
    using Goyfs;
    using UnityEngine;

    /// <summary>
    /// GoyfsBehaviours hook into a ContextMaterial at runtime.
    /// </summary>
    public abstract class GoyfsBehaviour : MonoBehaviour
    {
        /// <summary>
        /// The ContextMaterial this GoyfsBehaviour will operate within.
        /// </summary>
        [SerializeField, Tooltip("The ContextMaterial this GoyfsBehaviour will operate within")]
        private ContextMaterial material;

        /// <summary>
        /// Tracks whether or not UnityEngine has called this.Start() yet.
        /// </summary>
        private bool started;

        /// <summary>
        /// Sets this GoyfsBehaviour's ContextMaterial and detaches from the old value.
        /// </summary>
        public virtual ContextMaterial Material
        {
            set
            {
                if (this.material != null)
                {
                    GoyfsDebug.Log($"[Goyfs] GoyfsBehaviour {this.name} removing reference to ContextMaterial {this.material.name}");
                    this.material.References.Remove(this);
                }

                this.material = value;

                if (value != null)
                {
                    GoyfsDebug.Log($"[Goyfs] GoyfsBehaviour {this.name} adding reference to ContextMaterial {this.material.name}");
                    value.References.Add(this);
                    if (this.started)
                    {
                        value.Start();  // we must start the ContextMaterial immediately if we've already started
                    }
                }
            }
        }

        /// <summary>
        /// UnityEngine.MonoBehaviour message.
        /// </summary>
        protected virtual void Awake()
        {
            GoyfsDebug.Log($"[Goyfs] GoyfsBehaviour {this.name} Awake()");
            this.Material = this.material; // attach to serialized material
        }

        /// <summary>
        /// UnityEngine.MonoBehaviour message.
        /// </summary>
        protected virtual void Start()
        {
            GoyfsDebug.Log($"[Goyfs] GoyfsBehaviour {this.name} Start()");
            this.started = true;
            if (this.material != null)
            {
                GoyfsDebug.Log($"[Goyfs] GoyfsBehaviour {this.name} starting ContextMaterial {this.material.name}");
                this.material.Start();
            }
        }

        /// <summary>
        /// UnityEngine.MonoBehaviour message.
        /// </summary>
        protected virtual void OnDestroy()
        {
            GoyfsDebug.Log($"[Goyfs] GoyfsBehaviour {this.name} OnDestroy");
            this.Material = null;   // cleans up references
        }
    }
}
