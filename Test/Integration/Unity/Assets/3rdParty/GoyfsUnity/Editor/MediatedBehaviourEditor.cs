﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs.Unity.Editor
{
    using Goyfs.Unity;
    using System;
    using System.Collections.Generic;
    using UnityEditor;
    using UnityEngine;

    /// <summary>
    /// MediatedBehaviourEditor is a custom Unity Inspector window for MonoBehaviours extending MediatedBehaviour.
    /// It displays at runtime the ContextView the MediatedBehaviour registered with, and the type it is mediated by.
    /// 
    /// Hat tip CE.
    /// </summary>
    [CustomEditor(typeof(MediatedBehaviour), true)]
    public class MediatedBehaviourEditor : Editor
    {
        #region public
        public override void OnInspectorGUI()
        {
            // Draw the "Mediation" foldout only if the application is playing. (Views are only mediated when playing.)
            if (Application.isPlaying)
            {
                if (m_foldout = EditorGUILayout.Foldout(m_foldout, c_labelMediationFoldout, true))
                {
                    ++EditorGUI.indentLevel;
                    GUI.enabled = false;
                    EditorGUILayout.LabelField(c_labelContextField, m_context != null ? m_context.GetType().Name : "None");
                    EditorGUILayout.LabelField(c_labelMediatorField, m_mediatorType != null ? m_mediatorType.Name : "None");
                    GUI.enabled = true;
                    --EditorGUI.indentLevel;
                }
            }

            // Draw the rest of the inspector as usual.
            base.OnInspectorGUI();
        }
        #endregion // public

        #region private
        private Context m_context;
        private Type m_mediatorType;

        private bool m_foldout;
        private const string c_labelMediationFoldout = "Mediation";
        private const string c_labelContextField = "Context";
        private const string c_labelMediatorField = "Mediator";

        private void OnEnable()
        {
            // View are only mediated when playing, so searching outside play mode would be for nothing.
            if (Application.isPlaying)
            {
                m_context = null;
                m_mediatorType = null;

                // Sneak reference to the inspected view's ContextMaterial.
                MediatedBehaviour mediatedBehaviour = (MediatedBehaviour)target;
                ContextMaterial contextMaterial = GoyfsUnityEditor.GetPrivateFieldValue<ContextMaterial>(mediatedBehaviour, "material");
                FindBinding(mediatedBehaviour, contextMaterial);
            }
        }

        private void OnDisable()
        {
            m_context = null;
            m_mediatorType = null;
        }

        private void FindBinding(MediatedBehaviour mediatedBehaviour, ContextMaterial contextMaterial)
        {
            if (contextMaterial)
            {
                // sneak reference to mediatedBehaviour's ContextMaterial's Context's MediationBinder's type-binding for the given view
                contextMaterial.References.Add(this);
                Dictionary<Type, Type> mediationBindings = GoyfsUnityEditor.GetPrivateFieldValue<Dictionary<Type, Type>>(contextMaterial.Context.MediationBinder, "bindings");
                if (mediationBindings != null && mediationBindings.TryGetValue(mediatedBehaviour.GetType(), out m_mediatorType))
                {
                    m_context = contextMaterial.Context;
                }
                contextMaterial.References.Remove(this);

                if (m_mediatorType == null)
                {
                    // continue search to parent
                    FindBinding(mediatedBehaviour, GoyfsUnityEditor.GetPrivateFieldValue<ContextMaterial>(contextMaterial, "parent"));
                }
            }
        }
        #endregion // private
    }
}
