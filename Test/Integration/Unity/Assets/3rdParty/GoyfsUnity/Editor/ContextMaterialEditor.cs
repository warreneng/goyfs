﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs.Unity.Editor
{
    using Goyfs;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(ContextMaterial), true)]
    public class ContextMaterialEditor : Editor
    {
        // local state
        private IDictionary<Type, object>           instanceBindings;
        private IDictionary<Type, Type>             mediationBindings;
        private IDictionary<object, IMediator>      mediations;
        private bool                                showInstanceBindings;
        private bool                                showMediationBindings;
        private bool                                showMediations;

        private Dictionary<IEnumerable, bool> showEnumerableVals;

        public override void OnInspectorGUI()
        {
            // draw the default GUI first
            base.OnInspectorGUI();

            // draw a live view of instance and mediation binders
            if (Application.isPlaying)
            {
                if (this.showInstanceBindings = EditorGUILayout.Foldout(this.showInstanceBindings, "Instance Binder", true))
                {
                    this.DrawInstanceBinderGUI();
                }

                if (this.showMediationBindings = EditorGUILayout.Foldout(this.showMediationBindings, "Mediation Binder", true))
                {
                    this.DrawMediationBinderGUI();
                }

                if (this.showMediations = EditorGUILayout.Foldout(this.showMediations, "Mediations", true))
                {
                    this.DrawMediationsGUI();
                }
            }
        }

        private void OnEnable()
        {
            // only show live binders during play mode
            if (Application.isPlaying)
            {
                ContextMaterial contextMaterial = this.target as ContextMaterial;
                contextMaterial.References.Add(this);
                Context context = contextMaterial.Context;
                
                this.instanceBindings =     GoyfsUnityEditor.GetPrivateFieldValue<Dictionary<Type, object>>(context.InstanceBinder, "bindings");
                this.mediationBindings =    GoyfsUnityEditor.GetPrivateFieldValue<Dictionary<Type, Type>>(context.MediationBinder, "bindings");
                this.mediations =           GoyfsUnityEditor.GetPrivateFieldValue<Dictionary<object, IMediator>>(context.MediationBinder, "mediators");
                this.showEnumerableVals =   new Dictionary<IEnumerable, bool>();
            }
        }

        private void OnDisable()
        {
            (this.target as ContextMaterial).References.Remove(this);

            this.instanceBindings = null;
            this.mediationBindings = null;
            this.mediations = null;
            if (this.showEnumerableVals != null)
            {
                this.showEnumerableVals.Clear();
                this.showEnumerableVals = null;
            }
        }

        private void DrawInstanceBinderGUI()
        {
            ++EditorGUI.indentLevel;
            {
                foreach (KeyValuePair<Type, object> typeToInstanceBinding in this.instanceBindings)
                {
                    Type type = typeToInstanceBinding.Key;
                    object instanceBinding = typeToInstanceBinding.Value;

                    try
                    {
                        // Singleton?
                        object instance = GoyfsUnityEditor.GetPrivateFieldValue<object>(instanceBinding, "instance");

                        EditorGUILayout.BeginHorizontal();
                        {
                            EditorGUILayout.LabelField(type.Name);
                            this.DrawValueGUI(instance);
                        }
                        EditorGUILayout.EndHorizontal();
                    }
                    catch { }
                    try
                    {
                        // Multiton?
                        object instances = GoyfsUnityEditor.GetPrivateFieldValue<object>(instanceBinding, "instances");
                        foreach (DictionaryEntry entry in instances as IDictionary)
                        {
                            EditorGUILayout.BeginHorizontal();
                            {
                                EditorGUILayout.LabelField(string.Format("{0} ({1})", type.Name, entry.Key));
                                this.DrawValueGUI(entry.Value);
                            }
                            EditorGUILayout.EndHorizontal();
                        }
                    }
                    catch { }
                }
            }
            --EditorGUI.indentLevel;
        }

        private void DrawMediationBinderGUI()
        {
            ++EditorGUI.indentLevel;
            {
                foreach (KeyValuePair<Type, Type> viewTypeToMediatorType in mediationBindings)
                {
                    GUILayout.BeginHorizontal();
                    {
                        EditorGUILayout.LabelField(viewTypeToMediatorType.Key.Name);
                        EditorGUILayout.LabelField(viewTypeToMediatorType.Value.Name);
                    }
                    GUILayout.EndHorizontal();
                }
            }
            --EditorGUI.indentLevel;
        }

        private void DrawMediationsGUI()
        {
            ++EditorGUI.indentLevel;
            {
                foreach (KeyValuePair<object, IMediator> mediation in this.mediations)
                {
                    GUILayout.BeginHorizontal();
                    {
                        this.DrawValueGUI(mediation.Key, true);
                        EditorGUILayout.LabelField(mediation.Value.GetType().Name);
                    }
                    GUILayout.EndHorizontal();
                }
            }
            --EditorGUI.indentLevel;
        }

        private void DrawValueGUI(object value, bool debug_addVerticalSpacing = false)
        {
            if (value is UnityEngine.Object)
            {
                // display clickable reference field to object
                EditorGUI.BeginDisabledGroup(true);
                EditorGUILayout.ObjectField((UnityEngine.Object)value, value.GetType(), true, GUILayout.MinWidth(200f));
                EditorGUI.EndDisabledGroup();
            }
            else if (value is ICollection)
            {
                // display expandable list of ICollection's elements
                IEnumerable iEnumerable = (IEnumerable)value;
                bool show = false;
                if (!this.showEnumerableVals.TryGetValue(iEnumerable, out show))
                {
                    this.showEnumerableVals.Add(iEnumerable, false);
                }
                if (this.showEnumerableVals[iEnumerable] = EditorGUILayout.Foldout(show, new GUIContent(value.ToString())))
                {
                    EditorGUILayout.BeginVertical();
                    {
                        for (int i = 0; i < 3 && debug_addVerticalSpacing; ++i)
                        {
                            EditorGUILayout.Space();
                        }
                        foreach (object element in iEnumerable)
                        {
                            this.DrawValueGUI(element);
                        }
                    }
                    EditorGUILayout.EndVertical();
                }
            }
            else if (value != null)
            {
                // display object's ToString()
                EditorGUILayout.LabelField(new GUIContent(string.Format("{0} ({1})", value.ToString(), value.GetHashCode())));
            }
            else
            {
                // instance value not available
                EditorGUILayout.LabelField(new GUIContent("(null)"));
            }
        }
    }
}
