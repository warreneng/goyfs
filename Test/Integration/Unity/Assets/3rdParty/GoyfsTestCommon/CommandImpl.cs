﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs.Test.Common
{
    using Goyfs;
    using System;

    public abstract class Command0Impl : Command
    {
        public int LastExecute { get; private set; }
        public override void Execute() { this.LastExecute = Sequence.Next; }

        /// <summary>
        /// Command0Impl.A tracks its last constructed instance
        /// </summary>
        public class A : Command0Impl
        {
            public static A Instance { get; set; }
            public new Context Context { get => base.Context; set => base.Context = value; }
            public override void Execute()
            {
                base.Execute();
                Command0Impl.A.Instance = this;
            }
        }

        /// <summary>
        /// Commmand0Impl.B tracks its last constructed instance
        /// </summary>
        public class B : Command0Impl
        {
            public static B Instance { get; set; }
            public new Context Context { get => base.Context; set => base.Context = value; }
            public override void Execute()
            {
                base.Execute();
                Command0Impl.B.Instance = this;
            }
        }

        /// <summary>
        /// Specialized command that removes an ICommand from a Signal in the Context's IInstanceBinder.
        /// </summary>
        public class Remove<TSignal, TCommand> : Command0Impl where TSignal : Signal where TCommand : ICommand
        {
            public static Remove<TSignal, TCommand> Instance { get; set; }
            public new Context Context { get => base.Context; set => base.Context = value; }
            public override void Execute()
            {
                base.Execute();
                Remove<TSignal, TCommand>.Instance = this;
                this.Context.InstanceBinder.Get<TSignal>().RemoveCommand<TCommand>();
            }
        }
    }

    public class Command1Impl<T1> : Command<T1>
    {
        public int LastExecute { get; private set; }
        public T1 Arg1 { get; private set; }

        public override void Execute(T1 arg1)
        {
            this.LastExecute = Sequence.Next;
            this.Arg1 = arg1;
        }

        /// <summary>
        /// Command1Impl.A checks the arguments passed to it against expected value(s)
        /// </summary>
        public class A : Command1Impl<T1>
        {
            public static A Instance { get; set; }
            public new Context Context { get => base.Context; set => base.Context = value; }
            public override void Execute(T1 arg1)
            {
                base.Execute(arg1);
                A.Instance = this;
            }
        }

        /// <summary>
        /// Command1Impl.B checks the arguments passed to it against expected value(s)
        /// </summary>
        public class B : Command1Impl<T1>
        {
            public static B Instance { get; set; }
            public new Context Context { get => base.Context; set => base.Context = value; }
            public override void Execute(T1 arg1)
            {
                base.Execute(arg1);
                B.Instance = this;
            }
        }

        /// <summary>
        /// Specialized command that removes an ICommand from a Signal in the Context's IInstanceBinder.
        /// </summary>
        public class Remove<TSignal, TCommand> : Command1Impl<T1> where TSignal : Signal<T1> where TCommand : ICommand<T1>
        {
            public static Remove<TSignal, TCommand> Instance;
            public override void Execute(T1 arg1)
            {
                base.Execute(arg1);
                Remove<TSignal, TCommand> Instance = this;
                this.Context.InstanceBinder.Get<TSignal>().RemoveCommand<TCommand>();
            }
        }
    }

    public class Command2Impl<T1, T2> : Command <T1, T2>
    {
        public int LastExecute { get; private set; }
        public T1 Arg1 { get; private set; }
        public T2 Arg2 { get; private set; }

        public override void Execute(T1 arg1, T2 arg2)
        {
            this.LastExecute = Sequence.Next;
            this.Arg1 = arg1;
            this.Arg2 = arg2;
        }

        /// <summary>
        /// Command2Impl.A tracks its last constructed instance
        /// </summary>
        public class A : Command2Impl<T1, T2>
        {
            public static A Instance { get; set; }
            public new Context Context { get => base.Context; set => base.Context = value; }
            public override void Execute(T1 arg1, T2 arg2)
            {
                base.Execute(arg1, arg2);
                A.Instance = this;
            }
        }

        /// <summary>
        /// Command2Impl.B tracks its last constructed instance
        /// </summary>
        public class B : Command2Impl<T1, T2>
        {
            public static B Instance { get; set; }
            public new Context Context { get => base.Context; set => base.Context = value; }
            public override void Execute(T1 arg1, T2 arg2)
            {
                base.Execute(arg1, arg2);
                B.Instance = this;
            }
        }

        /// <summary>
        /// Specialized command that removes an ICommand from a Signal in the Context's IInstanceBinder.
        /// </summary>
        public class Remove<TSignal, TCommand> : Command2Impl<T1, T2> where TSignal : Signal<T1, T2> where TCommand : ICommand<T1, T2>
        {
            public static Remove<TSignal, TCommand> Instance;
            public override void Execute(T1 arg1, T2 arg2)
            {
                base.Execute(arg1, arg2);
                Remove<TSignal, TCommand> Instance = this;
                this.Context.InstanceBinder.Get<TSignal>().RemoveCommand<TCommand>();
            }
        }
    }

    public class Command3Impl<T1, T2, T3> : Command<T1, T2, T3>
    {
        public int LastExecute { get; private set; }
        public T1 Arg1 { get; private set; }
        public T2 Arg2 { get; private set; }
        public T3 Arg3 { get; private set; }

        public override void Execute(T1 arg1, T2 arg2, T3 arg3)
        {
            this.LastExecute = Sequence.Next;
            this.Arg1 = arg1;
            this.Arg2 = arg2;
            this.Arg3 = arg3;
        }

        /// <summary>
        /// Command3Impl.A tracks its last constructed instance
        /// </summary>
        public class A : Command3Impl<T1, T2, T3>
        {
            public static A Instance { get; set; }
            public new Context Context { get => base.Context; set => base.Context = value; }
            public override void Execute(T1 arg1, T2 arg2, T3 arg3)
            {
                base.Execute(arg1, arg2, arg3);
                A.Instance = this;
            }
        }

        /// <summary>
        /// Command3Impl.B tracks its last constructed instance
        /// </summary>
        public class B : Command3Impl<T1, T2, T3>
        {
            public static B Instance { get; set; }
            public new Context Context { get => base.Context; set => base.Context = value; }
            public override void Execute(T1 arg1, T2 arg2, T3 arg3)
            {
                base.Execute(arg1, arg2, arg3);
                B.Instance = this;
            }
        }

        /// <summary>
        /// Specialized command that removes an ICommand from a Signal in the Context's IInstanceBinder.
        /// </summary>
        public class Remove<TSignal, TCommand> : Command3Impl<T1, T2, T3> where TSignal : Signal<T1, T2, T3> where TCommand : ICommand<T1, T2, T3>
        {
            public static Remove<TSignal, TCommand> Instance;
            public override void Execute(T1 arg1, T2 arg2, T3 arg3)
            {
                base.Execute(arg1, arg2, arg3);
                Remove<TSignal, TCommand> Instance = this;
                this.Context.InstanceBinder.Get<TSignal>().RemoveCommand<TCommand>();
            }
        }
    }

    public class Command4Impl<T1, T2, T3, T4> : Command<T1, T2, T3, T4>
    {
        public int LastExecute { get; private set; }
        public T1 Arg1 { get; private set; }
        public T2 Arg2 { get; private set; }
        public T3 Arg3 { get; private set; }
        public T4 Arg4 { get; private set; }
        public override void Execute(T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        {
            this.LastExecute = Sequence.Next;
            this.Arg1 = arg1;
            this.Arg2 = arg2;
            this.Arg3 = arg3;
            this.Arg4 = arg4;
        }

        /// <summary>
        /// Command4Impl.A tracks its last constructed instance
        /// </summary>
        public class A : Command4Impl<T1, T2, T3, T4>
        {
            public static A Instance { get; set; }
            public new Context Context { get => base.Context; set => base.Context = value; }
            public override void Execute(T1 arg1, T2 arg2, T3 arg3, T4 arg4)
            {
                base.Execute(arg1, arg2, arg3, arg4);
                A.Instance = this;
            }
        }

        /// <summary>
        /// Command4Impl.B tracks its last constructed instance
        /// </summary>
        public class B : Command4Impl<T1, T2, T3, T4>
        {
            public static B Instance { get; set; }
            public new Context Context { get => base.Context; set => base.Context = value; }
            public override void Execute(T1 arg1, T2 arg2, T3 arg3, T4 arg4)
            {
                base.Execute(arg1, arg2, arg3, arg4);
                B.Instance = this;
            }
        }

        /// <summary>
        /// Specialized command that removes an ICommand from a Signal in the Context's IInstanceBinder.
        /// </summary>
        public class Remove<TSignal, TCommand> : Command4Impl<T1, T2, T3, T4> where TSignal : Signal<T1, T2, T3, T4> where TCommand : ICommand<T1, T2, T3, T4>
        {
            public static Remove<TSignal, TCommand> Instance;
            public override void Execute(T1 arg1, T2 arg2, T3 arg3, T4 arg4)
            {
                base.Execute(arg1, arg2, arg3, arg4);
                Remove<TSignal, TCommand> Instance = this;
                this.Context.InstanceBinder.Get<TSignal>().RemoveCommand<TCommand>();
            }
        }
    }
}
