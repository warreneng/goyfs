﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs.Test.Common
{
    public interface IListener0
    {
        void Callback();
    }

    public interface IListener1<T1>
    {
        void Callback(T1 arg1);
    }

    public interface IListener2<T1, T2>
    {
        void Callback(T1 arg1, T2 arg2);
    }

    public interface IListener3<T1, T2, T3>
    {
        void Callback(T1 arg1, T2 arg2, T3 arg3);
    }

    public interface IListener4<T1, T2, T3, T4>
    {
        void Callback(T1 arg1, T2 arg2, T3 arg3, T4 arg4);
    }

    public class Listener0Impl : IListener0
    {
        public int LastCallback = 0;
        public void Callback() { this.LastCallback = Sequence.Next; }
    }

    public class Listener1Impl<T1> : IListener1<T1>
    {
        public int LastCallback = 0;
        public T1 Arg1;
        public void Callback(T1 arg1)
        {
            this.LastCallback = Sequence.Next;
            this.Arg1 = arg1;
        }
    }

    public class Listener2Impl<T1, T2> : IListener2<T1, T2>
    {
        public int LastCallback = 0;
        public T1 Arg1;
        public T2 Arg2;
        public void Callback(T1 arg1, T2 arg2)
        {
            this.LastCallback = Sequence.Next;
            this.Arg1 = arg1;
            this.Arg2 = arg2;
        }
    }

    public class Listener3Impl<T1, T2, T3> : IListener3<T1, T2, T3>
    {
        public int LastCallback = 0;
        public T1 Arg1;
        public T2 Arg2;
        public T3 Arg3;
        public void Callback(T1 arg1, T2 arg2, T3 arg3)
        {
            this.LastCallback = Sequence.Next;
            this.Arg1 = arg1;
            this.Arg2 = arg2;
            this.Arg3 = arg3;
        }
    }

    public class Listener4Impl<T1, T2, T3, T4> : IListener4<T1, T2, T3, T4>
    {
        public int LastCallback = 0;
        public T1 Arg1;
        public T2 Arg2;
        public T3 Arg3;
        public T4 Arg4;
        public void Callback(T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        {
            this.LastCallback = Sequence.Next;
            this.Arg1 = arg1;
            this.Arg2 = arg2;
            this.Arg3 = arg3;
            this.Arg4 = arg4;
        }
    }
}
