﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2017 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs.Test.Integration
{
    using Goyfs.Test.Common;
    using Goyfs.Unity;
    using UnityEngine;

    /// <summary>
    /// Test mediation of a UnityView added to the scene at runtime via prefab instantiation.
    /// 
    /// Assumptions:
    ///     1. Prefab has UnityViewImpl.A script attached to it.
    ///     2. ContextImpl.A has set up its IMediationBinder to medate UnityViewImpl.A
    /// </summary>
    public class Test_MediatePrefabView : MonoBehaviour
    {
        [SerializeField]
        private GameObject unityViewPrefab;

        [SerializeField]
        private ContextMaterial contextMaterialA;

        // created at runtime
        private MediatedBehaviour unityView;

        // test identification
        private Sequence.Local sequence = new Sequence.Local();

        private void Start()
        {
            // Create the view in a Start() method. The view should still get its Start() called prior to this.Update().
            // Even though instantiating within this.Update() fails the test, I don't believe this is cheating.
            // In this test case only, the Update method creates and tests for view being mediated *without giving up the thread*
            // which guarantees the view's Start() cannot occur. In practice, the view's Start() *will* be called in the same frame
            // right after this.Update() completes. This is perfectly fine.
            this.unityView = GameObject.Instantiate(this.unityViewPrefab).GetComponent<MediatedBehaviour>();
        }

        private void Update()
        {
            // test: mediation of views added to scene
            IntegrationTest.Assert(MediatorImpl.Instances.Exists((MediatorImpl.Instance x) =>
            {
                this.contextMaterialA.References.Add(this);
                bool result = this.unityView.Equals(x.View) && this.contextMaterialA.Context.Equals(x.Context);
                this.contextMaterialA.References.Remove(this);
                return result;
            }),
            this.sequence.Next.ToString());

            // test: unmediation via destruction of view
            GameObject.DestroyImmediate(this.unityView);
            IntegrationTest.Assert(MediatorImpl.Instances.Exists((MediatorImpl.Instance x) => { return this.unityView.Equals(x.View) && (x.Mediator as Disposable).Disposed; }), this.sequence.Next.ToString());

            // cleanup
            IntegrationTest.Pass(this.gameObject);
        }
    }
}
