﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2017 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs.Test.Integration
{
    using Goyfs.Test.Common;
    using Goyfs.Unity;
    using UnityEngine;

    /// <summary>
    /// Test UnmediatedBehaviour and MediatedBehaviour's wakeup ordering.
    /// Because UnmediatedBehaviour has access to its Context, it can bind instances to the InstanceBinder.
    /// We need that to occur in its Awake() so that by the time the mediation of MediatedBehaviour occurs,
    /// the instances are already there for use.
    /// 
    /// Assumptions:
    ///     1. unmediated attaches to a ContextMaterial with underlying ContextImpl.UnmediatedMediatedOrder Context
    ///     2. unmediated binds a reference to itself as a GoyfsBehaviour to its Context on Awake()
    ///     3. ContextImpl.UnmediatedMediatedOrder mediates MediatedBehaviour with MediatorImpl.FetchSingleton that'll fetch that GoyfsBehaviour from IInstanceBinder
    /// </summary>
    public class Test_UnmediatedMediatedOrder : MonoBehaviour
    {
        [SerializeField]
        private UnmediatedBehaviour_BindsSelf unmediated;

        [SerializeField]
        private MediatedBehaviour mediated;

        // test identification
        private Sequence.Local sequence = new Sequence.Local();

        private void Update()
        {
            // test: assert mediator was able to fetch the UnmediatedBehaviour that bound itself to the IInstanceBinder
            IntegrationTest.Assert(MediatorImpl.Instances.Exists((MediatorImpl.Instance x) =>
            {
                return
                    x.View == this.mediated
                    && x.Mediator is MediatorImpl.FetchSingleton<MediatedBehaviour, GoyfsBehaviour>
                    && (x.Mediator as MediatorImpl.FetchSingleton<MediatedBehaviour, GoyfsBehaviour>).FetchedObject == this.unmediated;
            }),
            this.sequence.Next.ToString());

            // cleanup
            GameObject.DestroyImmediate(this.unmediated);
            GameObject.DestroyImmediate(this.mediated);
            IntegrationTest.Pass(this.gameObject);
        }
    }
}
