﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2017 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs.Test.Integration
{
    using Goyfs.Test.Common;
    using Goyfs.Unity;
    using UnityEngine;

    /// <summary>
    /// Tests the ability of a mediator constructed at a leaf Context to get an object from
    /// the InstanceBinder that lives at the root of the Context tree.
    /// 
    /// Assumptions:
    ///     1. UnityView attaches to a ContextMaterial with underlying ContextImpl.MediateWithFetchInstance Context
    ///     2. That ContextMaterial's parent is a ContextMaterial whose context has bound a singleton int=42 to the IInstanceBinder
    ///     3. ContextImpl.MediateWithFetchInstance's IMediationBinder with mediate UnityView with MediatorImpl.FetchInstance
    ///     4. MediatorImpl.FetchInstance will fetch an int from the IInstanceBinder
    /// </summary>
    public class Test_FetchInstanceAtRootFromLeaf : MonoBehaviour
    {
        [SerializeField]
        private MediatedBehaviour unityView;

        // test identification
        private Sequence.Local sequence = new Sequence.Local();

        protected void Update()
        {
            // test: assert the mediator received the correct object from root context
            IntegrationTest.Assert(MediatorImpl.Instances.Exists((MediatorImpl.Instance x) =>
            {
                return
                    x.View == this.unityView
                    && x.Mediator is MediatorImpl.FetchSingleton<MediatedBehaviour, int>
                    && (int)(x.Mediator as MediatorImpl.FetchSingleton<MediatedBehaviour, int>).FetchedObject == 42;
            }),
            this.sequence.Next.ToString());

            // cleanup
            GameObject.DestroyImmediate(this.unityView);
            IntegrationTest.Pass(this.gameObject);
        }
    }
}
