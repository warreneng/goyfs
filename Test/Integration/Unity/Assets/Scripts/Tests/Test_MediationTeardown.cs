﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2017 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs.Test.Integration
{
    using Common;
    using Unity;
    using UnityEngine;

    /// <summary>
    /// Tests the disposal behavior of ContextMaterial combined with MediationBinder. When a ContextMaterial's last reference is eliminated,
    /// it is expected that the leaf context nodes are always disposed first. Practically this means IContextSensitive objects bound
    /// to the leaf contexts (e.g. Mediators) are afforded the chance to dispose of themselves before the ancestor contexts they might be dependent on are disposed.
    /// 
    /// Assumptions:
    ///     1. ContextMaterials are set up such that A parents B and D, and B parents C.
    ///     2. UnityViews A, B, C, and D specify their corresponding ContextMaterial A, B, C, D
    ///     3. ContextMaterial A mediates UnityViews with MediatorImpl.A
    /// </summary>
    public class Test_MediationTeardown : MonoBehaviour
    {
        [SerializeField]
        private MediatedBehaviour unityViewA;

        [SerializeField]
        private MediatedBehaviour unityViewB;

        [SerializeField]
        private MediatedBehaviour unityViewC;

        [SerializeField]
        private MediatedBehaviour unityViewD;

        [SerializeField]
        private ContextMaterial contextMaterialA;

        [SerializeField]
        private ContextMaterial contextMaterialB;

        [SerializeField]
        private ContextMaterial contextMaterialC;

        [SerializeField]
        private ContextMaterial contextMaterialD;

        // test identification
        private Sequence.Local sequence = new Sequence.Local();

        protected void Update()
        {
            this.contextMaterialA.References.Add(this);
            this.contextMaterialB.References.Add(this);
            this.contextMaterialC.References.Add(this);
            this.contextMaterialD.References.Add(this);
            Context contextA = this.contextMaterialA.Context;
            Context contextB = this.contextMaterialB.Context;
            Context contextC = this.contextMaterialC.Context;
            Context contextD = this.contextMaterialD.Context;
            this.contextMaterialA.References.Remove(this);
            this.contextMaterialB.References.Remove(this);
            this.contextMaterialC.References.Remove(this);
            this.contextMaterialD.References.Remove(this);

            MediatorImpl.Instance instanceA = MediatorImpl.Instances.Find((MediatorImpl.Instance x) => { return this.unityViewA.Equals(x.View); });
            MediatorImpl.Instance instanceB = MediatorImpl.Instances.Find((MediatorImpl.Instance x) => { return this.unityViewB.Equals(x.View); });
            MediatorImpl.Instance instanceC = MediatorImpl.Instances.Find((MediatorImpl.Instance x) => { return this.unityViewC.Equals(x.View); });
            MediatorImpl.Instance instanceD = MediatorImpl.Instances.Find((MediatorImpl.Instance x) => { return this.unityViewD.Equals(x.View); });

            // destroy views in arbitrary order to test contexts are kept alive by deepest reference so mediators can tear down properly
            GameObject.DestroyImmediate(this.unityViewB);   // D < A > b? > C
            IntegrationTest.Assert(!contextA.Disposed, this.sequence.Next.ToString());
            IntegrationTest.Assert(!contextB.Disposed, this.sequence.Next.ToString());
            IntegrationTest.Assert(!contextC.Disposed, this.sequence.Next.ToString());
            IntegrationTest.Assert(!contextD.Disposed, this.sequence.Next.ToString());

            GameObject.DestroyImmediate(this.unityViewA);   // D < a? > b? > C
            IntegrationTest.Assert(!contextA.Disposed, this.sequence.Next.ToString());
            IntegrationTest.Assert(!contextB.Disposed, this.sequence.Next.ToString());
            IntegrationTest.Assert(!contextC.Disposed, this.sequence.Next.ToString());
            IntegrationTest.Assert(!contextD.Disposed, this.sequence.Next.ToString());

            GameObject.DestroyImmediate(this.unityViewC);   // D < a? > b? > c?
            IntegrationTest.Assert(!contextA.Disposed, this.sequence.Next.ToString());
            IntegrationTest.Assert(contextB.Disposed, this.sequence.Next.ToString());
            IntegrationTest.Assert(contextC.Disposed, this.sequence.Next.ToString());
            IntegrationTest.Assert(!contextD.Disposed, this.sequence.Next.ToString());

            GameObject.DestroyImmediate(this.unityViewD);   // d? < a? > b? > c?
            IntegrationTest.Assert(contextA.Disposed, this.sequence.Next.ToString());
            IntegrationTest.Assert(contextB.Disposed, this.sequence.Next.ToString());
            IntegrationTest.Assert(contextC.Disposed, this.sequence.Next.ToString());
            IntegrationTest.Assert(contextD.Disposed, this.sequence.Next.ToString());

            // Verify mediator disposal order. Should be B/A/C/D
            IntegrationTest.Assert(instanceB.LastDispose < instanceA.LastDispose, this.sequence.Next.ToString());
            IntegrationTest.Assert(instanceA.LastDispose < instanceC.LastDispose, this.sequence.Next.ToString());
            IntegrationTest.Assert(instanceC.LastDispose < instanceD.LastDispose, this.sequence.Next.ToString());
            IntegrationTest.Assert(instanceD.LastDispose > instanceC.LastDispose, this.sequence.Next.ToString());

            // cleanup
            IntegrationTest.Pass(this.gameObject);
        }
    }
}
