﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2017 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs.Test.Integration
{
    using Goyfs.Test.Common;
    using Goyfs.Unity;
    using UnityEngine;

    /// <summary>
    /// Tests the behavior of the stock MediationBinder when injecting a Context into new Mediators.
    /// We expect the UnityView's specified ContextMaterial's Context to be injected into its Mediator
    /// even if the mediation binding is defined in an ancestor Context.
    /// 
    /// Assumptions:
    ///     1. ContextMaterialA parents ContextMaterialB
    ///     2. ContextMaterialA is set up to mediate UnityViews with MediatorImpl.A, while ContextMaterialB does not.
    ///     3. UnityViewB specifies ContextMaterialB for its Mediation
    /// </summary>
    public class Test_MediateLocalContextInjected : MonoBehaviour
    {
        [SerializeField]
        private ContextMaterial contextMaterialA;

        [SerializeField]
        private ContextMaterial contextMaterialB;

        [SerializeField]
        private MediatedBehaviour unityViewB;

        // test identification
        private Sequence.Local sequence = new Sequence.Local();

        private void Update()
        {
            // test mediation of view via ancestor Context's MediationBinder: mediator should get view's local context injected, not the ancestor Context
            IntegrationTest.Assert(MediatorImpl.Instances.Exists((MediatorImpl.Instance x) =>
            {
                this.contextMaterialB.References.Add(this);
                bool result = this.unityViewB.Equals(x.View) && x.Context == this.contextMaterialB.Context;
                this.contextMaterialB.References.Remove(this);
                return result;
            }),
            this.sequence.Next.ToString());

            IntegrationTest.Assert(!MediatorImpl.Instances.Exists((MediatorImpl.Instance x) =>
            {
                this.contextMaterialA.References.Add(this);
                bool result = this.unityViewB.Equals(x.View) && x.Context == this.contextMaterialA.Context;
                this.contextMaterialA.References.Remove(this);
                return result;
            }),
            this.sequence.Next.ToString());

            // test unmediation via destruction of view
            GameObject.DestroyImmediate(this.unityViewB);
            IntegrationTest.Assert(MediatorImpl.Instances.Exists((MediatorImpl.Instance x) => { return this.unityViewB.Equals(x.View) && (x.Mediator as Disposable).Disposed; }), this.sequence.Next.ToString());

            // cleanup
            IntegrationTest.Pass(this.gameObject);
        }
    }
}
