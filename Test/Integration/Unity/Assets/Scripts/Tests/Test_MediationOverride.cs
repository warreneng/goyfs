﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2017 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs.Test.Integration
{
    using Goyfs.Test.Common;
    using Goyfs.Unity;
    using UnityEngine;

    /// <summary>
    /// Tests the behavior of the stock MediationBinder when finding a binding for views.
    /// We expect the UnityView's specified ContextMaterial's MediationBinder to mediate it with the
    /// binding declared as close to our Context as possible. This means it's possible to override bindings
    /// declared by ancestor Contexts.
    /// 
    /// Assumptions:
    ///     1. ContextMaterialA parents ContextMaterialB
    ///     2. ContextMaterialA is set up to mediate UnityViews with MediatorImpl.A, while ContextMaterialB is set up to mediate with MediatorImpl.B
    ///     3. UnityViewB specifies ContextMaterialB for its Mediation
    /// </summary>
    public class Test_MediationOverride : MonoBehaviour
    {
        [SerializeField]
        private MediatedBehaviour unityViewB;

        // test identification
        private Sequence.Local sequence = new Sequence.Local();

        private void Update()
        {
            // test that ContextB's MediationBinder override's ContextA's binding
            IntegrationTest.Assert(MediatorImpl.Instances.Exists((MediatorImpl.Instance x) => { return this.unityViewB.Equals(x.View) && x.Mediator is MediatorImpl.B<MediatedBehaviour>; }), this.sequence.Next.ToString());
            IntegrationTest.Assert(!MediatorImpl.Instances.Exists((MediatorImpl.Instance x) => { return this.unityViewB.Equals(x.View) && x.Mediator is MediatorImpl.A<MediatedBehaviour>; }), this.sequence.Next.ToString());
            
            // tear down view so referenced ContextMaterials clean up
            GameObject.DestroyImmediate(this.unityViewB);
            IntegrationTest.Assert(MediatorImpl.Instances.Exists((MediatorImpl.Instance x) => { return this.unityViewB.Equals(x.View) && (x.Mediator as Disposable).Disposed; }), this.sequence.Next.ToString());

            // cleanup
            IntegrationTest.Pass(this.gameObject);
        }
    }
}
