﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2017 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs.Test.Integration
{
    using Goyfs.Test.Common;
    using Goyfs.Unity;
    using UnityEngine;

    /// <summary>
    /// Test the reference counting feature of ContextMaterial. It should Dispose() its Context when the count reaches zero.
    /// 
    /// Assumptions:
    ///     1. Test's transform hierarchy includes a UnityView whose assigned ContextMaterial is ContextMaterialA.
    /// </summary>
    public class Test_ContextMaterialReference : MonoBehaviour
    {
        [SerializeField]
        private ContextMaterial contextMaterialA;

        [SerializeField]
        private UnmediatedBehaviour unityViewA;

        // test identification
        private Sequence.Local sequence = new Sequence.Local();

        protected void Update()
        {
            this.contextMaterialA.References.Add(this);
            Context context = this.contextMaterialA.Context;
            IntegrationTest.Assert(context != null, this.sequence.Next.ToString());     // test: ContextA exists
            IntegrationTest.Assert(!context.Disposed, this.sequence.Next.ToString());   // test: ContextA is not already disposed of
            
            this.contextMaterialA.References.Remove(this);
            GameObject.DestroyImmediate(this.unityViewA);
            IntegrationTest.Assert(context.Disposed, this.sequence.Next.ToString());    // test: ContextA is now disposed because ContextMaterial's ref count = 0

            this.contextMaterialA.References.Add(this);
            IntegrationTest.Assert(this.contextMaterialA.Context != null, this.sequence.Next.ToString());       // test: ContextMaterial should still construct another Context when asked later
            IntegrationTest.Assert(this.contextMaterialA.Context != context, this.sequence.Next.ToString());    // test: ensure ContextMaterial made a 2nd Context

            // cleanup
            this.contextMaterialA.References.Remove(this);
            IntegrationTest.Pass(this.gameObject);
        }
    }
}
