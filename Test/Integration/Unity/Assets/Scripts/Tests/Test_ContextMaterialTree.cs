﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2017 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs.Test.Integration
{
    using Goyfs.Test.Common;
    using Goyfs.Unity;
    using UnityEngine;

    /// <summary>
    /// Test the ability of ContextMaterials to properly construct Context trees according to the hierarchy established at author-time.
    /// 
    /// Assumptions:
    ///     1. ContextMaterials A, B, C, and D form a tree such that A parents B and D, while B parents C
    /// </summary>
    public class Test_ContextMaterialTree : MonoBehaviour
    {
        [SerializeField]
        private ContextMaterial contextMaterialA;

        [SerializeField]
        private ContextMaterial contextMaterialB;

        [SerializeField]
        private ContextMaterial contextMaterialC;

        [SerializeField]
        private ContextMaterial contextMaterialD;

        // test identification
        private Sequence.Local sequence = new Sequence.Local();

        protected void Update()
        {
            this.contextMaterialA.References.Add(this);
            this.contextMaterialB.References.Add(this);
            this.contextMaterialC.References.Add(this);
            this.contextMaterialD.References.Add(this);
            IntegrationTest.Assert(this.contextMaterialA.Context.Parent == null, this.sequence.Next.ToString());                          // Context A should have no parent
            IntegrationTest.Assert(this.contextMaterialB.Context.Parent == this.contextMaterialA.Context, this.sequence.Next.ToString()); // Context B should be parented by A
            IntegrationTest.Assert(this.contextMaterialC.Context.Parent == this.contextMaterialB.Context, this.sequence.Next.ToString()); // Context C should be parented by B
            IntegrationTest.Assert(this.contextMaterialD.Context.Parent == this.contextMaterialA.Context, this.sequence.Next.ToString()); // Context D should be parented by A

            // cleanup
            this.contextMaterialA.References.Remove(this);
            this.contextMaterialB.References.Remove(this);
            this.contextMaterialC.References.Remove(this);
            this.contextMaterialD.References.Remove(this);
            IntegrationTest.Pass(this.gameObject);
        }
    }
}
