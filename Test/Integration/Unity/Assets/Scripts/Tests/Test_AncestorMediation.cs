﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2017 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs.Test.Integration
{
    using Common;
    using Unity;
    using UnityEngine;

    /// <summary>
    /// Tests the behavior of the stock MediationBinder when finding a binding for views.
    /// We expect the UnityView's specified ContextMaterial to search up the ContextMaterial hierarchy until
    /// a MediationBinder is found that will mediate the view. ContextMaterials encountered during the rootward
    /// search that do not declare a mediation binding for the view should not prevent the view from receiving
    /// mediation by an ancestor ContextMaterial that does.
    ///
    /// Assumptions:
    ///     1. ContextMaterialA parents ContextMaterialD
    ///     2. ContextMaterialA is set up to mediate UnityViews with MediatorImpl.A
    ///     3. ContextMaterialD does not mediate anything
    ///     4. UnityViewD specifies ContextMaterialD for its mediation
    /// </summary>
    public class Test_AncestorMediation : MonoBehaviour
    {
        [SerializeField]
        private MediatedBehaviour unityViewD;

        // test identification
        private Sequence.Local sequence = new Sequence.Local();

        private void Update()
        {
            // test that ContextA's MediationBinder mediated unityViewD in the absence of ContextD's MediationBinder doing it
            IntegrationTest.Assert(MediatorImpl.Instances.Exists((MediatorImpl.Instance x) => { return this.unityViewD.Equals(x.View) && x.Mediator is MediatorImpl.A<MediatedBehaviour>; }), this.sequence.Next.ToString());

            // tear down view so referenced ContextMaterials clean up
            GameObject.DestroyImmediate(this.unityViewD);
            IntegrationTest.Assert(MediatorImpl.Instances.Exists((MediatorImpl.Instance x) => { return this.unityViewD.Equals(x.View) && (x.Mediator as Disposable).Disposed; }), this.sequence.Next.ToString());

            // cleanup
            IntegrationTest.Pass(this.gameObject);
        }
    }
}
