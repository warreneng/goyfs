﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2017 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs.Test.Integration
{
    using Goyfs.Test.Common;
    using Goyfs.Unity;
    using UnityEngine;

    /// <summary>
    /// Test mediation of a UnityView already present in the scene on scene load.
    /// 
    /// Assumptions:
    ///     1. UnityViewImpl.A script is attached to this this GameObject, and its ContextMaterial loads ContextImpl.A
    ///     2. ContextImpl.A has set up its IMediationBinder to medate UnityViewImpl.A
    /// </summary>
    public class Test_MediateSceneView : MonoBehaviour
    {
        [SerializeField]
        private MediatedBehaviour unityViewA;

        [SerializeField]
        private ContextMaterial contextMaterialA;

        // test identification
        private Sequence.Local sequence = new Sequence.Local();

        private void Update()
        {
            // test mediation of views preexisting in scene
            IntegrationTest.Assert(MediatorImpl.Instances.Exists((MediatorImpl.Instance x) =>
            {
                this.contextMaterialA.References.Add(this);
                bool result = this.unityViewA.Equals(x.View) && this.contextMaterialA.Context.Equals(x.Context);
                this.contextMaterialA.References.Remove(this);
                return result;
            }),
            this.sequence.Next.ToString());

            // test unmediation via destruction of view
            GameObject.DestroyImmediate(this.unityViewA);
            IntegrationTest.Assert(MediatorImpl.Instances.Exists((MediatorImpl.Instance x) =>
            {
                return this.unityViewA.Equals(x.View) && (x.Mediator as Disposable).Disposed;
            }),
            this.sequence.Next.ToString());

            // cleanup
            IntegrationTest.Pass(this.gameObject);
        }
    }
}
