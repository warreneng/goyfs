﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2017 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs.Test.Integration
{
    using Goyfs.Test.Common;
    using Goyfs.Unity;
    using UnityEngine;
    
    /// <summary>
    /// Test UnityView instances in a scene can bind values to their ContextMaterial's Context's IInstanceBinder prior to
    /// any mediations occuring in that frame. This allows Unity things (say an important GameObject) to be bound as singletons in a Context.
    /// 
    /// Assumptions:
    ///     1. unityView_bindsSelf attaches to a ContextMaterial with underlying ContextImpl.CircularSceneInstances Context
    ///     2. unityView_bindsGameObj attaches to a ContextMaterial with same underlying Context as (1.)
    ///     3. boundGameObj is a reference to unityView_bindsGameObj's GameObject
    ///     4. unityView_bindsSelf binds itself as a UnityView singleton to its Context on Awake()
    ///     5. unityView_bindsGameObj binds its GameObject as a GameObject singleton to its Context on Awake()
    ///     6. ContextImpl.CircularSceneInstances mediates unityView_bindsSelf with MediatorImpl.FetchSingleton that'll fetch <GameObject> from IInstanceBinder,
    ///         and mediates unityView_bindsGameObj with MediatorImpl.FetchSingleton that'll fetch <UnityView> from IInstanceBinder.
    /// </summary>
    public class Test_FetchCircularSceneInstanceBindings : MonoBehaviour
    {
        //[SerializeField]
        //private UnityView_BindsSelf unityView_bindsSelf;

        //[SerializeField]
        //private UnityView_BindsGameObject unityView_bindsGameObj;

        //[SerializeField]
        //private GameObject boundGameObj;

        //// test identification
        //private Sequence.Local sequence = new Sequence.Local();

        //private void Update()
        //{
        //    // test: assert mediator of view that bound a GameObject could fetch the other's bound UnityView
        //    IntegrationTest.Assert(MediatorImpl.Instances.Exists((MediatorImpl.Instance x) =>
        //    {
        //        return 
        //            x.View == this.unityView_bindsGameObj
        //            && x.Mediator is MediatorImpl.FetchSingleton<UnityView_BindsGameObject, MediatedBehaviour>
        //            && (x.Mediator as MediatorImpl.FetchSingleton<UnityView_BindsGameObject, MediatedBehaviour>).FetchedObject == this.unityView_bindsSelf;
        //    }),
        //    this.sequence.Next.ToString());

        //    // test: assert mediator of view that bound a UnityView could fetch the other's bound GameObject
        //    IntegrationTest.Assert(MediatorImpl.Instances.Exists((MediatorImpl.Instance x) =>
        //    {
        //        return
        //            x.View == this.unityView_bindsSelf
        //            && x.Mediator is MediatorImpl.FetchSingleton<UnityView_BindsSelf, GameObject>
        //            && (x.Mediator as MediatorImpl.FetchSingleton<UnityView_BindsSelf, GameObject>).FetchedObject == this.boundGameObj;
        //    }),
        //    this.sequence.Next.ToString());

        //    // cleanup
        //    GameObject.DestroyImmediate(this.unityView_bindsSelf);
        //    GameObject.DestroyImmediate(this.unityView_bindsGameObj);
        //    GameObject.DestroyImmediate(this.boundGameObj);
        //    IntegrationTest.Pass(this.gameObject);
        //}
    }
}
