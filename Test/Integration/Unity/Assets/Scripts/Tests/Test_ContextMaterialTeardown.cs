﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2017 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs.Test.Integration
{
    using Goyfs.Test.Common;
    using Goyfs.Unity;
    using UnityEngine;

    /// <summary>
    /// Tests ContextMaterial's behavior when various nodes in the tree lose their last reference.
    /// Their Contexts should never be disposed of prior to ContextMaterial.References hitting zero.
    /// 
    /// Assumptions:
    ///     1. UnityViewB is assigned ContextMaterialB. UnityViewC is assigned ContextMaterial C.
    ///     2. ContextMaterialC's parent is B, and B's parent is A.
    /// </summary>
    public class Test_ContextMaterialTeardown : MonoBehaviour
    {
        [SerializeField]
        private ContextMaterial contextMaterialA;

        [SerializeField]
        private ContextMaterial contextMaterialB;

        [SerializeField]
        private ContextMaterial contextMaterialC;

        [SerializeField]
        private UnmediatedBehaviour unityViewB;

        [SerializeField]
        private UnmediatedBehaviour unityViewC;

        // test identification
        private Sequence.Local sequence = new Sequence.Local();

        private void Update()
        {
            this.contextMaterialA.References.Add(this);
            this.contextMaterialB.References.Add(this);
            this.contextMaterialC.References.Add(this);
            Context contextA = this.contextMaterialA.Context;
            Context contextB = this.contextMaterialB.Context;
            Context contextC = this.contextMaterialC.Context;
            this.contextMaterialA.References.Remove(this);
            this.contextMaterialB.References.Remove(this);
            this.contextMaterialC.References.Remove(this);

            IntegrationTest.Assert(contextA.Parent == null, this.sequence.Next.ToString());     // test: hierarchy verification
            IntegrationTest.Assert(contextB.Parent == contextA, this.sequence.Next.ToString()); // test: hierarchy verification
            IntegrationTest.Assert(contextC.Parent == contextB, this.sequence.Next.ToString()); // test: hierarchy verification

            GameObject.DestroyImmediate(this.unityViewB);
            IntegrationTest.Assert(!contextA.Disposed, this.sequence.Next.ToString());  // test: A/B/C Context tree intact because ContextMaterialA still referenced by ContextMaterialB
            IntegrationTest.Assert(!contextB.Disposed, this.sequence.Next.ToString());  // test: A/B/C Context tree intact because CotnextMaterialB still referenced by ContextMaterialC
            IntegrationTest.Assert(!contextC.Disposed, this.sequence.Next.ToString());  // test: A/B/C Context tree intact because CotnextMaterialC still referenced by UnityViewC

            GameObject.DestroyImmediate(this.unityViewC);
            IntegrationTest.Assert(contextC.Disposed, this.sequence.Next.ToString());   // test: A/B/C Context tree Disposed because ContextMaterialC's reference from B disappeared
            IntegrationTest.Assert(contextA.Disposed, this.sequence.Next.ToString());   // test: A/B/C Context tree Disposed because ContextMaterialA's reference from B disappeared
            IntegrationTest.Assert(contextB.Disposed, this.sequence.Next.ToString());   // test: A/B/C Context tree Disposed because ContextMaterialB's reference from UnityViewC disappeared

            // cleanup
            IntegrationTest.Pass(this.gameObject);
        }
    }
}
