﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2017 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs.Test.Integration
{
    using Goyfs.Test.Common;
    using Goyfs.Unity;
    using UnityEngine;

    /// <summary>
    /// Test UnityView's ability to have its ContextMaterial property set by the user at runtime
    /// to facilitate dynamic mediation bindings.
    /// 
    /// Assumptions:
    ///     1. ContextMaterialA parents ContextMaterialB
    ///     2. ContextMaterialA mediates UnityView with MediatorImpl.A, and ContextMaterialB mediates UnityView with MediatorImpl.B
    ///     3. UnityViewA is attached to a scene GameObject and has no ContextMaterial assigned
    ///     4. UnityViewPrefab has a UnityView script attached with ContextMaterialA assigned
    /// </summary>
    public class Test_DynamicContextMaterial : MonoBehaviour
    {
        [SerializeField]
        private ContextMaterial contextMaterialA;

        [SerializeField]
        private ContextMaterial contextMaterialB;

        [SerializeField]
        private MediatedBehaviour unityViewA;

        [SerializeField]
        private GameObject unityViewPrefab;

        // test identification
        private Sequence.Local sequence = new Sequence.Local();

        protected void Update()
        {
            // test: UnityView is unmediated w/o ContextMaterial assigned
            IntegrationTest.Assert(!MediatorImpl.Instances.Exists((MediatorImpl.Instance x) =>
            {
                return this.unityViewA.Equals(x.View);
            }),
            this.sequence.Next.ToString());

            // test: mediation of views with no prior ContextMaterial assignment
            this.unityViewA.Material = this.contextMaterialA;
            IntegrationTest.Assert(MediatorImpl.Instances.Exists((MediatorImpl.Instance x) =>
            {
                this.contextMaterialA.References.Add(this);
                bool result = this.unityViewA.Equals(x.View) && this.contextMaterialA.Context.Equals(x.Context);
                this.contextMaterialA.References.Remove(this);
                return result;
            }),
            this.sequence.Next.ToString());

            // test: mediation of views when switching ContextMaterials
            this.unityViewA.Material = this.contextMaterialB;
            IntegrationTest.Assert(!MediatorImpl.Instances.Exists((MediatorImpl.Instance x) =>
            {
                // test: unhooked from ContextMaterialA
                this.contextMaterialA.References.Add(this);
                bool result = this.unityViewA.Equals(x.View) && this.contextMaterialA.Context.Equals(x.Context);
                this.contextMaterialA.References.Remove(this);
                return result;
            }),
            this.sequence.Next.ToString());
            IntegrationTest.Assert(MediatorImpl.Instances.Exists((MediatorImpl.Instance x) =>
            {
                // test: hooked next into ContextMaterialB
                this.contextMaterialB.References.Add(this);
                bool result = this.unityViewA.Equals(x.View) && this.contextMaterialB.Context.Equals(x.Context);
                this.contextMaterialB.References.Remove(this);
                return result;
            }),
            this.sequence.Next.ToString());

            // test mediation of views instantiated via prefab
            MediatedBehaviour unityViewInstance = GameObject.Instantiate(this.unityViewPrefab).GetComponent<MediatedBehaviour>();
            IntegrationTest.Assert(MediatorImpl.Instances.Exists((MediatorImpl.Instance x) =>
            {
                // test: prefab with no ContextMaterial assigned was mediated
                this.contextMaterialA.References.Add(this);
                bool result = unityViewInstance.Equals(x.View) && this.contextMaterialA.Context.Equals(x.Context) && x.Mediator is MediatorImpl.A<MediatedBehaviour>;
                this.contextMaterialA.References.Remove(this);
                return result;
            }),
            this.sequence.Next.ToString());
            unityViewInstance.Material = this.contextMaterialB;
            IntegrationTest.Assert(MediatorImpl.Instances.Exists((MediatorImpl.Instance x) =>
            {
                // test: prefab now mediated by newly assigned ContextMaterial
                this.contextMaterialB.References.Add(this);
                bool result = unityViewInstance.Equals(x.View) && this.contextMaterialB.Context.Equals(x.Context) && x.Mediator is MediatorImpl.B<MediatedBehaviour>;
                this.contextMaterialB.References.Remove(this);
                return result;
            }),
            this.sequence.Next.ToString());

            // cleanup
            GameObject.DestroyImmediate(this.unityViewA);
            GameObject.DestroyImmediate(unityViewInstance);
            this.contextMaterialA.References.Remove(this);
            this.contextMaterialB.References.Remove(this);
            IntegrationTest.Pass(this.gameObject);
        }
    }
}
