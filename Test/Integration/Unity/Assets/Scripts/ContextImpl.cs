﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2017 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs.Test.Integration
{
    using Goyfs;
    using Goyfs.Test.Common;
    using Goyfs.Unity;

    public class ContextImpl : UnityContext
    {
        public ContextImpl(Context parent) : base(new InstanceBinder(), parent)
        {
            this.InstanceBinder.Bind<char[]>(new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j' });
            this.InstanceBinder.Bind<object, string>(new object(), "foo");
            this.InstanceBinder.Bind<object, string>(new object(), "bar");
            this.InstanceBinder.Bind<int>(42);
        }
        public class A : ContextImpl
        {
            public A(Context parent) : base(parent)
            {
                this.MediationBinder.Bind<MediatedBehaviour, MediatorImpl.A<MediatedBehaviour>>();
            }
        }
        public class B : ContextImpl
        {
            public B(Context parent) : base(parent)
            {
                this.MediationBinder.Bind<MediatedBehaviour, MediatorImpl.B<MediatedBehaviour>>();
            }
        }
        public class MediateWithFetchInstance : UnityContext
        {
            public MediateWithFetchInstance(Context parent) : base(new InstanceBinder(), parent)
            {
                this.MediationBinder.Bind<MediatedBehaviour, MediatorImpl.FetchSingleton<MediatedBehaviour, int>>();
            }
        }
        public class BindSomethingFromUnmediatedViewAndGetInMediator : UnityContext
        {
            public BindSomethingFromUnmediatedViewAndGetInMediator(Context parent) : base(new InstanceBinder(), parent)
            {
                // set up: UnmediatedBehaviour binds itself to InstanceBinder on Awake(), MediatedBehaviour's Mediator's OnMediate is called late enough to retrieve it
                this.MediationBinder.Bind<MediatedBehaviour, MediatorImpl.FetchSingleton<MediatedBehaviour, GoyfsBehaviour>>();
            }
        }
    }
}
