﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs.Test.Unit
{
    using Goyfs;
    using Goyfs.Test.Common;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;

    [TestFixture]
    internal class Multiton_UnitTest
    {
        [Test]
        public void InitialState()
        {
            Multiton<object, string> multiton = new Multiton<object, string>();
            Assert.False(multiton.Disposed);    // test: initial state undisposed
            Assert.True(multiton.Empty);        // test: initial state no instances
        }

        [Test]
        public void DisposedState()
        {
            Multiton<object, string> multiton = new Multiton<object, string>();
            Assert.DoesNotThrow(() => { multiton.Dispose(); }); // test: dispose OK with no instances
            Assert.DoesNotThrow(() => { multiton.Dispose(); }); // test: redundant Dispose() OK
            //Assert.Throws<ObjectDisposedException>(() => { multiton.AddInstance(); });                      // test: Multiton usage not allowed after disposal
            //Assert.Throws<ObjectDisposedException>(() => { multiton.AddInstance(new object()); });          // test: Multiton usage not allowed after disposal
            Assert.Throws<ObjectDisposedException>(() => { multiton.AddInstance("foo"); });                 // test: Multiton usage not allowed after disposal
            Assert.Throws<ObjectDisposedException>(() => { multiton.AddInstance(new object(), "foo"); });   // test: Multiton usage not allowed after disposal
            //Assert.Throws<ObjectDisposedException>(() => { multiton.GetInstance(); });                      // test: Multiton usage not allowed after disposal
            Assert.Throws<ObjectDisposedException>(() => { multiton.GetInstance("foo"); });                 // test: Multiton usage not allowed after disposal
            //Assert.Throws<ObjectDisposedException>(() => { multiton.RemoveInstance(); });                   // test: Multiton usage not allowed after disposal
            Assert.Throws<ObjectDisposedException>(() => { multiton.RemoveInstance("foo"); });              // test: Multiton usage not allowed after disposal
        }

        //[Test]
        //public void AddUnnamedUninstantiated()
        //{
        //    Multiton<object, string> multiton = new Multiton<object, string>();
        //    Assert.Throws<InvalidOperationException>(() => { multiton.AddInstance(); });    // test: unnamed multitons disallowed
        //}

        //[Test]
        //public void AddUnnamedInstanced()
        //{
        //    Multiton<object, string> multiton = new Multiton<object, string>();
        //    Assert.Throws<InvalidOperationException>(() => { multiton.AddInstance(new object()); });    // test: unnamed multitons disallowed
        //}

        [Test]
        public void AddNamedUninstantiated()
        {
            Multiton<object, string> multiton = new Multiton<object, string>();
            Assert.Throws<ArgumentNullException>(() => { multiton.AddInstance(null); });            // test: instances must be named
            Assert.Throws<ArgumentNullException>(() => { multiton.AddInstance(string.Empty); });    // test: instances must be named
            Assert.DoesNotThrow(() => { multiton.AddInstance("foo"); });                            // test: adding named instance OK
            Assert.Throws<ArgumentException>(() => { multiton.AddInstance("foo"); });               // test: instance names must be unique
            Assert.DoesNotThrow(() => { multiton.AddInstance("bar"); });                            // test: unique instance names OK
        }

        [Test]
        public void AddNamedInstanced()
        {
            Multiton<object, string> multiton = new Multiton<object, string>();
            Assert.Throws<ArgumentNullException>(() => { multiton.AddInstance(null, "foo"); });                 // test: value cannot be null
            Assert.Throws<ArgumentNullException>(() => { multiton.AddInstance(new object(), string.Empty); });  // test: instances must be named
            Assert.DoesNotThrow(() => { multiton.AddInstance(new object(), "foo"); });                          // test: adding named instantiated OK
            Assert.Throws<ArgumentException>(() => { multiton.AddInstance(new object(), "foo"); });             // test: instance names must be unique
            Assert.DoesNotThrow(() => { multiton.AddInstance(new object(), "bar"); });                          // test: unique instance names OK
        }

        //[Test]
        //public void GetUnnamedInstance()
        //{
        //    Multiton<object, string> multiton = new Multiton<object, string>();
        //    Assert.Throws<InvalidOperationException>(() => { multiton.GetInstance(); });    // test: cannot get instance without name
        //}

        [Test]
        public void GetNamedUninstantiated()
        {
            Multiton<DisposableImpl, string> multiton = new Multiton<DisposableImpl, string>();
            Assert.Throws<KeyNotFoundException>(() => { multiton.GetInstance("foo"); });    // test: error when instance doesn't exist
            
            multiton.AddInstance("foo");
            object objectA = null;
            Assert.DoesNotThrow(() => { objectA = multiton.GetInstance("foo"); });  // test: getting added named instance OK
            Assert.NotNull(objectA);                                                // test: instance was constructed and returned
            Assert.IsAssignableFrom<DisposableImpl>(objectA);                       // test: returned object matched type T

            multiton.AddInstance("bar");
            object objectB = null;
            Assert.DoesNotThrow(() => { objectB = multiton.GetInstance("bar"); });  // test: getting 2nd named instance OK
            Assert.AreNotEqual(objectA, objectB);                                   // test: 2 unique instances were created
        }

        [Test]
        public void GetNamedInstanced()
        {
            Multiton<object, string> multiton = new Multiton<object, string>();
            Assert.Throws<KeyNotFoundException>(() => { multiton.GetInstance("foo"); });    // test: error when instance doesn't exist
            
            object objectA = new object();
            object objectB = null;
            multiton.AddInstance(objectA, "foo");
            Assert.DoesNotThrow(() => { objectB = multiton.GetInstance("foo"); });  // test: getting added named instance OK
            Assert.NotNull(objectB);                                                // test: instance was returned
            Assert.AreEqual(objectA, objectB);                                      // test: instance returned is same we added
        }

        //[Test]
        //public void RemoveUnnamedInstance()
        //{
        //    Multiton<object, string> multiton = new Multiton<object, string>();
        //    Assert.Throws<InvalidOperationException>(() => { multiton.RemoveInstance(); }); // test: removing unnamed instance disallowed
        //}

        [Test]
        public void RemoveNamedInstance()
        {
            Multiton<object, string> multiton = new Multiton<object, string>();
            Assert.Throws<KeyNotFoundException>(() => { multiton.RemoveInstance("foo"); }); // test: error when instance not found
            multiton.AddInstance("foo");
            multiton.AddInstance("bar");
            Assert.DoesNotThrow(() => { multiton.RemoveInstance("foo"); }); // test: removing named instance OK
            Assert.DoesNotThrow(() => { multiton.GetInstance("bar"); });    // test: removal of foo didn't remove bar
            Assert.Throws<KeyNotFoundException>(() => { multiton.RemoveInstance("foo"); }); // test: ensure foo was removed
            multiton.RemoveInstance("bar");
            Assert.True(multiton.Empty);    // test: multiton is empty after removals
        }
    }
}
