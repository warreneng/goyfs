﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs.Test.Unit
{
    using Goyfs;
    using Goyfs.Test.Common;
    using NUnit.Framework;
    using System;

    [TestFixture]
    internal class Singleton_UnitTest
    {
        [Test]
        public void InitialState()
        {
            Singleton<object> singleton = new Singleton<object>();
            Assert.False(singleton.Disposed);   // test: initial state undisposed
            //Assert.True(singleton.Empty);       // test: initial state no instances
        }

        [Test]
        public void DisposedState()
        {
            Singleton<object> singleton = new Singleton<object>();
            Assert.DoesNotThrow(() => { singleton.Dispose(); });    // test: dispose OK with no instances
            Assert.DoesNotThrow(() => { singleton.Dispose(); });    // test: redundant Dispose() OK
            //Assert.Throws<ObjectDisposedException>(() => { singleton.AddInstance(); });                     // test: Singleton usage not allowed after disposal
            Assert.Throws<ObjectDisposedException>(() => { singleton.AddInstance(new object()); });         // test: Singleton usage not allowed after disposal
            //Assert.Throws<ObjectDisposedException>(() => { singleton.AddInstance("foo"); });                // test: Singleton usage not allowed after disposal
            //Assert.Throws<ObjectDisposedException>(() => { singleton.AddInstance(new object(), "foo"); });  // test: Singleton usage not allowed after disposal
            Assert.Throws<ObjectDisposedException>(() => { singleton.GetInstance(); });                     // test: Singleton usage not allowed after disposal
            //Assert.Throws<ObjectDisposedException>(() => { singleton.GetInstance("foo"); });                // test: Singleton usage not allowed after disposal
            //Assert.Throws<ObjectDisposedException>(() => { singleton.RemoveInstance(); });                  // test: Singleton usage not allowed after disposal
            //Assert.Throws<ObjectDisposedException>(() => { singleton.RemoveInstance("foo"); });             // test: Singleton usage not allowed after disposal
        }

        [Test]
        public void AddUnnamedUninstantiated()
        {
            Singleton<object> singleton = new Singleton<object>();
            //Assert.DoesNotThrow(() => { singleton.AddInstance(); });                        // test: adding first instance OK
            //Assert.Throws<InvalidOperationException>(() => { singleton.AddInstance(); });   // test: error second instance
        }

        [Test]
        public void AddUnnamedInstanced()
        {
            Singleton<object> singleton = new Singleton<object>();
            object obj = new object();
            Assert.DoesNotThrow(() => { singleton.AddInstance(obj); });                                 // test: adding first instance OK
            Assert.Throws<InvalidOperationException>(() => { singleton.AddInstance(obj); });            // test: redundant add disallowed
            Assert.Throws<InvalidOperationException>(() => { singleton.AddInstance(new object()); });   // test: additional add disallowed
        }

        //[Test]
        //public void AddNamedUninstantiated()
        //{
        //    Singleton<object> singleton = new Singleton<object>();
        //    Assert.Throws<InvalidOperationException>(() => { singleton.AddInstance("foo"); });  // test: adding named instance disallowed
        //}

        //[Test]
        //public void AddNamedInstanced()
        //{
        //    Singleton<object> singleton = new Singleton<object>();
        //    Assert.Throws<InvalidOperationException>(() => { singleton.AddInstance(new object(), "foo"); });    // test: adding named instance disallowed
        //}

        [Test]
        public void GetUnnamedUninstantiated()
        {
            Singleton<DisposableImpl> singleton = new Singleton<DisposableImpl>();
            //Assert.Throws<InvalidOperationException>(() => { singleton.GetInstance(); });   // test: error instance not added yet

            //singleton.AddInstance();
            object objectA = null;
            Assert.DoesNotThrow(() => { objectA = singleton.GetInstance(); });  // test: getting unnamed instance OK after add
            Assert.NotNull(objectA);                                            // test: instance returned when asked for
            Assert.IsAssignableFrom<DisposableImpl>(objectA);                   // test: constructed instance matches type T

            object objectB = singleton.GetInstance();
            Assert.AreEqual(objectA, objectB);  // test: same instance returned for both GetInstance calls
        }

        [Test]
        public void GetUnnamedInstanced()
        {
            Singleton<object> singleton = new Singleton<object>();
            //Assert.Throws<InvalidOperationException>(() => { singleton.GetInstance(); });   // test: error instance not added yet

            object objectA = new object();
            object objectB = null;
            singleton.AddInstance(objectA);
            Assert.DoesNotThrow(() => { objectB = singleton.GetInstance(); });  // test: getting unnamed instance OK after add
            Assert.AreEqual(objectA, objectB);                                  // test: instance returned matches the one we added
        }

        //[Test]
        //public void GetNamedInstance()
        //{
        //    Singleton<object> singleton = new Singleton<object>();
        //    Assert.Throws<InvalidOperationException>(() => { singleton.GetInstance("foo"); });  // test: getting a named instance disallowed
        //}

        [Test]
        public void RemoveUnnamedInstance()
        {
            Singleton<object> singleton = new Singleton<object>();
            //Assert.Throws<InvalidOperationException>(() => { singleton.RemoveInstance(); });    // test: error instance not added yet
            //singleton.AddInstance();
            //Assert.DoesNotThrow(() => { singleton.RemoveInstance(); }); // test: removing unnamed instance OK after add
            //Assert.True(singleton.Empty);                               // test: singleton is empty after removal
        }

        //[Test]
        //public void RemoveNamedInstance()
        //{
        //    Singleton<object> singleton = new Singleton<object>();
        //    Assert.Throws<InvalidOperationException>(() => { singleton.RemoveInstance("foo"); });   // test: removing named instance disallowed
        //}

        [Test]
        public void InstanceDisposed()
        {
            Singleton<object> singleton = new Singleton<object>();
            DisposableImpl disposable = new DisposableImpl();
            singleton.AddInstance(disposable);
            Assert.DoesNotThrow(() => { singleton.Dispose(); });    // test: disposing of a Singleton<T> OK when it contains an instance
            Assert.True(disposable.Disposed);   // test: singleton called Dispose() on IDisposable
        }

        [Test]
        public void BindNullInstance()
        {
            Singleton<object> singleton = new Singleton<object>();
            Assert.Throws<ArgumentException>(() => { singleton.AddInstance(null); });   // test: cannot bind a null instance
        }

        [Test]
        public void BindDefaultInstance()
        {
            Singleton<int> singleton = new Singleton<int>();
            Assert.Throws<ArgumentException>(() => { singleton.AddInstance(0); });  // test: cannot bind a default value
        }
    }
}
