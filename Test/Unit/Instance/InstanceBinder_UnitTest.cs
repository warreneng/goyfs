﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs.Test.Unit
{
    using Goyfs;
    using Goyfs.Test.Common;
    using NSubstitute;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;

    [TestFixture]
    internal class InstanceBinder_UnitTest
    {
        [Test]
        public void InitialState()
        {
            InstanceBinder instanceBinder = new InstanceBinder();
            Assert.False(instanceBinder.Disposed);  // test: inital state undisposed
        }

        [Test]
        public void DisposedState()
        {
            InstanceBinder instanceBinder = new InstanceBinder();

            Assert.DoesNotThrow(() => { instanceBinder.Dispose(); });   // test: dispose OK with no instances
            Assert.DoesNotThrow(() => { instanceBinder.Dispose(); });   // test: redundant Dispose() OK
            Assert.Throws<ObjectDisposedException>(() => { instanceBinder.Bind<int>(); });                  // test: InstanceBinder usage not allowed after disposal
            Assert.Throws<ObjectDisposedException>(() => { instanceBinder.Bind<int>(1); });                 // test: InstanceBinder usage not allowed after disposal
            Assert.Throws<ObjectDisposedException>(() => { instanceBinder.Bind<int, string>("foo"); });     // test: InstanceBinder usage not allowed after disposal
            Assert.Throws<ObjectDisposedException>(() => { instanceBinder.Bind<int, string>(1, "foo"); });  // test: InstanceBinder usage not allowed after disposal
            Assert.Throws<ObjectDisposedException>(() => { instanceBinder.Get<int>(); });                   // test: InstanceBinder usage not allowed after disposal
            Assert.Throws<ObjectDisposedException>(() => { instanceBinder.Get<int, string>("foo"); });      // test: InstanceBinder usage not allowed after disposal
            Assert.Throws<ObjectDisposedException>(() => { instanceBinder.Unbind<int>(); });                // test: InstanceBinder usage not allowed after disposal
            Assert.Throws<ObjectDisposedException>(() => { instanceBinder.Unbind<int, string>("foo"); });   // test: InstanceBinder usage not allowed after disposal
        }

        [Test]
        public void OnDispose()
        {
            IInstanceBinder instanceBinder = new InstanceBinder();
            instanceBinder.Bind<DisposableImpl>();
            DisposableImpl disposable = instanceBinder.Get<DisposableImpl>();
            Assert.False(disposable.Disposed);  // test: ensure starting state is undisposed
            instanceBinder.Dispose();
            Assert.True(disposable.Disposed);   // test: ensure InstanceBinder calls Dispose() on its IDisposable singletons
        }

        [Test]
        public void OnDisposeMultiton()
        {
            IInstanceBinder instanceBinder = new InstanceBinder();
            instanceBinder.Bind<DisposableImpl, string>("foo");
            Disposable disposable = instanceBinder.Get<DisposableImpl, string>("foo");
            Assert.False(disposable.Disposed);  // test: ensure starting state is undisposed
            instanceBinder.Dispose();
            Assert.True(disposable.Disposed);   // test: ensure InstanceBinder calls Dispose() on its IDisposable multitons
        }

        [Test]
        public void AddSingletonDefaultValue()
        {
            InstanceBinder instanceBinder = new InstanceBinder();
            this.BindSingletonSuite(() => { instanceBinder.Bind<int>(); });
        }

        [Test]
        public void AddSingletonValue()
        {
            InstanceBinder instanceBinder = new InstanceBinder();
            this.BindSingletonSuite(() => { instanceBinder.Bind<int>(11); });
        }

        [Test]
        public void AddSingletonDefaultObject()
        {
            InstanceBinder instanceBinder = new InstanceBinder();
            this.BindSingletonSuite(() => { instanceBinder.Bind<object>(); });
        }

        [Test]
        public void AddSingletonObject()
        {
            InstanceBinder instanceBinder = new InstanceBinder();
            this.BindSingletonSuite(() => { instanceBinder.Bind<object>(new object()); });
        }

        [Test]
        public void AddSingletonAddValue()
        {
            InstanceBinder instanceBinder = new InstanceBinder();
            this.BindSingletonSuite(() => { instanceBinder.Bind<int>(); });
            Assert.Throws<InvalidOperationException>(() => { instanceBinder.Bind<int>(2); });
        }

        [Test]
        public void AddSingletonAddMultiton()
        {
            InstanceBinder instanceBinder = new InstanceBinder();
            this.BindSingletonSuite(() => { instanceBinder.Bind<int>(); });
            Assert.Throws<InvalidOperationException>(() => { instanceBinder.Bind<int, string>("foo"); }); // test: throws when binding a multiton type already bound as singleton
        }

        [Test]
        public void AddSingletonAddMultitonValue()
        {
            InstanceBinder instanceBinder = new InstanceBinder();
            this.BindSingletonSuite(() => { instanceBinder.Bind<int>(); });
            Assert.Throws<InvalidOperationException>(() =>{ instanceBinder.Bind<int, string>(2, "foo"); }); // test: throws when binding a multiton value already bound as a singleton
        }

        [Test]
        public void AddSingletonAddMultitonObject()
        {
            InstanceBinder instanceBinder = new InstanceBinder();
            this.BindSingletonSuite(() => { instanceBinder.Bind<object>(); });
            Assert.Throws<InvalidOperationException>(() => { instanceBinder.Bind<object, string>(new object(), "foo"); });  // test: throws when binding a multiton object already bound as a singleton
        }

        [Test]
        public void AddMultitonDefaultValue()
        {
            InstanceBinder instanceBinder = new InstanceBinder();
            this.BindMultitonSuite(() => { instanceBinder.Bind<int, string>("foo"); });
            Assert.DoesNotThrow(() => { instanceBinder.Bind<int, string>("bar"); });    // test: 2nd binding OK with different name
        }

        [Test]
        public void AddMultitonValue()
        {
            InstanceBinder instanceBinder = new InstanceBinder();
            this.BindMultitonSuite(() => { instanceBinder.Bind<int, string>(11, "foo"); });
            Assert.DoesNotThrow(() => { instanceBinder.Bind<int, string>(11, "bar"); });    // test: 2nd binding OK with different name
        }

        [Test]
        public void AddMultitonDefaultObject()
        {
            InstanceBinder instanceBinder = new InstanceBinder();
            this.BindMultitonSuite(() => { instanceBinder.Bind<object, string>("foo"); });
            Assert.DoesNotThrow(() => { instanceBinder.Bind<object, string>("bar"); }); // test: 2nd binding OK with different name
        }

        [Test]
        public void AddMultitonObject()
        {
            InstanceBinder instanceBinder = new InstanceBinder();
            this.BindMultitonSuite(() => { instanceBinder.Bind<object, string>(new object(), "foo"); });
            Assert.DoesNotThrow(() => { instanceBinder.Bind<object, string>(new object(), "bar"); });   // test: 2nd binding OK with different name
        }

        [Test]
        public void AddMultitonAddSingleton()
        {
            InstanceBinder instanceBinder = new InstanceBinder();
            this.BindMultitonSuite(() => { instanceBinder.Bind<object, string>("foo"); });
            Assert.Throws<InvalidOperationException>(() => { instanceBinder.Bind<object>(); }); // test: throws when binding a singleton type already bound as a multiton
        }

        [Test]
        public void AddMultitonAddSingletonObject()
        {
            InstanceBinder instanceBinder = new InstanceBinder();
            this.BindMultitonSuite(() => { instanceBinder.Bind<object, string>("foo"); });
            Assert.Throws<InvalidOperationException>(() => { instanceBinder.Bind<object>(new object()); }); // test: throws when binding a singleton object already bound as a multiton
        }

        [Test]
        public void GetSingletonDefaultValue()
        {
            InstanceBinder instanceBinder = new InstanceBinder();
            instanceBinder.Bind<int>();
            int value = -1;
            Assert.DoesNotThrow(() => { value = instanceBinder.Get<int>(); });  // test: getting default value OK
            Assert.AreEqual(value, default(int));                               // test: default value returned if we didn't bind a value, just the type
        }

        [Test]
        public void GetSingletonValue()
        {
            InstanceBinder instanceBinder = new InstanceBinder();
            instanceBinder.Bind<int>(11);
            int value = -1;
            Assert.DoesNotThrow(() => { value = instanceBinder.Get<int>(); });  // test: getting specified value OK
            Assert.AreEqual(value, 11);                                         // test: value bound to type returned
        }

        [Test]
        public void GetSingletonDefaultObject()
        {
            InstanceBinder instanceBinder = new InstanceBinder();
            instanceBinder.Bind<DisposableImpl>();
            DisposableImpl obj = null;
            Assert.DoesNotThrow(() => { obj = instanceBinder.Get<DisposableImpl>(); }); // test: getting uninitialized singleton object OK
            Assert.NotNull(obj);                                                        // test: object constructed and returned
            Assert.True(obj is DisposableImpl);                                         // test: object returned matches bind type
        }

        [Test]
        public void GetSingletonObject()
        {
            InstanceBinder instanceBinder = new InstanceBinder();
            DisposableImpl obj1 = new DisposableImpl();
            instanceBinder.Bind<DisposableImpl>(obj1);
            object obj2 = null;
            Assert.DoesNotThrow(() => { obj2 = instanceBinder.Get<DisposableImpl>(); });    // test: getting initialized singleton object OK
            Assert.NotNull(obj2);                                                           // test: object returned
            Assert.AreEqual(obj1, obj2);                                                    // test: same object bound as singleton returned
        }

        [Test]
        public void GetMultitonDefaultValue()
        {
            InstanceBinder instanceBinder = new InstanceBinder();
            instanceBinder.Bind<int, string>("foo");
            instanceBinder.Bind<int, string>("bar");
            int value = -1;
            Assert.DoesNotThrow(() => { value = instanceBinder.Get<int, string>("foo"); }); // test: getting default value by name OK
            Assert.AreEqual(default(int), value);                                           // test: default value of type returned
            value = -1;
            Assert.DoesNotThrow(() => { value = instanceBinder.Get<int, string>("bar"); }); // test: getting default value by name OK
            Assert.AreEqual(default(int), value);                                           // test: default value of type returned
        }

        [Test]
        public void GetMultitonValue()
        {
            InstanceBinder instanceBinder = new InstanceBinder();
            instanceBinder.Bind<int, string>(11, "foo");
            instanceBinder.Bind<int, string>(22, "bar");
            int value = -1;
            Assert.DoesNotThrow(() => { value = instanceBinder.Get<int, string>("foo"); }); // test: getting specified value by name OK
            Assert.AreEqual(value, 11);                                                     // test: named value bound to type returned
            value = -1;
            Assert.DoesNotThrow(() => { value = instanceBinder.Get<int, string>("bar"); }); // test: getting specified value by name OK
            Assert.AreEqual(value, 22);                                                     // test: named value bound to type returned
        }

        [Test]
        public void GetMultionDefaultObject()
        {
            InstanceBinder instanceBinder = new InstanceBinder();
            instanceBinder.Bind<object, string>("foo");
            instanceBinder.Bind<object, string>("bar");
            object obj1 = null;
            Assert.DoesNotThrow(() => { obj1 = instanceBinder.Get<object, string>("foo"); });   // test: getting uninitialized multiton object OK
            Assert.NotNull(obj1);                                                               // test: object constructed and returned
            Assert.True(obj1 is object);                                                        // test: object returned matches bind type
            object obj2 = null;
            Assert.DoesNotThrow(() => { obj2 = instanceBinder.Get<object, string>("bar"); });   // test: getting uninitialized multiton object OK
            Assert.NotNull(obj2);                                                               // test: object constructed and returned
            Assert.True(obj2 is object);                                                        // test: object returned matches bind type
            Assert.AreNotEqual(obj1, obj2);                                                     // test: unique objects returned
        }

        [Test]
        public void GetMultitonObject()
        {
            InstanceBinder instanceBinder = new InstanceBinder();
            object obj1 = new object();
            instanceBinder.Bind<object, string>(obj1, "foo");
            object obj2 = null;
            Assert.DoesNotThrow(() => { obj2 = instanceBinder.Get<object, string>("foo"); });   // test: getting multiton object OK
            Assert.AreEqual(obj1, obj2);                                                        // test: object bound to type by name returned
            instanceBinder.Bind<object, string>(new object(), "bar");
            obj2 = null;
            Assert.DoesNotThrow(() => { obj2 = instanceBinder.Get<object, string>("bar"); });   // test: getting multiton object OK
            Assert.AreNotEqual(obj1, obj2);                                                     // test: different objects returned by name
        }

        [Test]
        public void GetRemoteInstance()
        {
            Context parentContext = new Context(Substitute.For<IInstanceBinder>(), Substitute.For<IMediationBinder>());
            Context localContext = new Context(new InstanceBinder(), Substitute.For<IMediationBinder>(), parentContext);

            localContext.InstanceBinder.Get<int>();         // won't throw because of parent IInstanceBinder substitution
            parentContext.InstanceBinder.Received(1).Get<int>();            // test: InstanceBinder tried to get missing singleton value in next-higher Context

            localContext.InstanceBinder.Get<object>();      // won't throw because of parent IInstanceBinder substitution
            parentContext.InstanceBinder.Received(1).Get<object>();         // test: InstanceBinder tried to get missing singleton object in next-higher Context

            localContext.InstanceBinder.Get<int, string>("foo");                // won't throw because of parent IInstanceBinder substitution
            parentContext.InstanceBinder.Received(1).Get<int, string>("foo");   // test: InstanceBinder tried to get missing multiton value in next-higher Context

            localContext.InstanceBinder.Get<object, string>("bar");                 // won't throw because of parent IInstanceBinder substitution
            parentContext.InstanceBinder.Received(1).Get<object, string>("bar");    // test: InstanceBinder tried to get missing multiton object in next-higher Context
        }

        [Test]
        public void UnbindSingletonDefaultValue()
        {
            Context context = new Context(new InstanceBinder(), Substitute.For<IMediationBinder>());
            IInstanceBinder instanceBinder = context.InstanceBinder;
            instanceBinder.Bind<int>();
            this.UnbindSuite(() => { instanceBinder.Unbind<int>(); }, () => { instanceBinder.Get<int>(); });
        }

        [Test]
        public void UnbindSingletonValue()
        {
            Context context = new Context(new InstanceBinder(), Substitute.For<IMediationBinder>());
            IInstanceBinder instanceBinder = context.InstanceBinder;
            instanceBinder.Bind<int>(11);
            this.UnbindSuite(() => { instanceBinder.Unbind<int>(); }, () => { instanceBinder.Get<int>(); });
        }

        [Test]
        public void UnbindSingletonDefaultObject()
        {
            Context context = new Context(new InstanceBinder(), Substitute.For<IMediationBinder>());
            IInstanceBinder instanceBinder = context.InstanceBinder;
            instanceBinder.Bind<object>();
            this.UnbindSuite(() => { instanceBinder.Unbind<object>(); }, () => { instanceBinder.Get<object>(); });
        }

        [Test]
        public void UnbindSingletonObject()
        {
            Context context = new Context(new InstanceBinder(), Substitute.For<IMediationBinder>());
            IInstanceBinder instanceBinder = context.InstanceBinder;
            instanceBinder.Bind<object>(new object());
            this.UnbindSuite(() => { instanceBinder.Unbind<object>(); }, () => { instanceBinder.Get<object>(); });
        }

        [Test]
        public void UnbindMultitonDefaultValue()
        {
            Context context = new Context(new InstanceBinder(), Substitute.For<IMediationBinder>());
            IInstanceBinder instanceBinder = context.InstanceBinder;
            instanceBinder.Bind<int, string>("foo");
            instanceBinder.Bind<int, string>("bar");
            this.UnbindSuite(() => { instanceBinder.Unbind<int, string>("foo"); }, () => { instanceBinder.Get<int, string>("foo"); });
            this.UnbindSuite(() => { instanceBinder.Unbind<int, string>("bar"); }, () => { instanceBinder.Get<int, string>("bar"); });
        }

        [Test]
        public void UnbindMultitonValue()
        {
            Context context = new Context(new InstanceBinder(), Substitute.For<IMediationBinder>());
            IInstanceBinder instanceBinder = context.InstanceBinder;
            instanceBinder.Bind<int, string>(11, "foo");
            instanceBinder.Bind<int, string>(22, "bar");
            this.UnbindSuite(() => { instanceBinder.Unbind<int, string>("foo"); }, () => { instanceBinder.Get<int, string>("foo"); });
            this.UnbindSuite(() => { instanceBinder.Unbind<int, string>("bar"); }, () => { instanceBinder.Get<int, string>("bar"); });
        }

        [Test]
        public void UnbindMultitonDefaultObject()
        {
            Context context = new Context(new InstanceBinder(), Substitute.For<IMediationBinder>());
            IInstanceBinder instanceBinder = context.InstanceBinder;
            instanceBinder.Bind<object, string>("foo");
            instanceBinder.Bind<object, string>("bar");
            this.UnbindSuite(() => { instanceBinder.Unbind<object, string>("foo"); }, () => { instanceBinder.Get<object, string>("foo"); });
            this.UnbindSuite(() => { instanceBinder.Unbind<object, string>("bar"); }, () => { instanceBinder.Get<object, string>("bar"); });
        }

        [Test]
        public void UnbindMultitonObject()
        {
            Context context = new Context(new InstanceBinder(), Substitute.For<IMediationBinder>());
            IInstanceBinder instanceBinder = context.InstanceBinder;
            instanceBinder.Bind<object, string>(new object(), "foo");
            instanceBinder.Bind<object, string>(new object(), "bar");
            this.UnbindSuite(() => { instanceBinder.Unbind<object, string>("foo"); }, () => { instanceBinder.Get<object, string>("foo"); });
            this.UnbindSuite(() => { instanceBinder.Unbind<object, string>("bar"); }, () => { instanceBinder.Get<object, string>("bar"); });
        }

        private void BindSingletonSuite(Action bindDelegate)
        {
            Assert.DoesNotThrow(() => { bindDelegate(); });                         // test: binding OK
            Assert.Throws<InvalidOperationException>(() => { bindDelegate(); });    // test: error second binding
        }

        private void BindMultitonSuite(Action bindDelegate)
        {
            Assert.DoesNotThrow(() => { bindDelegate(); });                 // test: binding OK
            Assert.Throws<ArgumentException>(() => { bindDelegate(); });    // test: error second binding
        }

        private void UnbindSuite(TestDelegate unbindDelegate, TestDelegate getDelegate)
        {
            Assert.DoesNotThrow(unbindDelegate);                    // test: unbinding OK
            Assert.Throws<KeyNotFoundException>(unbindDelegate);    // test: cannot unbind nonexistent binding
            Assert.Throws<KeyNotFoundException>(getDelegate);       // test: cannot get nonexistent binding
        }
    }
}
