﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs.Test.Unit
{
    using Goyfs;
    using Goyfs.Test.Common;
    using NSubstitute;
    using NUnit.Framework;
    using System;
    using System.Collections;

    [TestFixture]
    internal class MediationBinder_UnitTest
    {
        [Test]
        public void InitialState()
        {
            MediationBinder mediationBinder = new MediationBinder();
            Assert.False(mediationBinder.Disposed); // test: initial state undisposed
        }

        [Test]
        public void DisposedState()
        {
            MediatorImpl.Instances.Clear();
            MediationBinder mediationBinder = new MediationBinder();
            mediationBinder.Bind<object, MediatorImpl.A<object>>();
            mediationBinder.Mediate(new object());
            
            Assert.DoesNotThrow(() => { mediationBinder.Dispose(); });  // test: disposing OK
            Assert.Throws<ObjectDisposedException>(() => { mediationBinder.Bind<object, MediatorImpl.A<object>>(); });  // test: MediationBinder cannot be used after disposal
            Assert.Throws<ObjectDisposedException>(() => { mediationBinder.Unbind<object>(); });                        // test: MediationBinder cannot be used after disposal
            Assert.Throws<ObjectDisposedException>(() => { mediationBinder.Mediate(new object()); });                   // test: MediationBinder cannot be used after disposal
            Assert.Throws<ObjectDisposedException>(() => { mediationBinder.Unmediate(new object()); });                 // test: MediationBinder cannot be used after disposal
            Assert.DoesNotThrow(() => { mediationBinder.Dispose(); });  // test: redundant disposal OK
            Assert.True(MediatorImpl.Instances.TrueForAll((MediatorImpl.Instance x) =>
            {
                // test: MediationBinder disposed of its Mediators
                return x.Mediator is MediatorImpl.A<object>
                    && (x.Mediator as MediatorImpl.A<object>).Disposed;
            }));
        }

        [Test]
        public void Bind()
        {
            MediatorImpl.Instances.Clear();
            MediationBinder mediationBinder = new MediationBinder();

            Assert.DoesNotThrow(() => { mediationBinder.Bind<object, MediatorImpl.A<object>>(); });                         // test: binding view/mediator pair OK
            Assert.Throws<InvalidOperationException>(() => { mediationBinder.Bind<object, MediatorImpl.A<object>>(); });    // test: error view type already has binding
            Assert.Throws<InvalidOperationException>(() => { mediationBinder.Bind<object, MediatorImpl.B<object>>(); });    // test: error view type already has binding (even though mediator differs)
            Assert.False(MediatorImpl.Instances.Exists((MediatorImpl.Instance x) =>
            {
                // test: MediationBinder didn't construct the mediator without Mediate() call
                return x.Mediator is MediatorImpl.A<object>;
            }));
        }

        [Test]
        public void Mediate()
        {
            MediatorImpl.Instances.Clear();
            MediationBinder mediationBinder = new MediationBinder();
            mediationBinder.Bind<object, MediatorImpl.A<object>>();

            object view1 = new object();
            Assert.True(mediationBinder.Mediate(view1));    // test: mediation OK
            Assert.True(MediatorImpl.Instances.Exists((MediatorImpl.Instance x) =>
            {
                // test: MediatorImpl.A constructed
                return x.Mediator is MediatorImpl.A<object>
                    && view1.Equals(x.View);
            }));

            object view2 = new object();
            Assert.True(mediationBinder.Mediate(view2));    // test: mediation of another instance of view type OK
            Assert.True(MediatorImpl.Instances.FindAll((MediatorImpl.Instance x) =>
            {
                // test: 1 more MediatorImpl.A constructed
                return x.Mediator is MediatorImpl.A<object>
                    && view2.Equals(x.View);
            })
            .Count == 1);
        }

        [Test]
        public void Unmediate()
        {
            MediatorImpl.Instances.Clear();
            MediationBinder mediationBinder = new MediationBinder();
            mediationBinder.Bind<object, MediatorImpl.A<object>>();
            
            Assert.False(mediationBinder.Unmediate(new object()));   // test: unmediating a view that isn't already mediated should return false
            object view1 = new object();
            object view2 = new object();
            mediationBinder.Mediate(view1);
            mediationBinder.Mediate(view2);

            Assert.True(mediationBinder.Unmediate(view1));          // test: unmediating a mediated view OK, returns true
            Assert.True(MediatorImpl.Instances.FindAll((MediatorImpl.Instance x) =>
            {
                // test: only the MediatorImpl.A mediating view1 was disposed
                return x.Mediator is MediatorImpl.A<object>
                    && (x.Mediator as MediatorImpl.A<object>).Disposed
                    && view1.Equals(x.View);
            })
            .Count == 1);
            Assert.True(mediationBinder.Unmediate(view2));         // test: unmediating other mediated view OK, returns true
            Assert.True(MediatorImpl.Instances.TrueForAll((MediatorImpl.Instance x) =>
            {
                // test: all mediators should have been disposed by now
                return x.Mediator is MediatorImpl.A<object>
                    && (x.Mediator as MediatorImpl.A<object>).Disposed;
            }));
        }

        [Test]
        public void Unbind()
        {
            MediatorImpl.Instances.Clear();
            MediationBinder mediationBinder = new MediationBinder();
            Assert.Throws<InvalidOperationException>(() => { mediationBinder.Unbind<object>(); });  // test: binding not found
            mediationBinder.Bind<object, MediatorImpl.A<object>>();
            mediationBinder.Mediate(new object());

            Assert.DoesNotThrow(() => { mediationBinder.Unbind<object>(); });   // test: unbinding active mediator OK
            Assert.False(MediatorImpl.Instances.TrueForAll((MediatorImpl.Instance x) =>
            {
                // test: active mediator not disposed (allow it to exist until unmediated)
                return x.Mediator is MediatorImpl.A<object>
                    && (x.Mediator as MediatorImpl.A<object>).Disposed;
            }));
        }

        [Test]
        public void ViewAsInterface()
        {
            MediatorImpl.Instances.Clear();
            MediationBinder mediationBinder = new MediationBinder();
            mediationBinder.Bind<int[], MediatorImpl.A<IEnumerable>>();
            int[] array = new int[1];
            
            Assert.DoesNotThrow(() => { mediationBinder.Mediate(array); }); // test: mediating the array OK
            Assert.True(MediatorImpl.Instances.Exists((MediatorImpl.Instance x) => { return array.Equals(x.View); }));  // test: view was cast to an IEnumerable for the Mediator
        }

        [Test]
        public void MediateViaAncestor()
        {
            MediationBinder mediationBinderA = new MediationBinder();
            MediationBinder mediationBinderB = new MediationBinder();
            Context contextA = new Context(Substitute.For<IInstanceBinder>(), mediationBinderA);
            Context contextB = new Context(Substitute.For<IInstanceBinder>(), mediationBinderB, contextA);
            mediationBinderA.Bind<object, MediatorImpl.A<object>>();
            object viewB = new object();

            Assert.True(mediationBinderB.Mediate(viewB));   // test: mediation successful despite mediationBinderB not defining a binding for viewB
            Assert.True(MediatorImpl.Instances.Exists((MediatorImpl.Instance x) =>
            {
                // test: mediationBinderB forwarded unfulfilled mediation request to ancestor mediationBinderA
                return x.Mediator is MediatorImpl.A<object>
                    && x.View == viewB
                    && x.Context == contextB;   // test: contextB was injected into created IMediator, not the ancestor's contextA
            }));
        }
    }
}
