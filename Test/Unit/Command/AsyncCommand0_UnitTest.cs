﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs.Test.Unit
{
    using Goyfs;
    using Goyfs.Test.Common;
    using NSubstitute;
    using NUnit.Framework;
    using System;

    [TestFixture]
    internal class AsyncCommand0_UnitTest
    {
        [Test]
        public void AsyncCommand()
        {
            Signal signal = new Signal();
            IInstanceBinder instanceBinder = Substitute.For<IInstanceBinder>();
            instanceBinder.Get<IAsyncExecutor>().Returns(new AsyncExecutorImpl());
            Context context = new Context(instanceBinder, Substitute.For<IMediationBinder>());
            signal.AddCommand<AsyncCommand0Impl>(context);

            Assert.DoesNotThrow(() => { signal.Dispatch(); });  // test: "in line" async command works
        }

        [Test]
        public void AsyncCommandNoTaskRunner()
        {
            Signal signal = new Signal();
            Context context = new Context(new InstanceBinder(), Substitute.For<IMediationBinder>());
            signal.AddCommand<AsyncCommand0Impl>(context);
            Assert.Throws<InvalidOperationException>(() => { signal.Dispatch(); }); // test: async command throws without IAsyncCommandRunner available
        }
    }
}
