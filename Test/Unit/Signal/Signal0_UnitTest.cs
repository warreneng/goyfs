﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs.Test.Unit
{
    using Goyfs;
    using Goyfs.Test.Common;
    using NSubstitute;
    using NUnit.Framework;
    using System;

    [TestFixture]
    internal class Signal0_UnitTest
    {
        [Test]
        public void InitialState()
        {
            Signal signal = new Signal();
            Assert.False(signal.Disposed, "should be undisposed after construction");
        }

        [Test]
        public void EmptyDispatch()
        {
            Signal signal = new Signal();
            Assert.DoesNotThrow(() => { signal.Dispatch(); });  // test: dispatching with no listeners is OK
        }

        [Test]
        public void SingleListener()
        {
            Signal signal = new Signal();
            IListener0 listener = Substitute.For<IListener0>();

            // add listener, dispatch
            signal.AddListener(listener.Callback);
            signal.Dispatch();
            listener.Received(1).Callback();    // test: listener was invoked exactly once

            // remove listener, dispatch again
            listener.ClearReceivedCalls();
            signal.RemoveListener(listener.Callback);
            signal.Dispatch();
            listener.Received(0).Callback();    // test: listener was not invoked again
        }

        [Test]
        public void DoubleListener()
        {
            Signal signal = new Signal();
            IListener0 listenerA = Substitute.For<IListener0>();
            IListener0 listenerB = Substitute.For<IListener0>();

            // add listeners, dispatch
            signal.AddListener(listenerA.Callback);
            signal.AddListener(listenerB.Callback);
            signal.Dispatch();
            listenerA.Received(1).Callback();   // test: listener was invoked exactly once
            listenerB.Received(1).Callback();   // test: listener was invoked exactly once

            // remove listenerA, dispatch
            listenerA.ClearReceivedCalls();
            listenerB.ClearReceivedCalls();
            signal.RemoveListener(listenerA.Callback);
            signal.Dispatch();
            listenerA.Received(0).Callback();   // test: listener was not invoked again
            listenerB.Received(1).Callback();   // test: listener was invoked again
            Assert.DoesNotThrow(() => { signal.RemoveListener(listenerA.Callback); });  // test: redundant RemoveListener() OK

            // remove listenerB, dispatch
            listenerA.ClearReceivedCalls();
            listenerB.ClearReceivedCalls();
            signal.RemoveListener(listenerB.Callback);
            signal.Dispatch();
            listenerA.Received(0).Callback();   // test: listenerA not invoked
            listenerB.Received(0).Callback();   // test: listenerB not invoked
        }

        [Test]
        public void DoubleListenerOrder()
        {
            Signal signal = new Signal();
            Listener0Impl listenerA = new Listener0Impl();
            Listener0Impl listenerB = new Listener0Impl();

            // add listeners, dispatch
            signal.AddListener(listenerA.Callback);
            signal.AddListener(listenerB.Callback);
            signal.Dispatch();
            Assert.Less(listenerA.LastCallback, listenerB.LastCallback);    // test: listener invocation order matches addition order
        }

        [Test]
        public void DuplicateListener()
        {
            Signal signal = new Signal();
            IListener0 listener = Substitute.For<IListener0>();

            // add listener twice, dispatch
            signal.AddListener(listener.Callback);
            Assert.DoesNotThrow(() => { signal.AddListener(listener.Callback); });  // test: redundant AddListener() OK
            signal.Dispatch();
            listener.Received(1).Callback();    // test: listener was invoked exactly once
            listener.ClearReceivedCalls();
            signal.RemoveListener(listener.Callback);
            Assert.DoesNotThrow(() => { signal.RemoveListener(listener.Callback); });   // test: redundant RemoveListener() OK
            listener.Received(0).Callback();    // test: listener not invoked again
        }

        [Test]
        public void DetectListenerLoop()
        {
            Signal signal = new Signal();
            
            // add listener, dispatch
            signal.AddListener(() => { signal.Dispatch(); });
            Assert.Throws<InvalidOperationException>(() => { signal.Dispatch(); }); // test: dispatching signal again in listener is an error
        }

        [Test]
        public void SingleCommand()
        {
            Signal signal = new Signal();
            
            // add command, dispatch
            signal.AddCommand<Command0Impl.A>(null);
            int sequence = Sequence.Next;
            signal.Dispatch();
            Assert.NotNull(Command0Impl.A.Instance);  // test: command should have been constructed
            Assert.AreEqual(sequence + 1, Command0Impl.A.Instance.LastExecute);    // test: an instance's Execute was invoked

            // remove command, dispatch again
            signal.RemoveCommand<Command0Impl.A>();
            sequence = Command0Impl.A.Instance.LastExecute;
            signal.Dispatch();
            Assert.AreEqual(sequence, Command0Impl.A.Instance.LastExecute); // test: removed command doesn't Execute again
        }

        [Test]
        public void DoubleCommand()
        {
            Signal signal = new Signal();

            // add commands, dispatch
            signal.AddCommand<Command0Impl.A>(null);
            signal.AddCommand<Command0Impl.B>(null);
            int sequence = Sequence.Next;
            signal.Dispatch();
            Assert.AreEqual(sequence + 1, Command0Impl.A.Instance.LastExecute); // test: an instance's Execute was invoked in expected order
            Assert.AreEqual(sequence + 2, Command0Impl.B.Instance.LastExecute); // test: an instance's Execute was invoked in expected order

            // remove command A, dispatch
            signal.RemoveCommand<Command0Impl.A>();
            sequence = Command0Impl.B.Instance.LastExecute;
            signal.Dispatch();
            Assert.AreEqual(sequence - 1, Command0Impl.A.Instance.LastExecute); // test: A's Execute was not invoked again
            Assert.AreEqual(sequence + 1, Command0Impl.B.Instance.LastExecute); // test: B's Execute was invoked again

            // remove command B, dispatch
            signal.RemoveCommand<Command0Impl.B>();
            sequence = Command0Impl.B.Instance.LastExecute;
            signal.Dispatch();
            Assert.AreEqual(sequence, Command0Impl.B.Instance.LastExecute); // test: B's Execute was not invoked again
        }

        [Test]
        public void DuplicateCommand()
        {
            Signal signal = new Signal();
            
            // add same command twice, dispatch
            signal.AddCommand<Command0Impl.A>(null);
            Assert.DoesNotThrow(() => { signal.AddCommand<Command0Impl.A>(null); });    // test: redundant AddCommand() OK
            int sequence = Sequence.Next;
            signal.Dispatch();
            Assert.AreEqual(sequence + 1, Command0Impl.A.Instance.LastExecute); // test: an instance's Execute was invoked exactly once
            signal.RemoveCommand<Command0Impl.A>();
            Assert.DoesNotThrow(() => { signal.RemoveCommand<Command0Impl.A>(); }); // test: redundant RemoveCommand() OK
        }

        [Test]
        public void ListenerCommandMix()
        {
            Signal signal = new Signal();

            // add listener and command, dispatch
            Listener0Impl listenerA = new Listener0Impl();
            Listener0Impl listenerB = new Listener0Impl();
            signal.AddListener(listenerA.Callback);
            signal.AddCommand<Command0Impl.A>(null);
            signal.AddListener(listenerB.Callback);
            signal.AddCommand<Command0Impl.B>(null);
            int sequence = Sequence.Next;
            signal.Dispatch();
            Assert.AreEqual(sequence + 1, Command0Impl.A.Instance.LastExecute); // test: command was Executed in expected order (commands always first)
            Assert.AreEqual(sequence + 2, Command0Impl.B.Instance.LastExecute); // test: command was Executed in expected order (commands always first)
            Assert.AreEqual(sequence + 3, listenerA.LastCallback);  // test: listener was invoked in expected order (listeners always last)
            Assert.AreEqual(sequence + 4, listenerB.LastCallback);  // test: listener was invoked in expected order (listeners always last)
        }

        [Test]
        public void DeferredListenerAddition()
        {
            Signal signal = new Signal();

            // add listeners, dispatch
            IListener0 listenerA = Substitute.For<IListener0>();
            IListener0 listenerB = Substitute.For<IListener0>();
            signal.AddListener(listenerA.Callback);
            signal.AddListener(() => { signal.AddListener(listenerB.Callback); });
            Assert.DoesNotThrow(() => { signal.Dispatch(); });   // test: dispatch OK when listener adds another listener
            listenerA.Received(1).Callback();   // test: first listener was called
            listenerB.Received(0).Callback();   // test: listener added while signal in dispatching state not called
            signal.Dispatch();
            listenerA.Received(2).Callback();   // test: first listener called again
            listenerB.Received(1).Callback();   // test: listenerB was added after signal finished dispatching
        }

        [Test]
        public void DeferredListenerRemoval()
        {
            Signal signal = new Signal();

            // add listeners, dispatch
            IListener0 listener = Substitute.For<IListener0>();
            signal.AddListener(() => { signal.RemoveListener(listener.Callback); });
            signal.AddListener(listener.Callback);
            Assert.DoesNotThrow(() => { signal.Dispatch(); });  // test: dispatch OK when listener removed in another listener
            listener.Received(1).Callback();    // test: listener still called despite deferred removal
            signal.Dispatch();
            listener.Received(1).Callback();    // test: listener not called again after deferred removal
        }

        [Test]
        public void DeferredCommandAddition()
        {
            Signal signal = new Signal();

            int sequence = Sequence.Next;
            Command0Impl.A.Instance = null;
            signal.AddListener(() => { signal.AddCommand<Command0Impl.A>(null); });
            Assert.DoesNotThrow(() => { signal.Dispatch(); });  // test: dispatch OK when listener adds a command
            Assert.Null(Command0Impl.A.Instance);   // test: command was not constructed before deferred addition
            signal.Dispatch();
            Assert.Less(sequence, Command0Impl.A.Instance.LastExecute); // test: command was eventually added to signal
        }

        [Test]
        public void DeferredCommandRemoval()
        {
            Signal signal = new Signal();
            IInstanceBinder instanceBinder = Substitute.For<IInstanceBinder>();
            instanceBinder.Get<Signal>().Returns(signal);
            Context context = new Context(instanceBinder, Substitute.For<IMediationBinder>());

            int sequence = Sequence.Next;
            signal.AddCommand<Command0Impl.Remove<Signal, Command0Impl.A>>(context);
            signal.AddCommand<Command0Impl.A>(context);
            Assert.DoesNotThrow(() => { signal.Dispatch(); });  // test: OK for command to remove another command during execution
            Assert.AreEqual(sequence + 2, Command0Impl.A.Instance.LastExecute); // test: Command0Impl.A executed despite removal
            signal.Dispatch();
            Assert.AreEqual(sequence + 2, Command0Impl.A.Instance.LastExecute); // test: A was eventually removed from signal
        }

        [Test]
        public void DisposedState()
        {
            // test that the signal disposes OK and cannot be used further
            Signal signal = new Signal();

            Assert.DoesNotThrow(() => { signal.Dispose(); });   // test: disposal OK
            Assert.Throws<ObjectDisposedException>(() => { signal.AddListener(() => { }); });           // test: not allowed to use signal after disposal
            Assert.Throws<ObjectDisposedException>(() => { signal.RemoveListener(() => { }); });        // test: not allowed to use signal after disposal
            Assert.Throws<ObjectDisposedException>(() => { signal.AddCommand<Command0Impl.A>(null); }); // test: not allowed to use signal after disposal
            Assert.Throws<ObjectDisposedException>(() => { signal.RemoveCommand<Command0Impl.A>(); });  // test: not allowed to use signal after disposal
            Assert.Throws<ObjectDisposedException>(() => { signal.Dispatch(); });                       // test: not allowed to use signal after disposal
        }

        [Test]
        public void DisposeListeners()
        {
            // test that the signal disposes OK with listeners bound
            Signal signal = new Signal();
            IListener0 listener = Substitute.For<IListener0>();

            signal.AddListener(listener.Callback);
            Assert.DoesNotThrow(() => { signal.Dispose(); });   // test: disposal with listeners mapped OK
            Assert.Throws<ObjectDisposedException>(() => { signal.Dispatch(); });   // test: not allowed to use signal after disposal
            listener.Received(0).Callback();    // test: listener was never called after disposal
        }

        [Test]
        public void DisposeCommands()
        {
            // test that the signal disposes OK with commands bound
            Signal signal = new Signal();

            int sequence = Sequence.Next;
            signal.AddCommand<Command0Impl.A>(null);
            Assert.DoesNotThrow(() => { signal.Dispose(); });   // test: disposal with commands mapped OK
            Assert.Throws<ObjectDisposedException>(() => { signal.Dispatch(); });   // test: not allowed to use signal after disposal
            Assert.Greater(sequence, Command0Impl.A.Instance.LastExecute);  // test: command never ran
        }

        [Test]
        public void ContextInjectedIntoCommand()
        {
            // 2017-20-07: Signals are no longer IContextSensitive
            Context context = new Context(new InstanceBinder(), Substitute.For<IMediationBinder>());
            Signal signal = new Signal();
            signal.AddCommand<Command0Impl.A>(context);
            signal.Dispatch();
            Assert.AreSame(context, Command0Impl.A.Instance.Context);   // test: Context was provided to command eventually
        }

        [Test]
        public void LazilyConstructedSignalDoesNotInjectContext()
        {
            // 2017-20-07: Signals are no longer IContextSensitive
            Context context = new Context(new InstanceBinder(), Substitute.For<IMediationBinder>());
            context.InstanceBinder.Bind<Signal>();
            Signal signal = context.InstanceBinder.Get<Signal>();
            signal.AddCommand<Command0Impl.A>(null);
            signal.Dispatch();
            Assert.IsNull(Command0Impl.A.Instance.Context); // test: Context not set on Command if we didn't provide it, even if signal was bound to an IInstanceBinder within a Context
        }

        [Test]
        public void ManuallyConstructedSignalDoesNotInjectContext()
        {
            // 2017-20-07: Signals are no longer IContextSensitive
            Context context = new Context(new InstanceBinder(), Substitute.For<IMediationBinder>());
            Signal signal = new Signal();
            signal.AddCommand<Command0Impl.A>(null);
            context.InstanceBinder.Bind<Signal>(signal);
            signal.Dispatch();
            Assert.IsNull(Command0Impl.A.Instance.Context); // test: context didn't get injected into Command despite signal being bound to one
        }

        [Test]
        public void DispatchRecoversFromThrow()
        {
            // 2016-16-06 recover from listeners and commands throwing errors and ensure signal sets state back to IDLE
            Context context = new Context(new InstanceBinder(), Substitute.For<IMediationBinder>());
            Signal signal = new Signal();
            Action listener = () => { throw new NotImplementedException(); };
            signal.AddListener(listener);
            Assert.Throws(typeof(NotImplementedException), () => { signal.Dispatch(); }); // test: we expect the listener to simulate uncaught exception
            signal.RemoveListener(listener);
            Assert.DoesNotThrow(() => { signal.Dispatch(); });  // test: signal recovered from earlier exception and is back in IDLE state
        }
    }
}
