﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs.Test.Unit
{
    using Goyfs;
    using Goyfs.Test.Common;
    using NSubstitute;
    using NUnit.Framework;
    using System;

    [TestFixture]
    internal class Signal3_UnitTest
    {
        ////////// Standard tests (see Signal0_UnitTest) ////////// 

        [Test]
        public void InitialState()
        {
            Signal<int, int, int> signal = new Signal<int, int, int>();
            Assert.False(signal.Disposed, "should be undisposed after construction");
        }

        [Test]
        public void EmptyDispatch()
        {
            Signal<int, int, int> signal = new Signal<int, int, int>();
            Assert.DoesNotThrow(() => { signal.Dispatch(0, 0, 0); });  // test: dispatching with no listeners is OK
        }

        [Test]
        public void SingleListener()
        {
            Signal<int, int, int> signal = new Signal<int, int, int>();
            IListener3<int, int, int> listener = Substitute.For<IListener3<int, int, int>>();

            // add listener, dispatch
            signal.AddListener(listener.Callback);
            signal.Dispatch(0, 1, 2);
            listener.Received(1).Callback(0, 1, 2);    // test: listener was invoked exactly once

            // remove listener, dispatch again
            listener.ClearReceivedCalls();
            signal.RemoveListener(listener.Callback);
            signal.Dispatch(0, 1, 2);
            listener.Received(0).Callback(0, 1, 2);  // test: listener was not invoked again
        }

        [Test]
        public void DoubleListener()
        {
            Signal<int, int, int> signal = new Signal<int, int, int>();
            IListener3<int, int, int> listenerA = Substitute.For<IListener3<int, int, int>>();
            IListener3<int, int, int> listenerB = Substitute.For<IListener3<int, int, int>>();

            // add listeners, dispatch
            signal.AddListener(listenerA.Callback);
            signal.AddListener(listenerB.Callback);
            signal.Dispatch(0, 1, 2);
            listenerA.Received(1).Callback(0, 1, 2);   // test: listener was invoked exactly once
            listenerB.Received(1).Callback(0, 1, 2);   // test: listener was invoked exactly once

            // remove listenerA, dispatch
            listenerA.ClearReceivedCalls();
            listenerB.ClearReceivedCalls();
            signal.RemoveListener(listenerA.Callback);
            signal.Dispatch(0, 1, 2);
            listenerA.Received(0).Callback(0, 1, 2); // test: listener was not invoked again
            listenerB.Received(1).Callback(0, 1, 2); // test: listener was invoked again
            Assert.DoesNotThrow(() => { signal.RemoveListener(listenerA.Callback); });  // test: redundant RemoveListener() OK

            // remove listenerB, dispatch
            listenerA.ClearReceivedCalls();
            listenerB.ClearReceivedCalls();
            signal.RemoveListener(listenerB.Callback);
            signal.Dispatch(0, 1, 2);
            listenerA.Received(0).Callback(0, 1, 2); // test: listenerA not invoked
            listenerB.Received(0).Callback(0, 1, 2); // test: listenerB not invoked
        }

        [Test]
        public void DoubleListenerOrder()
        {
            Signal<int, int, int> signal = new Signal<int, int, int>();
            Listener3Impl<int, int, int> listenerA = new Listener3Impl<int, int, int>();
            Listener3Impl<int, int, int> listenerB = new Listener3Impl<int, int, int>();

            // add listeners, dispatch
            signal.AddListener(listenerA.Callback);
            signal.AddListener(listenerB.Callback);
            signal.Dispatch(0, 1, 2);
            Assert.Less(listenerA.LastCallback, listenerB.LastCallback);    // test: listener invocation order matches addition order
        }

        [Test]
        public void DuplicateListener()
        {
            Signal<int, int, int> signal = new Signal<int, int, int>();
            IListener3<int, int, int> listener = Substitute.For<IListener3<int, int, int>>();

            // add listener twice, dispatch
            signal.AddListener(listener.Callback);
            Assert.DoesNotThrow(() => { signal.AddListener(listener.Callback); });  // test: redundant AddListener() OK
            signal.Dispatch(0, 1, 2);
            listener.Received(1).Callback(0, 1, 2);    // test: listener was invoked exactly once
            listener.ClearReceivedCalls();
            signal.RemoveListener(listener.Callback);
            Assert.DoesNotThrow(() => { signal.RemoveListener(listener.Callback); });   // test: redundant RemoveListener() OK
            signal.Dispatch(0, 1, 2);
            listener.Received(0).Callback(0, 1, 2);  // test: listener not invoked again
        }

        [Test]
        public void DetectListenerLoop()
        {
            Signal<int, int, int> signal = new Signal<int, int, int>();

            // add listener, dispatch
            signal.AddListener((x, y, z) => { signal.Dispatch(x, y, z); });
            Assert.Throws<InvalidOperationException>(() => { signal.Dispatch(0, 1, 2); }); // test: dispatching signal again in listener is an error
        }

        [Test]
        public void SingleCommand()
        {
            Signal<int, int, int> signal = new Signal<int, int, int>();

            // add command, dispatch
            signal.AddCommand<Command3Impl<int, int, int>.A>(null);
            int sequence = Sequence.Next;
            signal.Dispatch(0, 1, 2);
            Assert.NotNull(Command3Impl<int, int, int>.A.Instance); // test: command should have been constructed
            Assert.AreEqual(sequence + 1, Command3Impl<int, int, int>.A.Instance.LastExecute);   // test: an instance's Execute was invoked

            // remove command, dispatch again
            signal.RemoveCommand<Command3Impl<int, int, int>.A>();
            sequence = Command3Impl<int, int, int>.A.Instance.LastExecute;
            signal.Dispatch(0, 1, 2);
            Assert.AreEqual(sequence, Command3Impl<int, int, int>.A.Instance.LastExecute);   // test: removed command doesn't Execute again
        }

        [Test]
        public void DoubleCommand()
        {
            Signal<int, int, int> signal = new Signal<int, int, int>();

            // add commands, dispatch
            signal.AddCommand<Command3Impl<int, int, int>.A>(null);
            signal.AddCommand<Command3Impl<int, int, int>.B>(null);
            int sequence = Sequence.Next;
            signal.Dispatch(0, 1, 2);
            Assert.AreEqual(sequence + 1, Command3Impl<int, int, int>.A.Instance.LastExecute);   // test: an instance's Execute was invoked in expected order
            Assert.AreEqual(sequence + 2, Command3Impl<int, int, int>.B.Instance.LastExecute);   // test: an instance's Execute was invoked in expected order

            // remove command A, dispatch
            signal.RemoveCommand<Command3Impl<int, int, int>.A>();
            sequence = Command3Impl<int, int, int>.B.Instance.LastExecute;
            signal.Dispatch(0, 1, 2);
            Assert.AreEqual(sequence - 1, Command3Impl<int, int, int>.A.Instance.LastExecute);   // test: A's Execute was not invoked again
            Assert.AreEqual(sequence + 1, Command3Impl<int, int, int>.B.Instance.LastExecute);   // test: B's Execute was invoked again

            // remove command B, dispatch
            signal.RemoveCommand<Command3Impl<int, int, int>.B>();
            sequence = Command3Impl<int, int, int>.B.Instance.LastExecute;
            signal.Dispatch(0, 1, 2);
            Assert.AreEqual(sequence, Command3Impl<int, int, int>.B.Instance.LastExecute);   // test: B's execute was not invoked again
        }

        [Test]
        public void DuplicateCommand()
        {
            Signal<int, int, int> signal = new Signal<int, int, int>();

            // add same command twice, dispatch
            signal.AddCommand<Command3Impl<int, int, int>.A>(null);
            Assert.DoesNotThrow(() => { signal.AddCommand<Command3Impl<int, int, int>.A>(null); });  // test: redundant AddCommand() OK
            int sequence = Sequence.Next;
            signal.Dispatch(0, 1, 2);
            Assert.AreEqual(sequence + 1, Command3Impl<int, int, int>.A.Instance.LastExecute);   // test: an instance's Execute was invoked exactly once
            signal.RemoveCommand<Command3Impl<int, int, int>.A>();
            Assert.DoesNotThrow(() => { signal.RemoveCommand<Command3Impl<int, int, int>.A>(); });   // test: redundant RemoveCommand() OK
        }

        [Test]
        public void ListenerCommandMix()
        {
            Signal<int, int, int> signal = new Signal<int, int, int>();

            // add listener and command, dispatch
            Listener3Impl<int, int, int> listenerA = new Listener3Impl<int, int, int>();
            Listener3Impl<int, int, int> listenerB = new Listener3Impl<int, int, int>();
            signal.AddListener(listenerA.Callback);
            signal.AddCommand<Command3Impl<int, int, int>.A>(null);
            signal.AddListener(listenerB.Callback);
            signal.AddCommand<Command3Impl<int, int, int>.B>(null);
            int sequence = Sequence.Next;
            signal.Dispatch(0, 1, 2);
            Assert.AreEqual(sequence + 1, Command3Impl<int, int, int>.A.Instance.LastExecute);   // test: command was Executed in expected order (commands always first)
            Assert.AreEqual(sequence + 2, Command3Impl<int, int, int>.B.Instance.LastExecute);   // test: command was Executed in expected order (commands always first)
            Assert.AreEqual(sequence + 3, listenerA.LastCallback);  // test: listener was invoked in expected order (listeners always last)
            Assert.AreEqual(sequence + 4, listenerB.LastCallback);  // test: listener was invoked in expected order (listeners always last)
        }

        [Test]
        public void DeferredListenerAddition()
        {
            Signal<int, int, int> signal = new Signal<int, int, int>();

            // add listeners, dispatch
            IListener3<int, int, int> listenerA = Substitute.For<IListener3<int, int, int>>();
            IListener3<int, int, int> listenerB = Substitute.For<IListener3<int, int, int>>();
            signal.AddListener(listenerA.Callback);
            signal.AddListener((x, y, z) => { signal.AddListener(listenerB.Callback); });
            Assert.DoesNotThrow(() => { signal.Dispatch(0, 1, 2); });  // test: dispatch OK when listener adds another listener
            listenerA.Received(1).Callback(0, 1, 2);   // test: first listener was invoked
            listenerB.Received(0).Callback(0, 1, 2);   // test: listener added while signal in dispatching state not called
            signal.Dispatch(0, 1, 2);
            listenerA.Received(2).Callback(0, 1, 2);   // test: first listener was called again
            listenerB.Received(1).Callback(0, 1, 2);   // test: listenerB was added after signal finished dispatching first time
        }

        [Test]
        public void DeferredListenerRemoval()
        {
            Signal<int, int, int> signal = new Signal<int, int, int>();

            // add listeners, dispatch
            IListener3<int, int, int> listener = Substitute.For<IListener3<int, int, int>>();
            signal.AddListener((x, y, z) => signal.RemoveListener(listener.Callback));
            signal.AddListener(listener.Callback);
            Assert.DoesNotThrow(() => { signal.Dispatch(0, 1, 2); });  // test: dispatch OK when listener removed in another listener
            listener.Received(1).Callback(0, 1, 2);    // test: listener still invoked despite deferred removal
            signal.Dispatch(3, 4, 5);
            listener.Received(0).Callback(3, 4, 5);   // test: listener not called again after deferred removal
        }

        [Test]
        public void DeferredCommandAddition()
        {
            Signal<int, int, int> signal = new Signal<int, int, int>();

            int sequence = Sequence.Next;
            Command3Impl<int, int, int>.A.Instance = null;
            signal.AddListener((x, y, z) => signal.AddCommand<Command3Impl<int, int, int>.A>(null));
            Assert.DoesNotThrow(() => { signal.Dispatch(0, 1, 2); });  // test: dispatch OK when listener adds command
            Assert.Null(Command3Impl<int, int, int>.A.Instance); // test: command was not constructed before deferred addition
            signal.Dispatch(0, 1, 2);
            Assert.Less(sequence, Command3Impl<int, int, int>.A.Instance.LastExecute);   // test: command was eventually added to signal
        }

        [Test]
        public void DeferrredCommandRemoval()
        {
            Signal<int, int, int> signal = new Signal<int, int, int>();
            IInstanceBinder instanceBinder = Substitute.For<IInstanceBinder>();
            instanceBinder.Get<Signal<int, int, int>>().Returns(signal);
            Context context = new Context(instanceBinder, Substitute.For<IMediationBinder>());

            int sequence = Sequence.Next;
            signal.AddCommand<Command3Impl<int, int, int>.Remove<Signal<int, int, int>, Command3Impl<int, int, int>.A>>(context);
            signal.AddCommand<Command3Impl<int, int, int>.A>(context);
            Assert.DoesNotThrow(() => { signal.Dispatch(0, 1, 2); });  // test: OK for command to remove another command during execution
            Assert.AreEqual(sequence + 2, Command3Impl<int, int, int>.A.Instance.LastExecute);   // test: Command3Impl<int, int, int>.A executed despite removal
            signal.Dispatch(0, 1, 2);
            Assert.AreEqual(sequence + 2, Command3Impl<int, int, int>.A.Instance.LastExecute);   // test: Command3Impl<int, int, int>.A eventually removed from signal
        }

        [Test]
        public void DisposeListeners()
        {
            // test that the signal disposes OK with listeners bound
            Signal<int, int, int> signal = new Signal<int, int, int>();
            IListener3<int, int, int> listener = Substitute.For<IListener3<int, int, int>>();

            signal.AddListener(listener.Callback);
            Assert.DoesNotThrow(() => { signal.Dispose(); });   // test: disposal with listeners mapped OK
            Assert.Throws<ObjectDisposedException>(() => { signal.Dispatch(0, 1, 2); });   // test: not allowed to use signal after disposal
            listener.Received(0).Callback(0, 1, 2);    // test: listener was never called after disposal
        }

        [Test]
        public void DisposeCommands()
        {
            // test that the signal disposes OK with commands bound
            Signal<int, int, int> signal = new Signal<int, int, int>();

            int sequence = Sequence.Next;
            signal.AddCommand<Command3Impl<int, int, int>.A>(null);
            Assert.DoesNotThrow(() => { signal.Dispose(); });   // test: disposal with commands OK
            Assert.Throws<ObjectDisposedException>(() => { signal.Dispatch(0, 1, 2); });   // test: not allowed to use signal after disposal
            Assert.Greater(sequence, Command3Impl<int, int, int>.A.Instance.LastExecute);    // test: command never ran
        }

        [Test]
        public void ContextInjectedIntoCommand()
        {
            // 2017-20-07: Signals are no longer IContextSensitive
            Context context = new Context(new InstanceBinder(), Substitute.For<IMediationBinder>());
            Signal<int, int, int> signal = new Signal<int, int, int>();
            signal.AddCommand<Command3Impl<int, int, int>.A>(context);
            signal.Dispatch(0, 1, 2);
            Assert.AreSame(context, Command3Impl<int, int, int>.A.Instance.Context); // test: Context was provided to command eventually
        }

        [Test]
        public void LazilyConstructedSignalDoesNotInjectContext()
        {
            // 2017-20-07: Signals are no longer IContextSensitive
            Context context = new Context(new InstanceBinder(), Substitute.For<IMediationBinder>());
            context.InstanceBinder.Bind<Signal<int, int, int>>();
            Signal<int, int, int> signal = context.InstanceBinder.Get<Signal<int, int, int>>();
            signal.AddCommand<Command3Impl<int, int, int>.A>(null);
            signal.Dispatch(0, 1, 2);
            Assert.IsNull(Command3Impl<int, int, int>.A.Instance.Context);   // test: Context not set on Command if we didn't provide it, even if signal was bound to an IInstanceBinder within a Context
        }

        [Test]
        public void ManuallyConstructedSignalDoesNotInjectContext()
        {
            // 2017-20-07: Signals are no longer IContextSensitive
            Context context = new Context(new InstanceBinder(), Substitute.For<IMediationBinder>());
            Signal<int, int, int> signal = new Signal<int, int, int>();
            signal.AddCommand<Command3Impl<int, int, int>.A>(null);
            context.InstanceBinder.Bind<Signal<int, int, int>>(signal);
            signal.Dispatch(0, 1, 2);
            Assert.IsNull(Command3Impl<int, int, int>.A.Instance.Context);   // test: Context didn't get injected into Command despite signal being bound to one
        }

        [Test]
        public void DispatchRecoversFromThrow()
        {
            // 2016-16-06: recover from listeners and commands throwing errors and ensure signal sets state back to IDLE
            Context context = new Context(new InstanceBinder(), Substitute.For<IMediationBinder>());
            Signal<int, int, int> signal = new Signal<int, int, int>();
            Action<int, int, int> listener = (x, y, z) => throw new NotImplementedException();
            signal.AddListener(listener);
            Assert.Throws(typeof(NotImplementedException), () => signal.Dispatch(0, 1, 2));    // test: we expect the listener to simulate uncaught exception
            signal.RemoveListener(listener);
            Assert.DoesNotThrow(() => signal.Dispatch(0, 1, 2));   // test: signal recovered from earlier exception and is back in IDLE state
        }

        ////////// end Standard tests //////////

        ////////// begin parameters tests (see Signal1_UnitTest.cs) //////////
        
        [Test]
        public void ListenerValueArg()
        {
            Signal<int, int, int> signal = new Signal<int, int, int>();
            
            IListener3<int, int, int> listenerA = Substitute.For<IListener3<int, int, int>>();
            signal.AddListener(listenerA.Callback);
            signal.Dispatch(11, 22, 33);
            listenerA.Received(1).Callback(11, 22, 33); // test: values 11, 22, 33 passed to callback in order

            IListener3<int, int, int> listenerB = Substitute.For<IListener3<int, int, int>>();
            signal.AddListener(listenerB.Callback);
            signal.Dispatch(33, 44, 55);
            listenerA.Received(1).Callback(33, 44, 55); // test: values 33, 44, 55 passed through on 2nd dispatch
            listenerB.Received(1).Callback(33, 44, 55); // test: values indentical for A and B
        }

        [Test]
        public void ListenerReferenceArg()
        {
            Signal<int[], int[], int[]> signal = new Signal<int[], int[], int[]>();
            
            IListener3<int[], int[], int[]> listenerA = Substitute.For<IListener3<int[], int[], int[]>>();
            int[] array1 = new int[1];
            int[] array2 = new int[2];
            int[] array3 = new int[3];
            signal.AddListener(listenerA.Callback);
            signal.Dispatch(array1, array2, array3);
            listenerA.Received(1).Callback(array1, array2, array3);  // test: reference to arrays passed to callback in order

            IListener3<int[], int[], int[]> listenerB = Substitute.For<IListener3<int[], int[], int[]>>();
            array1 = new int[3];
            array2 = new int[4];
            array3 = new int[5];
            signal.AddListener(listenerB.Callback);
            signal.Dispatch(array1, array2, array3);
            listenerA.Received(1).Callback(array1, array2, array3);  // test: references to different arrays received in order
            listenerB.Received(1).Callback(array1, array2, array3);  // test: references indentical for A and B
        }

        [Test]
        public void CommandValueArg()
        {
            Signal<int, int, int> signal = new Signal<int, int, int>();
            
            signal.AddCommand<Command3Impl<int, int, int>.A>(null);
            signal.Dispatch(11, 22, 33);
            Assert.AreEqual(11, Command3Impl<int, int, int>.A.Instance.Arg1);   // test: arg1 was received by command
            Assert.AreEqual(22, Command3Impl<int, int, int>.A.Instance.Arg2);   // test: arg2 was received by command
            Assert.AreEqual(33, Command3Impl<int, int, int>.A.Instance.Arg3);   // test: arg3 was received by command

            signal.AddCommand<Command3Impl<int, int, int>.B>(null);
            signal.Dispatch(44, 55, 66);
            Assert.AreEqual(44, Command3Impl<int, int, int>.A.Instance.Arg1);   // test: values passed to both commands
            Assert.AreEqual(55, Command3Impl<int, int, int>.A.Instance.Arg2);   // test: values passed to both commands
            Assert.AreEqual(66, Command3Impl<int, int, int>.A.Instance.Arg3);   // test: values passed to both commands
            Assert.AreEqual(44, Command3Impl<int, int, int>.B.Instance.Arg1);   // test: values passed to both commands
            Assert.AreEqual(55, Command3Impl<int, int, int>.B.Instance.Arg2);   // test: values passed to both commands
            Assert.AreEqual(66, Command3Impl<int, int, int>.B.Instance.Arg3);   // test: values passed to both commands
        }

        [Test]
        public void CommandReferenceArg()
        {
            Signal<int[], int[], int[]> signal = new Signal<int[], int[], int[]>();
            
            signal.AddCommand<Command3Impl<int[], int[], int[]>.A>(null);
            int[] array1 = new int[1];
            int[] array2 = new int[2];
            int[] array3 = new int[3];
            signal.Dispatch(array1, array2, array3);
            Assert.AreEqual(array1, Command3Impl<int[], int[], int[]>.A.Instance.Arg1); // test: references passed to command
            Assert.AreEqual(array2, Command3Impl<int[], int[], int[]>.A.Instance.Arg2); // test: references passed to command
            Assert.AreEqual(array3, Command3Impl<int[], int[], int[]>.A.Instance.Arg3); // test: references passed to command

            signal.AddCommand<Command3Impl<int[], int[], int[]>.B>(null);
            array1 = new int[4];
            array2 = new int[5];
            array3 = new int[6];
            signal.Dispatch(array1, array2, array3);
            Assert.AreEqual(array1, Command3Impl<int[], int[], int[]>.A.Instance.Arg1); // test: references passed to both commands
            Assert.AreEqual(array2, Command3Impl<int[], int[], int[]>.A.Instance.Arg2); // test: references passed to both commands
            Assert.AreEqual(array3, Command3Impl<int[], int[], int[]>.A.Instance.Arg3); // test: references passed to both commands
            Assert.AreEqual(array1, Command3Impl<int[], int[], int[]>.B.Instance.Arg1); // test: references passed to both commands
            Assert.AreEqual(array2, Command3Impl<int[], int[], int[]>.B.Instance.Arg2); // test: references passed to both commands
            Assert.AreEqual(array3, Command3Impl<int[], int[], int[]>.B.Instance.Arg3); // test: references passed to both commands
        }

        ////////// end parameters tests //////////
    }
}
