﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs.Test.Unit
{
    using Goyfs;
    using Goyfs.Test.Common;
    using NSubstitute;
    using NUnit.Framework;
    using System;

    [TestFixture]
    internal class Signal1_UnitTest
    {
        ////////// Standard tests (see Signal0_UnitTest) ////////// 

        [Test]
        public void InitialState()
        {
            Signal<int> signal = new Signal<int>();
            Assert.False(signal.Disposed, "should be undisposed after construction");
        }

        [Test]
        public void EmptyDispatch()
        {
            Signal<int> signal = new Signal<int>();
            Assert.DoesNotThrow(() => { signal.Dispatch(0); });  // test: dispatching with no listeners is OK
        }

        [Test]
        public void SingleListener()
        {
            Signal<int> signal = new Signal<int>();
            IListener1<int> listener = Substitute.For<IListener1<int>>();

            // add listener, dispatch
            signal.AddListener(listener.Callback);
            signal.Dispatch(8);
            listener.Received(1).Callback(8);    // test: listener was invoked exactly once

            // remove listener, dispatch again
            listener.ClearReceivedCalls();
            signal.RemoveListener(listener.Callback);
            signal.Dispatch(9);
            listener.Received(0).Callback(9);   // test: listener was not invoked again
        }

        [Test]
        public void DoubleListener()
        {
            Signal<int> signal = new Signal<int>();
            IListener1<int> listenerA = Substitute.For<IListener1<int>>();
            IListener1<int> listenerB = Substitute.For<IListener1<int>>();

            // add listeners, dispatch
            signal.AddListener(listenerA.Callback);
            signal.AddListener(listenerB.Callback);
            signal.Dispatch(8);
            listenerA.Received(1).Callback(8);  // test: listener was invoked exactly once
            listenerB.Received(1).Callback(8);  // test: listener was invoked exactly once

            // remove listenerA, dispatch
            listenerA.ClearReceivedCalls();
            listenerB.ClearReceivedCalls();
            signal.RemoveListener(listenerA.Callback);
            signal.Dispatch(8);
            listenerA.Received(0).Callback(8);   // test: listener was not invoked again
            listenerB.Received(1).Callback(8);   // test: listener was invoked again
            Assert.DoesNotThrow(() => { signal.RemoveListener(listenerA.Callback); });  // test: redundant RemoveListener() OK

            // remove listenerB, dispatch
            listenerA.ClearReceivedCalls();
            listenerB.ClearReceivedCalls();
            signal.RemoveListener(listenerB.Callback);
            signal.Dispatch(8);
            listenerA.Received(0).Callback(8);  // test: listenerA not invoked
            listenerB.Received(0).Callback(8);  // test: listenerB not invoked
        }

        [Test]
        public void DoubleListenerOrder()
        {
            Signal<int> signal = new Signal<int>();
            Listener1Impl<int> listenerA = new Listener1Impl<int>();
            Listener1Impl<int> listenerB = new Listener1Impl<int>();

            // add listeners, dispatch
            signal.AddListener(listenerA.Callback);
            signal.AddListener(listenerB.Callback);
            signal.Dispatch(8);
            Assert.Less(listenerA.LastCallback, listenerB.LastCallback);    // test: listener invocation order matches addition order
        }

        [Test]
        public void DuplicateListener()
        {
            Signal<int> signal = new Signal<int>();
            IListener1<int> listener = Substitute.For<IListener1<int>>();

            // add listener twice, dispatch
            signal.AddListener(listener.Callback);
            Assert.DoesNotThrow(() => { signal.AddListener(listener.Callback); });  // test: redundant AddListener() OK
            signal.Dispatch(8);
            listener.Received(1).Callback(8);   // test: listener was invoked exactly once
            listener.ClearReceivedCalls();
            signal.RemoveListener(listener.Callback);
            Assert.DoesNotThrow(() => { signal.RemoveListener(listener.Callback); });   // test: redundant RemoveListener() OK
            signal.Dispatch(8);
            listener.Received(0).Callback(8);   // test: listener not invoked again
        }

        [Test]
        public void DetectListenerLoop()
        {
            Signal<int> signal = new Signal<int>();

            // add listener, dispatch
            signal.AddListener(x => signal.Dispatch(x));
            Assert.Throws<InvalidOperationException>(() => { signal.Dispatch(8); });    // test: dispatching signal again in listener is an error
        }

        [Test]
        public void SingleCommand()
        {
            Signal<int> signal = new Signal<int>();

            // add command, dispatch
            signal.AddCommand<Command1Impl<int>.A>(null);
            int sequence = Sequence.Next;
            signal.Dispatch(8);
            Assert.NotNull(Command1Impl<int>.A.Instance);  // test: command should have been constructed
            Assert.AreEqual(sequence + 1, Command1Impl<int>.A.Instance.LastExecute);    // test: an instance's Execute was invoked

            // remove command, dispatch again
            signal.RemoveCommand<Command1Impl<int>.A>();
            sequence = Command1Impl<int>.A.Instance.LastExecute;
            signal.Dispatch(8);
            Assert.AreEqual(sequence, Command1Impl<int>.A.Instance.LastExecute); // test: removed command doesn't Execute again
        }

        [Test]
        public void DoubleCommand()
        {
            Signal<int> signal = new Signal<int>();

            // add commands, dispatch
            signal.AddCommand<Command1Impl<int>.A>(null);
            signal.AddCommand<Command1Impl<int>.B>(null);
            int sequence = Sequence.Next;
            signal.Dispatch(8);
            Assert.AreEqual(sequence + 1, Command1Impl<int>.A.Instance.LastExecute);    // test: an instance's Execute was invoked in expected order
            Assert.AreEqual(sequence + 2, Command1Impl<int>.B.Instance.LastExecute);    // test: an instance's Execute was invoked in expected order

            // remove command A, dispatch
            signal.RemoveCommand<Command1Impl<int>.A>();
            sequence = Command1Impl<int>.B.Instance.LastExecute;
            signal.Dispatch(8);
            Assert.AreEqual(sequence - 1, Command1Impl<int>.A.Instance.LastExecute);    // test: A's Execute was not invoked again
            Assert.AreEqual(sequence + 1, Command1Impl<int>.B.Instance.LastExecute);    // test: B's Execute was invoked again

            // remove command B, dispatch
            signal.RemoveCommand<Command1Impl<int>.B>();
            sequence = Command1Impl<int>.B.Instance.LastExecute;
            signal.Dispatch(8);
            Assert.AreEqual(sequence, Command1Impl<int>.B.Instance.LastExecute);    // test: B's execute was not invoked again
        }

        [Test]
        public void DuplicateCommand()
        {
            Signal<int> signal = new Signal<int>();

            // add same command twice, dispatch
            signal.AddCommand<Command1Impl<int>.A>(null);
            Assert.DoesNotThrow(() => { signal.AddCommand<Command1Impl<int>.A>(null); });   // test: redundant AddCommand() OK
            int sequence = Sequence.Next;
            signal.Dispatch(8);
            Assert.AreEqual(sequence + 1, Command1Impl<int>.A.Instance.LastExecute);    // test: an instance's Execute was invoked exactly once
            signal.RemoveCommand<Command1Impl<int>.A>();
            Assert.DoesNotThrow(() => { signal.RemoveCommand<Command1Impl<int>.A>(); });    // test: redundant RemoveCommand() OK
        }

        [Test]
        public void ListenerCommandMix()
        {
            Signal<int> signal = new Signal<int>();

            // add listener and command, dispatch
            Listener1Impl<int> listenerA = new Listener1Impl<int>();
            Listener1Impl<int> listenerB = new Listener1Impl<int>();
            signal.AddListener(listenerA.Callback);
            signal.AddCommand<Command1Impl<int>.A>(null);
            signal.AddListener(listenerB.Callback);
            signal.AddCommand<Command1Impl<int>.B>(null);
            int sequence = Sequence.Next;
            signal.Dispatch(8);
            Assert.AreEqual(sequence + 1, Command1Impl<int>.A.Instance.LastExecute);    // test: command was Executed in expected order (commands always first)
            Assert.AreEqual(sequence + 2, Command1Impl<int>.B.Instance.LastExecute);    // test: command was Executed in expected order (commands always first);
            Assert.AreEqual(sequence + 3, listenerA.LastCallback);  // test: listener was invoked in expected order (listeners always last)
            Assert.AreEqual(sequence + 4, listenerB.LastCallback);  // test: listener was invoked in expected order (listeners always last)
        }

        [Test]
        public void DeferredListenerAddition()
        {
            Signal<int> signal = new Signal<int>();

            // add listeners, dispatch
            IListener1<int> listenerA = Substitute.For<IListener1<int>>();
            IListener1<int> listenerB = Substitute.For<IListener1<int>>();
            signal.AddListener(listenerA.Callback);
            signal.AddListener(x => signal.AddListener(listenerB.Callback));
            Assert.DoesNotThrow(() => { signal.Dispatch(8); });  // test: dispatch OK when listener adds another listener
            listenerA.Received(1).Callback(8);  // test: first listener was called
            listenerB.Received(0).Callback(8);  // test: listener added while signal in dispatching state not called
            signal.Dispatch(8);
            listenerA.Received(2).Callback(8);  // test: first listenerA was called again
            listenerB.Received(1).Callback(8);  // test: listenerB was added after signal finished dispatching first time
        }

        [Test]
        public void DeferredListenerRemoval()
        {
            Signal<int> signal = new Signal<int>();

            // add listeners, dispatch
            IListener1<int> listener = Substitute.For<IListener1<int>>();
            signal.AddListener(x => signal.RemoveListener(listener.Callback));
            signal.AddListener(listener.Callback);
            Assert.DoesNotThrow(() => { signal.Dispatch(8); }); // test: dispatch OK when listener removed in another listener
            listener.Received(1).Callback(8);   // test: listener still called despite deferred removal
            signal.Dispatch(8);
            listener.Received(1).Callback(8);   // test: listener not called again after deferred removal
        }

        [Test]
        public void DeferredCommandAddition()
        {
            Signal<int> signal = new Signal<int>();

            int sequence = Sequence.Next;
            Command1Impl<int>.A.Instance = null;
            signal.AddListener(x => signal.AddCommand<Command1Impl<int>.A>(null));
            Assert.DoesNotThrow(() => { signal.Dispatch(8); }); // test: dispatch OK when listener adds a command
            Assert.Null(Command1Impl<int>.A.Instance);  // test: command was not constructed before deferred addition
            signal.Dispatch(8);
            Assert.Less(sequence, Command1Impl<int>.A.Instance.LastExecute);    // test: command was eventually added to signal
        }

        [Test]
        public void DeferredCommandRemoval()
        {
            Signal<int> signal = new Signal<int>();
            IInstanceBinder instanceBinder = Substitute.For<IInstanceBinder>();
            instanceBinder.Get<Signal<int>>().Returns(signal);
            Context context = new Context(instanceBinder, Substitute.For<IMediationBinder>());

            int sequence = Sequence.Next;
            signal.AddCommand<Command1Impl<int>.Remove<Signal<int>, Command1Impl<int>.A>>(context);
            signal.AddCommand<Command1Impl<int>.A>(context);
            Assert.DoesNotThrow(() => { signal.Dispatch(8); });  // test: OK for command to remove another command during execution
            Assert.AreEqual(sequence + 2, Command1Impl<int>.A.Instance.LastExecute);    // test: Command1Impl<int>.A executed despite removal
            signal.Dispatch(8);
            Assert.AreEqual(sequence + 2, Command1Impl<int>.A.Instance.LastExecute);    // test: A was eventually removed from signal
        }

        [Test]
        public void DisposeListeners()
        {
            // test that the signal disposes OK with listeners bound
            Signal<int> signal = new Signal<int>();
            IListener1<int> listener = Substitute.For<IListener1<int>>();

            signal.AddListener(listener.Callback);
            Assert.DoesNotThrow(() => { signal.Dispose(); });   // test: disposal with listeners mapped OK
            Assert.Throws<ObjectDisposedException>(() => { signal.Dispatch(8); });  // test: not allowed to use signal after disposal
            listener.Received(0).Callback(8);   // test: listener was never called after disposal
        }

        [Test]
        public void DisposeCommands()
        {
            // test that the signal disposes OK with commands bound
            Signal<int> signal = new Signal<int>();

            int sequence = Sequence.Next;
            signal.AddCommand<Command1Impl<int>.A>(null);
            Assert.DoesNotThrow(() => { signal.Dispose(); });   // test: disposal with commands OK
            Assert.Throws<ObjectDisposedException>(() => { signal.Dispatch(8); });   // test: not allowed to use signal after disposal
            Assert.Greater(sequence, Command1Impl<int>.A.Instance.LastExecute); // test: command never ran
        }

        [Test]
        public void ContextInjectedIntoCommand()
        {
            // 2017-20-07: Signals are no longer IContextSensitive
            Context context = new Context(new InstanceBinder(), Substitute.For<IMediationBinder>());
            Signal<int> signal = new Signal<int>();
            signal.AddCommand<Command1Impl<int>.A>(context);
            signal.Dispatch(8);
            Assert.AreSame(context, Command1Impl<int>.A.Instance.Context);  // test: Context was provided to command eventually
        }

        [Test]
        public void LazilyConstructedSignalDoesNotInjectContext()
        {
            // 2017-20-07: Signals are no longer IContextSensitive
            Context context = new Context(new InstanceBinder(), Substitute.For<IMediationBinder>());
            context.InstanceBinder.Bind<Signal<int>>();
            Signal<int> signal = context.InstanceBinder.Get<Signal<int>>();
            signal.AddCommand<Command1Impl<int>.A>(null);
            signal.Dispatch(8);
            Assert.IsNull(Command1Impl<int>.A.Instance.Context);    // test: Context not set on Command if we didn't provide it, even if signal was bound to an IInstanceBinder within a Context
        }

        [Test]
        public void ManuallyConstructedSignalDoesNotInjectContext()
        {
            // 2017-20-07: Signals are no longer IContextSensitive
            Context context = new Context(new InstanceBinder(), Substitute.For<IMediationBinder>());
            Signal<int> signal = new Signal<int>();
            signal.AddCommand<Command1Impl<int>.A>(null);
            context.InstanceBinder.Bind<Signal<int>>(signal);
            signal.Dispatch(8);
            Assert.IsNull(Command1Impl<int>.A.Instance.Context);    // test: Context didn't get injected into Command despite signal being bound to one
        }

        [Test]
        public void DispatchRecoversFromThrow()
        {
            // 2016-16-06: recover from listeners and commands throwing errors and ensure signal sets state back to IDLE
            Context context = new Context(new InstanceBinder(), Substitute.For<IMediationBinder>());
            Signal<int> signal = new Signal<int>();
            Action<int> listener = x => throw new NotImplementedException();
            signal.AddListener(listener);
            Assert.Throws(typeof(NotImplementedException), () => signal.Dispatch(8));   // test: we expect the listener to simulate uncaught exception
            signal.RemoveListener(listener);
            Assert.DoesNotThrow(() => signal.Dispatch(8));  // test: signal recovered from earlier exception and is back in IDLE state
        }

        ////////// end Standard tests //////////

        [Test]
        public void ListenerValueArg()
        {
            Signal<int> signal = new Signal<int>();
            
            IListener1<int> listenerA = Substitute.For<IListener1<int>>();
            signal.AddListener(listenerA.Callback);
            signal.Dispatch(11);
            listenerA.Received(1).Callback(11); // test: value 11 was passed to callback

            IListener1<int> listenerB = Substitute.For<IListener1<int>>();
            signal.AddListener(listenerB.Callback);
            signal.Dispatch(22);
            listenerA.Received(1).Callback(22); // test: value 22 was passed through on 2nd dispatch
            listenerB.Received(1).Callback(22); // test: value was indentical for A and B
        }

        [Test]
        public void ListenerReferenceArg()
        {
            Signal<int[]> signal = new Signal<int[]>();

            IListener1<int[]> listenerA = Substitute.For<IListener1<int[]>>();
            int[] array = new int[1];
            signal.AddListener(listenerA.Callback);
            signal.Dispatch(array);
            listenerA.Received(1).Callback(array);  // test: reference to array was passed to callback

            IListener1<int[]> listenerB = Substitute.For<IListener1<int[]>>();
            array = new int[2];
            signal.AddListener(listenerB.Callback);
            signal.Dispatch(array);
            listenerA.Received(1).Callback(array);  // test: reference to 2nd array received
            listenerB.Received(1).Callback(array);  // test: reference was indentical for A and B
        }

        [Test]
        public void CommandValueArg()
        {
            Signal<int> signal = new Signal<int>();

            signal.AddCommand<Command1Impl<int>.A>(null);
            signal.Dispatch(11);
            Assert.AreEqual(11, Command1Impl<int>.A.Instance.Arg1);    // test: value 11 is passed to command

            signal.AddCommand<Command1Impl<int>.B>(null);
            signal.Dispatch(22);
            Assert.AreEqual(22, Command1Impl<int>.A.Instance.Arg1, 22); // test: value 22 is passed to both commands
            Assert.AreEqual(22, Command1Impl<int>.B.Instance.Arg1, 22); // test: value 22 is passed to both commands
        }

        [Test]
        public void CommandReferenceArg()
        {
            Signal<int[]> signal = new Signal<int[]>();
            
            signal.AddCommand<Command1Impl<int[]>.A>(null);
            int[] array = new int[1];
            signal.Dispatch(array);
            Assert.AreSame(array, Command1Impl<int[]>.A.Instance.Arg1); // test: reference to array was passed to command

            signal.AddCommand<Command1Impl<int[]>.B>(null);
            array = new int[2];
            signal.Dispatch(array);
            Assert.AreSame(array, Command1Impl<int[]>.A.Instance.Arg1); // test: reference to 2nd array received
            Assert.AreSame(Command1Impl<int[]>.A.Instance.Arg1, Command1Impl<int[]>.B.Instance.Arg1); // test: reference idential between A and B
        }
    }
}
