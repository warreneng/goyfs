﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs.Test.Unit
{
    using Goyfs;
    using NSubstitute;
    using NUnit.Framework;
    using System;

    [TestFixture]
    internal class Context_UnitTest
    {
        [Test]
        public void InitialState()
        {
            IInstanceBinder instanceBinder = Substitute.For<IInstanceBinder>();
            Context instanceBinderContext = null;
            instanceBinder.Context = Arg.Do<Context>((Context x) => { instanceBinderContext = x; });    // workaround for watching setter-only Context property

            IMediationBinder mediationBinder = Substitute.For<IMediationBinder>();
            Context mediationBinderContext = null;
            mediationBinder.Context = Arg.Do<Context>((Context x) => { mediationBinderContext = x; });  // workaround for watching setter-only Context property

            Context context = new Context(instanceBinder, mediationBinder);

            Assert.False(context.Disposed);     // test: initial state undisposed
            Assert.IsNull(context.Parent);      // test: no parent
            Assert.AreEqual(instanceBinder, context.InstanceBinder);    // test: Context is using our IInstanceBinder
            Assert.AreEqual(mediationBinder, context.MediationBinder);  // test: Context is using our IMediationBinder
            Assert.AreEqual(context, instanceBinderContext);    // test: Context was provided to IInstanceBinder
            Assert.AreEqual(context, mediationBinderContext);   // test: Context was provided to IMediationBinder
        }

        [Test]
        public void DisposedState()
        {
            IInstanceBinder instanceBinder = Substitute.For<IInstanceBinder>();
            IMediationBinder mediationBinder = Substitute.For<IMediationBinder>();
            Context context = new Context(instanceBinder, mediationBinder);
            Action onDisposing = Substitute.For<System.Action>();
            context.Disposing += onDisposing;

            Assert.DoesNotThrow(() => { context.Dispose(); });  // test: OK to dispose
            Assert.IsTrue(context.Disposed);        // test: Context in disposed state
            instanceBinder.Received(1).Dispose();   // test: Context disposed its IInstanceBinder
            mediationBinder.Received(1).Dispose();  // test: Context disposed its IMediationBinder
            onDisposing.Received(1).Invoke();       // test: Context invoked its Disposing Action
            Assert.DoesNotThrow(() => { context.Dispose(); });  // test: redundant dispose OK
            onDisposing.Received(1).Invoke();       // test: Context didn't invoke its Disposing Action again
        }
    }
}