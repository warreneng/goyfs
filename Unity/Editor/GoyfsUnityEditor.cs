﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs.Unity.Editor
{
    using System;
    using System.Reflection;

    public static class GoyfsUnityEditor
    {
        /// <summary>
        /// Returns the value from a non-public field on the given object.
        /// Hat tip CE.
        /// </summary>
        /// <typeparam name="T">return value type</typeparam>
        /// <param name="obj">object to inspect</param>
        /// <param name="fieldName">name of the field</param>
        /// <returns>value of the field</returns>
        /// <exception cref="ArgumentNullException">obj is null</exception>
        /// <exception cref="ArgumentOutOfRangeException">fieldName not found</exception>
        public static T GetPrivateFieldValue<T>(object obj, string fieldName)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            // Recursively search from leaf-to-root of the class hierarchy for the field with the given name.
            Type t = obj.GetType();
            FieldInfo f = null;
            while (f == null && t != null)
            {
                f = t.GetField(fieldName, BindingFlags.NonPublic | BindingFlags.Instance);
                t = t.BaseType;
            }

            if (f == null)
            {
                throw new ArgumentOutOfRangeException("fieldName", string.Format("Field {0} was not found in Type {1}!", fieldName, obj.GetType().FullName));
            }

            return (T)f.GetValue(obj);
        }
    }
}
