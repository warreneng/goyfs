﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs.Unity
{
    using Goyfs;
    using System;
    using TypeReferences;
    using UnityEngine;

    /// <summary>
    /// ContextMaterial is the bridge between Unity and a Goyfs Context.
    /// Its job is to be an Asset in your project tree that can be injected into MonoBehaviours as a [SerializeField].
    /// Multiple MediatedBehaviours sharing one ContextMaterial will likewise share the underlying Context.
    /// 
    /// The user can create Context trees at author-time via ContextMaterial parental hierarchies.
    /// 
    /// When no references to a ContextMaterial remain, its underlying Context is disposed of until requested again later.
    /// </summary>
    [CreateAssetMenu(menuName="GoyfsUnity/ContextMaterial")]
    public class ContextMaterial : ScriptableObject
    {
        /// <summary>
        /// Type of the Context to construct when requested at runtime.
        /// </summary>
        #pragma warning disable 0649    // don't warn about 'contextType' never being assigned (Unity serializes it for us)
        [SerializeField, ClassExtends(typeof(UnityContext), AllowAbstract=false, Grouping=ClassGrouping.ByNamespaceFlat)]
        private ClassTypeReference contextType;
        #pragma warning restore 0649

        /// <summary>
        /// ContextMaterial whose Context will be the parent of our Context.
        /// </summary>
        #pragma warning disable 0649    // don't warn about 'parent' never being assigned (Unity serializes it for us)
        [SerializeField]
        private ContextMaterial parent;
        #pragma warning restore 0649

        /// <summary>
        /// Context instance we construct and dispose of as needed.
        /// </summary>
        private UnityContext context;

        /// <summary>
        /// Tracks whether or not Start() has been called on this ContextMaterial yet.
        /// </summary>
        private bool started;

        /// <summary>
        /// Starts this ContextMaterial, including its Context's UnityMediationBinder.
        /// </summary>
        public void Start()
        {
            GoyfsDebug.Log($"[Goyfs] ContextMaterial {this.name} starting...");
            if (!this.started)
            {
                this.started = true;
                if (this.parent != null)
                {
                    GoyfsDebug.Log($"[Goyfs] ContextMaterial {this.name} starting parent {this.parent.name}");
                    this.parent.References.Add(this);
                    this.parent.Start();
                }

                // throws InvalidOperationException if nobody added reference to this ContextMaterial first
                (this.Context.MediationBinder as UnityMediationBinder).Start();
            }
            else
            {
                GoyfsDebug.Log($"[Goyfs] ContextMaterial {this.name} already started");
            }
        }

        /// <summary>
        /// Register a runtime reference to this ContextMaterial.
        /// When no references remain, the underlying Context will be Disposed.
        /// </summary>
        public readonly ContextMaterialReference References = new ContextMaterialReference();

        /// <summary>
        /// Gets this ContextMaterial's Context instance.
        /// You must add a reference (material.References.Add) prior to accessing this property.
        /// <exception cref="InvalidOperationException">no references exist to this ContextMaterial</exception>
        public Context Context
        {
            get
            {
                // construct context if necessary
                if (this.context == null)
                {
                    // require at least one reference before constructing context
                    if (this.References.Count < 1)
                    {
                        throw new InvalidOperationException("[Goyfs] Cannot construct Context. Did you add a reference to the ContextMaterial first?");
                    }
                    GoyfsDebug.Log($"[Goyfs] ContextMaterial {this.name} Context is null, must create it");

                    // parent contexts must be initialized before children.
                    Context parentContext = null;
                    if (this.parent != null)
                    {
                        GoyfsDebug.Log($"[Goyfs] ContextMaterial {this.name} adding reference to parent ContextMaterial {this.parent.contextType.Type.Name}");
                        this.parent.References.Add(this);
                        parentContext = this.parent.Context;
                    }

                    // construct our Context
                    GoyfsDebug.Log($"[Goyfs] ContextMaterial {this.name} constructing Context {this.contextType.Type.Name}");
                    this.context = Activator.CreateInstance(this.contextType, parentContext) as UnityContext;
                }
                return this.context;
            }
        }

        /// <summary>
        /// UnityEngine message implementation.
        /// </summary>
        private void OnEnable()
        {
            GoyfsDebug.Log($"[Goyfs] ContextMaterial.OnEnable {this.name}");
            this.References.Empty += this.OnRefCountZeroed;
        }

        /// <summary>
        /// UnityEngine message implementation
        /// </summary>
        private void OnDisable()
        {
            GoyfsDebug.Log($"[Goyfs] ContextMaterial.OnDisable {this.name}");
            this.References.Empty -= this.OnRefCountZeroed;
        }

        /// <summary>
        /// Event handler that'll dispose of our Context when there are no references remaining.
        /// </summary>
        private void OnRefCountZeroed()
        {
            GoyfsDebug.Log($"[Goyfs] ContextMaterial {this.name} OnRefCountZeroed");
            if (this.context != null)
            {
                GoyfsDebug.Log($"[Goyfs] ContextMaterial {this.name} disposing its Context");
                this.context.Dispose();
                this.context = null;
            }
            if (this.parent != null)
            {
                GoyfsDebug.Log($"[Goyfs] ContextMaterial {this.name} removing ref to parent {this.parent.name}");
                this.parent.References.Remove(this);
            }
            this.started = false;
        }
    }
}
