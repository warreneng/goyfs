﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs.Unity
{
    using Goyfs;

    /// <summary>
    /// MediatedBehaviour attaches to a ContextMaterial when provided to automatically seek mediation.
    /// </summary>
    public class MediatedBehaviour : GoyfsBehaviour
    {
        /// <summary>
        /// Context this MediatedBehaviour is operating within.
        /// Private access only because user-derived MediatedBehaviours aren't allowed to interact with Contexts. Use an associated Mediator instead.
        /// </summary>
        private Context context;

        /// <summary>
        /// Sets this MediatedBehaviour's ContextMaterial and detaches from the old one.
        /// </summary>
        public override sealed ContextMaterial Material
        {
            set
            {
                if (this.context != null)
                {
                    GoyfsDebug.Log($"[Goyfs] MediatedBehaviour {this.name} unmediating self in Context {this.context.GetType().Name}");
                    this.context.MediationBinder.Unmediate(this);
                }
                base.Material = value;
                if (value != null)
                {
                    GoyfsDebug.Log($"[Goyfs] MediatedBehaviour {this.name} mediating self in Context {value.Context.GetType().Name}");
                    this.context = value.Context;
                    this.context.MediationBinder.Mediate(this);
                }
            }
        }
    }
}
