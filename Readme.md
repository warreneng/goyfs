# Welcome to Goyfs
This repository contains source files for the Goyfs Application Framework. It's open source and free to use however you'd like under the MIT license. To get started, you'll need to add the files in Goyfs/Core to your project. If you're using the Unity game engine, you should also add the files in Goyfs/Unity for useful features specific to Unity.

#### What is it?
Goyfs is an MVC framework written in C# for .NET. It is inspired by Strange IoC, Robotlegs, and the various lessons found in <u>Game Programming Patterns</u> by Robert Nystrom.

#### Why use it?
Its aim is to make it easier to develop your application using MVC principles. Use it in Unity to avoid the headache of writing all your code in MonoBehaviours and relying on `GetComponent` to find your dependencies. Goyfs helps standardize your application's structure so boilerplate code is not implemented on a whim.

---
## Inside the Box
#### Goyfs Core
- [Context](#markdown-header-context)
- [InstanceBinder](#markdown-header-instancebinder)
- [MediationBinder](#markdown-header-mediationbinder)
- [Signal](#markdown-header-signal)
- [Command](#markdown-header-command)
#### Goyfs Unity
- [ContextMaterial](#markdown-header-contextmaterial)
- [UnityContext](#markdown-header-unitycontext)
- [MediatedBehaviour](#markdown-header-mediatedbehaviour)
- [UnmediatedBehaviour](#markdown-header-unmediatedbehaviour)
- [AsyncExecutorBehaviour](#markdown-header-asyncexecutorbehaviour)
---
### Context
Think of Contexts as a scope within your application. A Context alone doesn't accomplish much, but this is where you'll store object references and wire up your mediations. Contexts can be parented to other Contexts to create cascading scopes that add to, inherit, and override the functionality of their parents.
#### Why use it?
Contexts allow you to bundle together sections of your application. Contexts can be loaded and unloaded dynamically, meaning subsystems in your application can exist only when necessary.
#### How to use it?
```csharp
public static void Main(string[] args)
{
	// make a plain Context
	Context contextA = new Context(new InstanceBinder(), new MediationBinder());
	
	// make a derived Context implementation as a child of contextA
	Context contextB = new MyContext(contextA);
}
```
This example shows the construction of two Contexts and creating a tree with them *(we'll discuss the InstanceBinder and MediationBinders later)*. Context A is empty and will need additional setup somewhere else in your codebase. Context B represents a user-derived implementation of Context that contains its own setup code.

Context B is a child of A. This means you have access to what's in both A and B contexts if you are operating within B, whereas you only have access to what's in A if you are operating within A. This is similar to how scopes work in C# between control blocks like for-loops, the method, and the class.

---

### InstanceBinder
Inside of a Context you'll find an InstanceBinder. It keeps track of objects you bind to it. Together with its Context and the Context tree it acts as a resource locator spanning across your application.
#### Why use it?
The InstanceBinder is where you go to get references to other objects in your application. You can store whatever you like in it, but it's especially useful to store objects you might typically implement as a singleton. The singleton pattern is a quick way to make a system statically available across the application. But as your application grows and everything is implemented as a singleton, rewriting the pattern becomes tedious boilerplate work. Instead, you can use the InstanceBinder to turn anything into a singleton by letting the InstanceBinder track it for you.

When combined with a Context, the InstanceBinder can also help manage the lifecycle of your singletons. Any object that implements IDisposable will have its `Dispose()` method invoked by its InstanceBinder when the Context is unloaded. This helps address another headache of the singleton pattern: trying to keep tabs on creating and cleaning up singletons. Instead of writing creation/cleanup logic in every singleton, you can offload the responsibility to the InstanceBinder. You can then be assured that if the Context is loaded, the singleton is available, and when the Context is unloaded, so is the singleton.
#### How to use it?
```csharp
public class MyContext : Context
{
	public MyContext(Context parent = null) : base(new InstanceBinder(), new MediationBinder(), parent)
	{
		// bind a PhysicsSystem as a singleton
		this.InstanceBinder.Bind<PhysicsSystem>(new PhysicsSystem());
		
		// bind a collection to track active players
		this.InstanceBinder.Bind<List<Player>>();
	}
}
```
This is an example of a derived Context implementation binding singletons to its InstanceBinder. They can now be retrieved by anyone with a reference to the Context like so:
```csharp
public static void Main(string[] args)
{
	MyContext contextB = new MyContext();
	PhysicsSystem physics = contextB.InstanceBinder.Get<PhysicsSystem>();
	List<Player> players = contextB.InstanceBinder.Get<List<Player>>();
}
```
The PhysicsSystem you get back is the same one constructed at the time of its binding in MyContext. The List of players you get back, however, is constructed on demand using its default constructor because no object was provided at the time of binding. You can choose either style depending on your needs.

Multitons are also supported as long as you provide a unique key for the binding. The key can be any value *(except the Type's default value)* or reference *(except null)*. Suppose you have two PhysicsSystems you want track as multitons:
```csharp
public class MyContext : Context
{
	public MyContext(Context parent = null) : base(new InstanceBinder(), new MediationBinder(), parent)
	{
		// bind two PhysicsSystems as multitons
		this.InstanceBinder.Bind<PhysicsSystem>(new PhysicsSystem(), "earth");
		this.InstanceBinder.Bind<PhysicsSystem>(new PhysicsSystem(), "space");
	}
}
```
Multitons can be retrieved from the InstanceBinder as long as you know their key:
```csharp
public static void Main(string[] args)
{
	MyContext contextB = new MyContext();
	PhysicsSystem earth = contextB.InstanceBinder.Get<PhysicsSystem>("earth");
	PhysicsSystem space = contextB.InstanceBinder.Get<PhysicsSystem>("space");
}
```
---
### MediationBinder
Each Context also contains a MediationBinder. It is responsible for applying mediators to your views at runtime. Like the InstanceBinder, it leverages Contexts' ability to form trees to inherit and override mediation rules depending on the Context.
#### Why use it?
Mediators help decouple your views from the rest of the application. If, for instance, you write a UI that displays a list, but inside that class you also write the logic to acquire the data it displays, then you cannot reuse that list for other purposes. With mediation you let the Mediator handle communication with the rest of the application. You can configure which views get which Mediators via the MediationBinder and, depending on the Context, change its behavior in a decoupled way.

Any object can be mediated. Anything you want to reuse in different contexts may be a candidate for mediation by moving as much context-specific logic into a Mediator.
#### How to use it?
```csharp
public class MyContext : Context
{
	public MyContext(Context parent = null) : base(new InstanceBinder(), new MediationBinder(), parent)
	{
		// declare the binding in this Context
		this.MediationBinder.Bind<MyUi, MyMediator>();
	}
}

// designed to mediate a MyUi object
public class MyMediator : Mediator<MyUi>
{
	public override void OnMediate(object view)
	{
		base.OnMediate(view);
		
		// communicate with the rest of the app
		PhysicsSystem physics = this.Context.InstanceBinder.Get<PhysicsSystem>();
		
		// feed data into MyUi
		this.View.Draw(physics.Gravity);
	}
}
```
Then at runtime when a MyUi shows up, you go to the MediationBinder for mediation. MyMediator will be constructed &mdash; paired to the MyUi &mdash; and begin feeding data to its view.
```csharp
public static void Main(string[] args)
{
	MyContext contextB = new MyContext();
	MyUi myUi = new MyUi();	// implementation not specified
	contextB.MediationBinder.Mediate(myUi);	// MyMediator will be created for this MyUi
}
```
A Mediator's lifecycle is tightly coupled to the view. When `Unmediate(myUi)` is called on the MediationBinder, the Mediator is disposed of immediately.

---
### Signal
Signals are similar to native C# Events except they're standalone objects that sit between subjects and observers. You can add listeners to a Signal and invoke the listeners by dispatching the Signal.

Signals are another way to implement the Observer pattern, except you're listening to an event  *("an enemy took lethal damage")* instead of listening to a subject *("I took lethal damage")*. The difference is that you don't have to track down every last enemy in the application to establish your listeners. Usually you'll include an identifier with the Signal's parameters so listeners can discern the subject.
#### Why use it?
Signals provided with the Goyfs framework implement IDisposable so you won't have to worry about them retaining listener references after the Context is unloaded. This helps prevent zombie objects from dodging the garbage collector.

They also help facilitate a more decoupled, event-based architecture. With Signals as middlemen, subjects and observers don't need direct reference to each other. This means new subjects and observers can be introduced around a given Signal without needing to revise existing code.
#### How to use it?
```csharp
public class MyContext : Context
{
	public MyContext(Context parent = null) : base(new InstanceBinder(), new MediationBinder(), parent)
	{
		// bind EnemyDiedSignal as a singleton in this Context
		this.InstanceBinder.Bind<EnemyDiedSignal>();
	}
}

public class EnemyDiedSignal : Signal<int>
{
	// Base Signal implementation handles everything for you.
	// You just need a unique Type to put into the InstanceBinder.
}

public class Program
{
	public static void Main(string[] args)
	{
		MyContext contextB = new MyContext();
		
		// retrieve the signal
		EnemyDiedSignal signal = contextB.InstanceBinder.get<EnemyDiedSignal>();
		
		// listen for dying enemies
		signal.AddListener(Program.OnEnemyDied);
		
		// oops, enemy #2 died
		signal.Dispatch(2);
	}
	
	private static void OnEnemyDied(int enemyId)
	{
		// oh no, "enemyId" died!
	}
}
```
In the example above, we derive a class called EnemyDiedSignal from the base Signal\<T\> class. There is no additional code in EnemyDiedSignal beyond the base implementation, but that's OK. Its unique Type allows you to bind this signal to an InstanceBinder as a singleton. Without a derived Type, you may encounter headaches trying to bind Signals that share generic parameters *(e.g. trying to use `Signal<int>` to communicate a time, while another `Signal<int>` is used to communicate a player ID. You'd need to bind these as multitons to distinguish them, at which point it seems easier to have made two derived Signals)*.

Goyfs provides base Signal implementations that support up to 4 parameters. If you need more parameters, you may consider instead creating a struct with all the fields you need and passing it as an argument to a one-parameter Signal:
```csharp
public class PlayerJoinedRoomSignal : Signal<PlayerJoinedRoomSignal.Args>
{
	public struct Args
	{
		public int PlayerId;
		public string DisplayName;
		public int WeaponId;
		public Vector3 Position;
		public Quaternion Rotation;
	}
}
```
It is best practice to use Signals as indicators that something has occurred, not a roundabout way to invoke a method on the observer's end. This usually means naming your signals with verbs in the past tense *(e.g. EnemyDied, PlayerJoinedRoom, InputReceived)* to enforce the concept that the subject is only reporting to the rest of the application. There may be a temptation to try something like "LoadNextLevelSignal" and have an observer load a level. Doing this is essentially an abstracted method call and should be avoided.

---
### Command
Commands are snippets of controller code that can be bound to a Signal. It'll execute every time  the Signal is dispatched.
#### Why use it?
Commands are another way to decouple logic from your models and views. If you have several models that need to be updated in a coordinated way, a Command can encapsulate the work as a cohesive unit. Commands also help keep your Mediators clean by freeing the Mediators from the task of manipulating models directly. For example, if a Mediated button is pressed, instead of the Mediator directly calling `IncrementButtonPresses()` on your application's achievement system, it can dispatch the ButtonPressedSignal it already uses. Now, when you attach an IncrementButtonPressesCommand to that signal, the Command can be in charge of updating the achievement system *(and it can even call the audio system to play a ding)*.
#### How to use it?
```csharp
public class MyContext : Context
{
	public MyContext(Context parent = null) : base(new InstanceBinder(), new MediationBinder(), parent)
	{
		// bind PauseButtonPressedSignal
		this.InstanceBinder.Bind<PauseButtonPressedSignal>();
		
		// add a Command to handle pausing the game
		this.InstanceBinder.Get<PauseButtonPressedSignal>().AddCommand<PauseGameCommand>(this);
	}
}

public class PauseGameCommand : Command
{
	// execute is invoked once per dispatch of the signal it's added to
	public override void Execute()
	{
		// stop the game simulation
		this.Context.InstanceBinder.Get<GameEngine>().PauseSimulation();
		
		// open the pause menu UI
		this.Context.InstanceBinder.Get<MenuManager>().Open(Menus.Pause);
		
		// log the event to business intelligence service
		this.Context.InstanceBinder.Get<BIService>().Log("Paused");
	}
}
```
Now whenever the PauseButtonPressedSignal is dispatched, the PauseGameCommand will run through the series of tasks necessary to achieve a paused state.

---
## Unity Integration
Goyfs comes with a Unity integration out of the box that'll help you get going. The included parts of interest are:
- ContextMaterial
- UnityContext
- MediatedBehaviour
- UnmediatedBehaviour
- AsyncExecutorBehaviour

---
### ContextMaterial
ContextMaterials play a similar role to Unity's Materials: ContextMaterials are assets in your project tree, they are shared by any number of prefabs and/or objects in your scene, and they store your arguments *(like what textures to use, in Material's case)*. With a ContextMaterial asset you can use the Unity Inspector to specify a Context implementation it'll wrap at runtime. You may also specify a parent for the ContextMaterial, which will construct Context trees on your behalf.
#### Why use it?
ContextMaterials are essential to the integration of Goyfs into the Unity engine. As a serializable asset, you can inject references to a ContextMaterial directly into your MonoBehaviours via public fields or [SerializeField] attributes. ContextMaterials also reference-count themselves and automatically construct and unload their underlying Contexts based on your access patterns.
#### How to use it?
To create a ContextMaterial, use the Assets menu or right-click in the Project window, then select Create/GoyfsUnity/ContextMaterial.

---
### UnityContext
UnityContext is a derivative of Context made specifically to operate within the Unity engine.

#### Why use it?
If you look under the hood, you'll see that UnityContext makes sure to use a UnityMediationBinder instead of a standard MediationBinder. The differences between the two boil down to the UnityMediationBinder playing nice with the MonoBehaviour lifecycle.

#### How to use it?
You won't have to do anything different than you might with a regular Context. If you configure your Context externally, the interface is exactly the same and you won't see any difference. If prefer to do configuration inside a derived Context implementation, you merely derive from UnityContext instead.

---
### MediatedBehaviour
MediatedBehaviours can replace the MonoBehaviours that you want to be paired with a Mediator. It automatically seeks mediation via the ContextMaterial reference you provide to it, and unmediates itself during its `OnDestroy()` hook.
#### Why use it?
Many MonoBehaviours are great candidates for mediation, especially those that should be confined to presentation duty *(e.g. UI, characters, audio)*. Typically MonoBehaviours are created with both presentation and business logic intertwined within the class, which hurts its flexibility for reuse elsewhere in the application. By abstracting the business logic away from the MonoBehaviour and into a Mediator, you can pair your view with a Context-specific Mediator. That means your CharacterMediatedBehaviour can flaunt new outfits on demand in the customization screen, but jump and attack at the press of a button in battle.
#### How to use it?
You'll need to get in the habit of deriving your Unity scripts from MediatedBehaviour instead of MonoBehaviour. The tasks of seeking mediation and ending it are taken care of automatically in `Awake()` and `OnDestroy()`, respectively *(just make sure to invoke the base method if you override these hooks)*. Other than that, it's up to you to decide what logic goes in your MediatedBehaviours and what goes into its Mediators.
```csharp
public class PlayerAvatar : MediatedBehaviour
{
	public void LoadOutfit(string uri)
	{ ... }
	public void PlayAnimation(string animName)
	{ ... }
	public void Attack(Vector3 direction)
	{ ... }
}

// use this Mediator in your avatar customization Context
public class AvatarCustomizationMediator : Mediator<PlayerAvatar>
{
	protected override void OnMediate(object view)
	{
		base.OnMediate(view);	// assigns protected T View; for convenience
		
		// initialize the PlayerAvatar
		Inventory inventory = this.context.InstanceBinder.Get<Inventory>();
		this.View.LoadOutfit(inventory.CurrentOutfit);
		this.View.PlayAnimation("fancyPose");
		
		// listen to signals that the current outfit has changed
		this.Context.InstanceBinder.Get<CurrentOutfitChangedSignal>().AddListener(this.OnCurrentOutfitChanged);
	}
	
	private void OnCurrentOutfitChanged(string outfit)
	{
		this.View.LoadOutfit(outfit);
	}
	
	private void OnDestroy()
	{
		base.OnDestroy();
		this.context.InstanceBinder.Get<CurrentOutfitChangedSignal>().RemoveListener(this.OnCurrentOutfitChanged);
	}
}
    
// use this Mediator in your gameplay Context(s)
public class AvatarBattleMediator : Mediator<PlayerAvatar>
{
	protected override void OnMediate(object view)
	{
		base.OnMediate(view);	// assigns protected T View; for convenience
		
		// initialize the PlayerAvatar
		Inventory inventory = this.context.InstanceBinder.Get<Inventory>();
		this.View.LoadOutfit(inventory.CurrentOutfit);
		this.View.PlayAnimation("idle");
		
		// listen for user input on their controller
		this.context.InstanceBinder.Get<Button01PressedSignal>().AddListener(this.OnButton01Pressed);
	}
	
	private void OnButton01Pressed()
	{
		this.View.PlayAnimation("attack");
		this.View.Attack(this.View.transform.forward);
	}
	
	private void OnDestroy()
	{
		base.OnDestroy();
		this.context.InstanceBinder.Get<Button01PressedSignal>().RemoveListener(this.OnButton01Pressed);
	}
}
```
---
### UnmediatedBehaviour
UnmediatedBehaviour is a variant of MediatedBehaviour that does not seek mediation from its ContextMaterial reference, and exposes its Context as a protected field for your derived implementations.
#### Why use it?
Sometimes you can't be bothered to write a Mediator, but you still want your MonoBehaviour to have access to the rest of the application through a Context. UnmediatedBehaviour exists as an option to incorporate the Goyfs framework into your project without using Mediators. Use your best judgement on whether you'll require MediatedBehaviours, use UnmediatedBehaviours, or a mix of the two depending on the situation.
#### How to use it?
You'll need to derive your Unity scripts from UnmediatedBehaviour instead of MonoBehaviour. UnmediatedBehaviour will establish your Context reference for you in `Awake()` and `OnDestroy()` respectively *(just make sure to invoke the base method if you override these hooks)*. After that, you can proceed writing your script as you would a typical MonoBehaviour.
```csharp
public class FallingBehaviour : UnmediatedBehaviour
{
	private PhysicsSystem physics;	// retrieved via Context
	private Vector3 velocity;
	
	protected override void Start()
	{
		base.Start();	// let UnmediatedBehaviour do its thing
		this.physics = this.context.InstanceBinder.Get<PhysicsSystem>();
	}
    
	private void Update()
	{
		this.velocity.y += this.physics.gravity * Time.deltaTime;
		this.transform.position += this.velocity;
	}
}
```
---
### AsyncExecutorBehaviour
The Goyfs Unity integration includes AsyncExecutorBehaviour, which is a required implementation of IAsyncExecutor if you want to run asynchronous Commands. It uses MonoBehaviour's `StartCoroutine()` method under the hood, so you can use it to run any IEnumerator you want.
#### Why use it?
Executing a unit of work across multiple frames can be very useful in certain situations. You can, for example, make a call to a web service and yield execution until you receive the reply. Encapsulating this process helps co-locate the code in an enumerator instead of across disjointed stateful fields inside a class.
#### How to use it?
You should attach AsyncExecutorBehaviour to a GameObject in your scene and make sure it's bound to a Context's InstanceBinder as an IAsyncExecutor. Because you may only need one AsyncExecutorBehaviour for your entire application, it is recommended that you bind it to a GameObject that has had `DontDestroyOnLoad()` called on it, and that you bind it as close to the root Context as possible to maximize its accessibility.
```csharp
public class MyContext : Context
{
	public MyContext(Context parent = null) : base(new InstanceBinder(), new MediationBinder(), parent)
	{
		GameObject root = new GameObject(Root);
		Object.DontDestroyOnLoad(root);
		IAsyncExecutor executor = root.AddComponent<AsyncExecutorBehaviour>();
		this.InstanceBinder.Bind<IAsyncExecutor>(executor);
		
		// set up the IAsyncCommand example
		ProgrammerIsBoredSignal signal = new ProgrammerIsBoredSignal();
		this.InstanceBinder.Bind<ProgrammerIsBoredSignal>(signal);
		signal.AddCommand<CheckTwitterCommand>();
	}
}

public class Program
{
	public static void Main(string[] args)
	{
		Context contextB = new MyContext();
		IAsyncExecutor executor = contextB.InstanceBinder.Get<IAsyncExecutor>();
		
		// example 1: run an IEnumerator directly
		executor.Run(Program.CheckTwitter());
		
		// example 2: trigger an AsyncCommand via Signal
		contextB.InstanceBinder.Get<ProgrammerIsBoredSignal>().Dispatch();
	}
	
	private static IEnumerator CheckTwitter()
	{
		UnityWebRequest request = UnityWebRequest.Get("http://www.twitter.com");
		
		// wait here until the request is done
		yield return request.SendWebRequest();
		
		// add error checking here
		// do something with the response
	}
	
	private class CheckTwitterCommand : AsyncCommand
	{
		protected override IEnumerator Execution()
		{
			// make reuse of the existing CheckTwitter method
			yield return Program.CheckTwitter();
		}
	}
}
```