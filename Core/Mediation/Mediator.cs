﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs
{
    /// <summary>
    /// Abstract IMediator implementation.
    /// </summary>
    /// <typeparam name="TView">type of view this Mediator can be bound to</typeparam>
    public abstract class Mediator<TView> : Disposable, IMediator where TView : class
    {
        /// \copydoc IMediator::Context
        public Context Context { protected get; set; }

        /// <summary>
        /// View that this Mediator is bound to.
        /// </summary>
        protected TView view;

        /// <summary>
        /// Begin mediation of a view.
        /// </summary>
        /// <param name="view">view pairing</param>
        public virtual void OnMediate(object view)
        {
            this.view = (TView)view;
        }
    }
}
