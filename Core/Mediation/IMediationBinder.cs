﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs
{
    using System;

    /// <summary>
    /// An IMediationBinder pairs views with IMediators for the lifetime of the view.
    /// </summary>
    public interface IMediationBinder : IContextSensitive, IDisposable
    {
        /// <summary>
        /// Create a binding between TView and TMediator.
        /// </summary>
        /// <typeparam name="TView">type of the view</typeparam>
        /// <typeparam name="TMediator">type of the mediator</typeparam>
        void Bind<TView, TMediator>() where TView : class where TMediator : IMediator;

        /// <summary>
        /// Remove bindings associated with TView.
        /// </summary>
        /// <typeparam name="TView">type of the view</typeparam>
        void Unbind<TView>() where TView : class;

        /// <summary>
        /// Attempt mediation of a view according to the existing bindings.
        /// </summary>
        /// <param name="view">view to be mediated</param>
        /// <returns>true if mediation was successful</returns>
        bool Mediate(object view);

        /// <summary>
        /// Attempt to end mediation of a view.
        /// </summary>
        /// <param name="view">view to be unmediated</param>
        /// <returns>true if unmediation was successful</returns>
        bool Unmediate(object view);

        /// <summary>
        /// Returns the Type of the IMediator that will mediate a view.
        /// </summary>
        /// <param name="view">view to be mediated</param>
        /// <returns>mediator Type if binding exists, null if not</returns>
        Type GetMediatorType(object view);
    }
}
