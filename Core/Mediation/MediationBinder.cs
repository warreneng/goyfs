﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// MediationBinder binds view types to IMediator types.
    /// It handles the IMediator/view lifecycle on behalf of its Context.
    /// </summary>
    public class MediationBinder : Disposable, IMediationBinder
    {
        /// <summary>
        /// \copydoc IMediationBinder::Context
        /// 
        /// Mediators' Contexts are set to their MediationBinder's Context.
        /// </summary>
        public Context Context { protected get; set; }

        /// <summary>
        /// Registered view-to-IMediator bindings. As Mediate(object view) is called, this controls which IMediator is paired.
        /// </summary>
        private Dictionary<Type, Type> bindings;

        /// <summary>
        /// Collection of view/IMediator instances currently active.
        /// </summary>
        private Dictionary<object, IMediator> mediators;

        /// <summary>
        /// Constructs a new instance of MediationBinder.
        /// </summary>
        public MediationBinder() : base()
        {
            this.bindings = new Dictionary<Type, Type>();
            this.mediators = new Dictionary<object, IMediator>();
        }

        /// <summary>
        /// \copybrief IMediationBinder::Bind<TView,TMediator>()
        /// </summary>
        /// <typeparam name="TView">type of the view to mediate</typeparam>
        /// <typeparam name="TMediator">type of the mediator to pair with the view</typeparam>
        /// <exception cref="InvalidOperationException">view type already bound</exception>
        /// <exception cref="ObjectDisposedException">calling after Dispose() has occurred</exception>
        public virtual void Bind<TView, TMediator>() where TView : class where TMediator : IMediator
        {
            this.AssertUndisposed();

            if (this.bindings.ContainsKey(typeof(TView)))
            {
                throw new InvalidOperationException($"Binding already exists for view with type {typeof(TView).FullName}.");
            }
            else
            {
                this.bindings.Add(typeof(TView), typeof(TMediator));
            }
        }

        /// <summary>
        /// \copybrief IMediationBinder::Unbind<TView>()
        /// 
        /// Existing view/mediator pairs for this view are unaffected, but can no longer be unmediated.
        /// </summary>
        /// <typeparam name="TView">type of the view binding</typeparam>
        /// <exception cref="InvalidOperationException">view binding doesn't exist</exception>
        /// <exception cref="ObjectDisposedException">calling after Dispose() has occurred</exception>
        public virtual void Unbind<TView>() where TView : class
        {
            this.AssertUndisposed();

            if (!this.bindings.ContainsKey(typeof(TView)))
            {
                throw new InvalidOperationException($"View type {typeof(TView).FullName} is not bound.");
            }
            else
            {
                this.bindings.Remove(typeof(TView));
            }
        }

        /// <summary>
        /// \copybrief IMediationBinder::Mediate(object view)
        /// 
        /// Upon pairing with a view, mediators' IMediator::OnMediate(object view) will be called.
        /// </summary>
        /// <param name="view">view instance to mediate</param>
        /// <returns>true if mediation succeeded or has already occurred</returns>
        /// <exception cref="ObjectDisposedException">calling after Dispose() has occurred</exception>
        public virtual bool Mediate(object view)
        {
            this.AssertUndisposed();

            bool mediated = this.mediators.ContainsKey(view);
            if (!mediated)
            {
                Type mediatorType = this.GetMediatorType(view);
                if (mediatorType != null)
                {
                    mediated = true;
                    IMediator mediator = Activator.CreateInstance(mediatorType) as IMediator;
                    this.mediators.Add(view, mediator);
                    mediator.Context = this.Context;
                    mediator.OnMediate(view);
                }
            }
            return mediated;
        }

        /// <summary>
        /// \copybrief IMediationBinder::Unmediate(object view)
        /// 
        /// Any mediator paired with the view will have its Dispose() called.
        /// </summary>
        /// <param name="view">view to unmediate</param>
        /// <returns>true if the mediator was removed</returns>
        /// <exception cref="ObjectDisposedException">calling after Dispose() has occurred</exception>
        public virtual bool Unmediate(object view)
        {
            this.AssertUndisposed();

            if (this.mediators.ContainsKey(view))
            {
                IMediator mediator = this.mediators[view];
                this.mediators.Remove(view);
                mediator.Dispose();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// \copybrief IMediationBinder::GetMediatorType(object view)
        /// 
        /// Returns the IMediator bound to the view's type by this MediationBinder.
        /// If no binding exists, the parent Context's IMediationBinder is asked for the Type.
        /// </summary>
        /// <param name="view">view to be mediated</param>
        /// <returns>type of the IMediator that would be constructed to mediate view</returns>
        /// <exception cref="ObjectDisposedException">calling after Dispose() has occurred</exception>
        public Type GetMediatorType(object view)
        {
            this.AssertUndisposed();

            Type viewType = view.GetType();
            if (this.bindings.ContainsKey(viewType))
            {
                return this.bindings[viewType];
            }
            else if (this.Context.Parent != null)
            {
                return this.Context.Parent.MediationBinder.GetMediatorType(view);
            }
            else
            {
                return null;
            }
        }

        /// \copydoc Disposable::OnDispose(bool invokedByUser)
        protected override void OnDispose(bool invokedByUser)
        {
            if (!this.Disposed)
            {
                if (invokedByUser)
                {
                    // dispose mediators
                    foreach (KeyValuePair<object, IMediator> kvp in this.mediators)
                    {
                        kvp.Value.Dispose();
                    }
                }

                this.bindings.Clear();
                this.mediators.Clear();
            }
        }
    }
}
