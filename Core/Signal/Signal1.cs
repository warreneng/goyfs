﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Signal that dispatches with 1 argument.
    /// </summary>
    /// <typeparam name="T1">first argument type</typeparam>
    public class Signal<T1> : Disposable
    {
        /// <summary>
        /// Command types to execute when this Signal is dispatched.
        /// </summary>
        private List<ICommand<T1>> commands;

        /// <summary>
        /// Callbacks to invoke when this Signal is dispatched.
        /// </summary>
        private List<Action<T1>> listeners;

        /// <summary>
        /// Current state of the signal.
        /// Useful for preventing Dispatch loops and modifying listeners/commands while dispatching.
        /// </summary>
        private SignalState state;

        /// <summary>
        /// Queues Add/Remove modifications while Signal is in SignalState::Dispatching.
        /// </summary>
        private Queue<Action> deferredModifications;

        /// <summary>
        /// Add a listener to this Signal.
        /// </summary>
        /// <param name="listener">Action delegate taking 1 argument</param>
        /// <returns>this Signal</returns>
        /// <exception cref="ArgumentNullException">null listener arg</exception>
        /// <exception cref="ObjectDisposedException">calling after Dispose() has occurred</exception>
        public Signal<T1> AddListener(Action<T1> listener)
        {
            this.AssertUndisposed();
            
            if (listener == null)
            {
                throw new ArgumentNullException("listener");
            }
            else if (this.state != SignalState.Idle)
            {
                (this.deferredModifications = this.deferredModifications ?? new Queue<Action>(1)).Enqueue(() => { this.AddListener(listener); });
            }
            else
            {
                // add unique listener
                this.RemoveListener(listener);
                (this.listeners = this.listeners ?? new List<Action<T1>>(1)).Add(listener);
            }

            return this;
        }

        /// <summary>
        /// Remove a listener from this Signal.
        /// </summary>
        /// <param name="listener">Action delegate taking 1 argument</param>
        /// <returns>this Signal</returns>
        /// <exception cref="ObjectDisposedException">calling after Dispose() has occurred</exception>
        public Signal<T1> RemoveListener(Action<T1> listener)
        {
            this.AssertUndisposed();

            if (this.state != SignalState.Idle)
            {
                (this.deferredModifications = this.deferredModifications ?? new Queue<Action>(1)).Enqueue(() => { this.RemoveListener(listener); });
            }
            else
            {
                this.listeners?.Remove(listener);
            }

            return this;
        }

        /// <summary>
        /// Adds an ICommand to execute when this Signal is dispatched.
        /// </summary>
        /// <typeparam name="TCommand">ICommand taking 1 argument</typeparam>
        /// <param name="context">Context to execute the command in</param>
        /// <returns>this Signal</returns>
        /// <exception cref="ObjectDisposedException">calling after Dispose() has occurred</exception>
        public Signal<T1> AddCommand<TCommand>(Context context) where TCommand : ICommand<T1>
        {
            this.AssertUndisposed();

            if (this.state != SignalState.Idle)
            {
                (this.deferredModifications = this.deferredModifications ?? new Queue<Action>(1)).Enqueue(() => { this.AddCommand<TCommand>(context); });
            }
            else
            {
                // add unique command
                this.RemoveCommand<TCommand>();
                (this.commands = this.commands ?? new List<ICommand<T1>>(1)).Add(Activator.CreateInstance<TCommand>());

                // inject context
                this.commands[this.commands.Count - 1].Context = context;
            }

            return this;
        }

        /// <summary>
        /// Removes an ICommand from executing when this Signal is dispatched.
        /// </summary>
        /// <typeparam name="TCommand">ICommand taking 1 argument</typeparam>
        /// <returns>this Signal</returns>
        /// <exception cref="ObjectDisposedException">calling after Dispose() has occurred</exception>
        public Signal<T1> RemoveCommand<TCommand>() where TCommand : ICommand<T1>
        {
            this.AssertUndisposed();

            if (this.state != SignalState.Idle)
            {
                (this.deferredModifications = this.deferredModifications ?? new Queue<Action>(1)).Enqueue(() => { this.RemoveCommand<TCommand>(); });
            }
            else
            {
                this.commands?.RemoveAll((ICommand<T1> x) => { return x is TCommand; });
            }

            return this;
        }

        /// <summary>
        /// Dispatches this Signal.
        /// 
        /// Commands added to this Signal will be executed in order of their addition,
        /// followed by listener invocations in order of addition.
        /// </summary>
        /// <param name="arg1">first argument</param>
        /// <returns>this Signal</returns>
        /// <exception cref="InvalidOperationException">dispatching signal twice in same callstack</exception>
        /// <exception cref="ObjectDisposedException">calling after Dispose() has occurred</exception>
        public Signal<T1> Dispatch(T1 arg1)
        {
            this.AssertUndisposed();

            // prevent looping dispatches
            if (this.state == SignalState.Dispatching)
            {
                throw new InvalidOperationException("Dispatch loop detected");
            }
            this.state = SignalState.Dispatching;

            try
            {
                // invoke commands first
                for (int i = 0; i < this.commands?.Count; ++i)
                {
                    this.commands[i].Execute(arg1);
                }

                // invoke listeners last
                for (int i = 0; i < this.listeners?.Count; ++i)
                {
                    this.listeners[i](arg1);
                }
            }
            finally
            {
                // ready to dispatch again
                this.state = SignalState.Idle;

                // resolve deferred adds/removes
                while (this.deferredModifications?.Count > 0)
                {
                    this.deferredModifications.Dequeue()(); // dequeue and invoke
                }
            }

            return this;
        }

        /// \copydoc Disposable::OnDispose(bool invokedByUser)
        protected override void OnDispose(bool invokedByUser)
        {
            if (!this.Disposed)
            {
                this.commands?.Clear();
                this.listeners?.Clear();
                this.deferredModifications?.Clear();
            }
        }
    }
}
