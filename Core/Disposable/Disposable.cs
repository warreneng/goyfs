﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 *  
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs
{
    using System;

    /// <summary>
    /// Base implementation of IDisposable for classes that have resources to clean up in preparation for GC.
    /// </summary>
    public abstract class Disposable : IDisposable
    {
        /// <summary>
        /// Gets a value indicating whether this object has been disposed.
        /// </summary>
        public bool Disposed { get; private set; }

        /// <summary>
        /// Throws System.ObjectDisposedException if this object has been Disposed.
        /// </summary>
        public void AssertUndisposed()
        {
            if (this.Disposed)
            {
                throw new ObjectDisposedException(this.GetType().FullName);
            }
        }

        /// <summary>
        /// Finalizer invoked by garbage collector.
        /// </summary>
        ~Disposable()
        {
            this.OnDispose(false);
            this.Disposed = true;
        }

        /// <summary>
        /// Induce manual clean up of resources in preparation for garbage collection.
        /// 
        /// Future finalization of this object by the Garbage Collector is suppressed.
        /// </summary>
        public void Dispose()
        {
            this.OnDispose(true);
            this.Disposed = true;
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Clean up resources in preparation for garbage collection.
        /// Override and add class-specific resource cleanup.
        /// 
        /// Here's an implementation template:
        /// 
        /// protected override void OnDispose(bool invokedByUser)
        /// {
        ///     if (!this.Disposed)
        ///     {
        ///        if (invokedByUser)
        ///        {
        ///            // put cleanup code here that invokes Dispose() on other objects
        ///        }
        ///         
        ///        // put cleanup code here that doesn't invoke Dispose() on other objects
        ///     }
        ///     base.OnDispose(invokedByUser);
        /// }
        /// </summary>
        /// <param name="invokedByUser">true if invoked via Dispose(), false if invoked by class finalizer</param>
        protected abstract void OnDispose(bool invokedByUser);
    }
}
