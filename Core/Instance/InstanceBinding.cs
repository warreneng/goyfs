﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs
{
    using System;

    /// <summary>
    /// InstanceBindings control the manner in which instances exist within IInstanceBinders.
    /// </summary>
    /// <typeparam name="TInstance">type of the bound Instance</typeparam>
    public abstract class InstanceBinding<TInstance> : Disposable, IContextSensitive
    {
        /// \copydoc IContextSensitive::Context
        public Context Context { private get; set; }

        /// <summary>
        /// Returns an Instance's value, constructing it if necessary and scoping it to the current Context
        /// </summary>
        /// <param name="instance">Instance whose value to construct and/or get</param>
        /// <returns>value of this Instance</returns>
        protected TInstance GetOrConstructValue(TInstance instance)
        {
            if (instance == null)
            {
                instance = (TInstance)Activator.CreateInstance(typeof(TInstance));
                this.ScopeToContext(instance);
            }
            return instance;
        }

        /// <summary>
        /// Set the Context of an object if it implements IContextSensitive.
        /// </summary>
        /// <param name="instance">possible IContextSensitive object</param>
        protected void ScopeToContext(TInstance instance)
        {
            if (instance is IContextSensitive)
            {
                (instance as IContextSensitive).Context = this.Context;
            }
        }
    }
}
