﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Warren Eng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace Goyfs
{
    using System.Collections;
    
    /// <summary>
    /// IAsyncExecutor takes responsibility for iterating an
    /// IEnumerator to completion over some period of time.
    /// </summary>
    public interface IAsyncExecutor
    {
        /// <summary>
        /// Adds an IEnumerator to be iterated to completion over some period of time.
        /// </summary>
        /// <param name="execution">asynchronous unit of work</param>
        void Run(IEnumerator execution);

        /// <summary>
        /// Halts execution of a running IEnumerator.
        /// </summary>
        /// <param name="execution">IEnumerator previously passed as argument to Run()</param>
        void Halt(IEnumerator execution);
    }
}
